<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Basic visualization using meshalyzer</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/visualization/meshalyzer/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Edward Vigmond <edward.vigmond@u-bordeaux.fr></i>
<h2 id="meshalyzer-tutorial">Introduction to meshalyzer</h2>
<p>meshalyzer is a visualization tool for openCARP, capable of
displaying 4D sets on meshes and allowing user interaction to
investiagte data. The software can be downloaded from the <a
href="https://git.opencarp.org/openCARP/meshalyzer">openCARP</a>
webpage. meshalyzer needs a mesh and optionally, data to display on the
mesh. To run the experiments of this example change directories as
follows:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/visualization/meshalyzer</span></code></pre></div>
<h2 id="generate-model-and-data">Generate Model and Data</h2>
<p>First we need something to display. Generate the model and data
with</p>
<pre><code>./run.py [--np n] [--bidomain]</code></pre>
<p>Use the bidomain flag if you want to look at extracellular potential
data. This may take a while, especially if you have a low core count.
This is a one time operation.</p>
<h2 id="launch-meshalyzer">Launch meshalyzer</h2>
<pre><code>meshalyzer </code></pre>
<p>A file dialog appears from which you can navigate to any subdirectory
of <em>meshes</em> and select <em>block.pts</em>.</p>
<p>The meshalyzer windows will pop up automatically. One window is the
<em>control window</em> with many widgets for controlling the display.
The other window is the <em>model window</em> which displays the data on
the model. The model window is white because there are no surfaces in
the model and that is the entity displayed by default. Creating surfaces
is a one time operation which is done by:</p>
<pre><code>click ``File/Compute surfaces`` from the top menu bar
click the *OK* to write it to disk</code></pre>
<p>Right now, there is no data, so it is one colour, orange. Let's read
in some data. The file selector will be in the directory of the last
file selected.</p>
<div class="parsed-literal">
<p>click on <strong>File/Read IGB data</strong> from the top menu bar go
up 2 directories by clicking <em>..</em> twice select the directory
<em>meshalyzer_test</em> select 'vm.igb'</p>
</div>
<p>In the model window, the rectangle should be black on a white
background. The tissue is black because it is all at rest.</p>
<figure>
<img src="vis_meshalyzer_CW.png" class="align-center"
style="width:25.0%"
alt="Meshalyzer control window showing button to toggle vertex display (cyan box), tabs for Highlighting (yellow box) and within the Surface tab (green box), the button to toggle display of the surface element outlines (magenta box)." />
<figcaption aria-hidden="true">Meshalyzer control window showing button
to toggle vertex display (cyan box), tabs for Highlighting (yellow box)
and within the Surface tab (green box), the button to toggle display of
the surface element outlines (magenta box).</figcaption>
</figure>
<h3 id="advance-in-time">Advance in time</h3>
<p>Put your mouse in the model window. Press the right arrow. Notice how
the time advances in the control window. However, you still do not see
anything change. This is because the colour scale is optimized for the
first frame of data. To reoptimize the colour scale for a particular
frame, put the mouse in the model window and press <strong>o</strong>.
The stimulus was applied at 2 ms so make sure you advance at least that
far. You can go backwards in time with the left arrow.</p>
<p>The time instant displayed can also be controlled with the time
slider. Clicking to the left or right changes decrements or increments
the time respectively, while the time button can be dragged while
holding the <em>middle mouse button</em></p>
<h3 id="watch-a-movie">Watch a movie</h3>
<p>In the control window, press the double arrow. The time will advance
until the last frame. The animation can be stopped with the pause
button. To make a movie, click on <code>Output/PNG sequence</code> from
the top menu bar in the control window. A series of PNGs will be created
which are numercially ordered so you can use software like <a
href="https://www.ffmpeg.org">ffmpeg</a> to create a movie. You will be
asked for the base name of the files on to which will be appended a
number. The movie starts from the frame displayed so make sure you are
not at the last frame. The time can be controlled with the <em>time
slider</em> at the bottm of the control window.</p>
<h3 id="inspecting-a-single-node">Inspecting a single node</h3>
<div class="parsed-literal">
<p>Click on the <strong>Highlight tab</strong> in the <em>Control
widget</em>. Now turn on highlighting by clicking
<strong>On</strong>.</p>
</div>
<p>Let's select a node near the middle of the sheet. Before you can
select a point, you need to be able to see the points. There are 2 ways
to do this</p>
<ol type="1">
<li>turn on point display by clicking on the <code>Vertices</code>
button in the control window</li>
<li>click on the <code>Surface tab</code> and display the
<code>Outline</code></li>
</ol>
<div class="parsed-literal">
<p>Click on <strong>Pick Vertex</strong> in the <em>Highlight tab</em>
(it should turn red) and try to pick a point near the center of the
sheet using <em>mouse button 1</em>.</p>
</div>
<p>If you successfully picked a point, it will be a red dot and the
attached surface elements will be outlined in gold and filled in with
pink. The <code>vertex</code> field will be updated in the Highlight
tab. If nothing was picked or you want to select another point, you can
also press the <strong>p</strong> key with the mouse in the model window
(the button turns red as well in the control window) and try again.
Sometimes, zooming in helps.</p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>To zoom, drag while holding <em>mouse button 3</em>. To translate the
model, hold <em>mouse button 2</em>. Rotate with <em>mouse button
1</em>. For those without 3 button mice, you can also hold the
<em>shift</em> and <em>control</em> keys while depressing the mouse
button.</p>
</div>
<div class="tip">
<div class="title">
<p>Tip</p>
</div>
<p>If your control window gets buried or you lose track of it, just
press <strong>C</strong> in the model window. Conversely, from the
control window, select <code>Image/Raise Window</code> from the top menu
bar to raise the model window.</p>
</div>
<h4 id="highlighting">Highlighting</h4>
<p>Once you have selected your point, you can turn off the outline or
points. Looking at the Highlight tab, you can see the element selected
its data value at that particular point in time. To find out more
information, click on the gold <strong>?</strong> button. This opens the
<em>highlight window</em>. The top line indicates the vertex highlighted
and how many vertices are in the model. The third line tells you the
coordinates of the selected point. Below, there is also the connectivity
information showing all elements of which the node is a part, as well as
all nodes which are connected by an edge. These items are clickable and
become highlighted themselves.</p>
<div class="parsed-literal">
<p>Select an attached surface element from the list in the highlight
window.</p>
</div>
<p>You will see a blue outline and scrolling down in the window you will
see the information about the surace element. All highlighted items hve
a gray background in the list. Now, let's see data as a function of
time.</p>
<div class="parsed-literal">
<p>Click on <strong>Time Series</strong> in the <em>Highlight
tab</em>.</p>
</div>
<p>A window will pop up with the time trace at that node. The vertical
green line indicates the current time. Put the cursor on the time slider
and scroll it while holding the middle mouse button.The green line will
move.</p>
<div class="tip">
<div class="title">
<p>Tip</p>
</div>
<p>Clicking the middle mouse button in the time series window will show
the coordinates of the mouse.</p>
</div>
<h4 id="comparing-nodes">Comparing nodes</h4>
<p>Suppose we want to compare the time series at two nodes. First, we
need to store the visible time trace.</p>
<ol>
<li><p>Click on the <code>Hold</code> button at the bottom of the time
trace window.</p></li>
<li><p>Put the mouse in the <code>Vertex</code> input field and change
the highlighted node by</p>
<blockquote>
<ul>
<li>rolling the mouse wheel</li>
<li>dragging the mouse while clicking</li>
<li>typing a new number</li>
<li>repicking as above</li>
</ul>
</blockquote></li>
</ol>
<p>You will see the red time trace update to the new node highlighted
and the original trace is blue. Several traces may be stored. If you
want to save the curves to disk, click the <code>Write</code> button,
select a name, and an ASCII file will be written. When you want to get
rid of the curves, put your mouse on the time trace and press mouse
button 3, which pops up a menu from which you select
<code>clear static curves</code>.</p>
<h3 id="saving-the-state">Saving the State</h3>
<p>Adjust the display to something that you like. You can adjust the
colour scale, the orientation and scale of the block, and the time
displayed. Once you have it perfect, save it:</p>
<div class="parsed-literal">
<p>click on <strong>File/Save state</strong> from the top menu bar enter
a name, eg., 'testsave'</p>
</div>
<p>Now, change the display of the model again, even clearing any static
timer series. You would now like to get the old view back,</p>
<div class="parsed-literal">
<p>click on <strong>File/Restore state</strong> from the top menu bar
select 'testsave.mshz'</p>
</div>
<p>It should look the same as before. You should even see the previously
held time curves. If you just want to save the <strong>current
view</strong> without any of the properties, you can
<code>Save transform</code> and then <code>Read transform</code> from
the <strong>File</strong> menu.</p>
<h2 id="different-data-set">Different Data Set</h2>
<p>If you have run the bidomain simulation, you can load a new data set.
We will choose an IGB data set. IGB files are a particular format of
data file composed of an ascii header and a binary data. Meshalyzer can
also read plain ASCII files with one data value for line for each
node.</p>
<div class="parsed-literal">
<p>click on <strong>File/Read IGB data</strong> from the top menu bar
select 'phie.igb' go to time frame 6 type <strong>o</strong> in the
model window.</p>
</div>
<p>The model window will be mostly white as the model merges with the
background.</p>
<div class="tip">
<div class="title">
<p>Tip</p>
</div>
<p>You can change the background color under
<code>Image/background colour</code></p>
</div>
<p>If you advance the time to almost the end, you will only see a few
colours. This is because the data range has changed. The colour scale
can be set to optimize every new frame. To do this</p>
<div class="parsed-literal">
<p>click on the <em>auto</em> button on the left side of the colour box
below the <em>optimal</em> button.</p>
</div>
<p>Now, you will see all colours every frame.</p>
<p>More details on how to use meshalyzer and its capabilities are found
in the next example meshalyzer_advanced as well as <a
href="/documentation/examples/visualization/meshalyzer">meshalyzer
manual</a></p>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
