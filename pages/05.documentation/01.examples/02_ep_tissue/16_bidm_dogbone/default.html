<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Dogbone Pattern Generation under Unequal Anisotropy</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/16_bidm_dogbone/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Karli Gillette <karli.gillette@gmail.com></i>
<h2 id="introduction">Introduction</h2>
<p>Unequal anisotropy ratios, as seen in cardiac tissue, can be
responsible for the formation of unexpectedly complex polarization
patterns when applying stimuli through point sources. While an
elliptical polarization pattern is normally expected when applying a
strong source potential, a dogbone pattern can be achieved under unequal
conductive anisotropy. The dogbone pattern is termed for the occurrence
of two circular regions of opposite polarity (virtual electrodes) that
arise. The pattern can be achieved by applying anodal (-) or cathodal(+)
simulation. To run the experiments of this example change directories as
follows:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/16_bidm_dogbone</span></code></pre></div>
<h2 id="experimental-setup">Experimental Setup</h2>
<p>The geometry and the electrodes are defined as follows:</p>
<p><strong>Model:</strong> A hexahedral FE model with dimensions 20.0mm
x 20.0mm x 10.0mm is submersed in a bath on both the upper and lower
ends with thickness 5.0 mm. Fibers are assigned in the model ranging
from -60 to 60 degrees. The model is automatically generated in mesher
during the simulation.</p>
<figure>
<img src="02_16_dogbone_geometry.png" class="align-center"
style="width:75.0%" />
</figure>
<p><strong>Stimulus:</strong> A strong anodal or cathodal extracellular
point stimulus (pt_stim) is applied just above the top surface of the
mesh in the bath. An extracellular ground electrode (GND) is assigned to
the bottom of the bath.</p>
<p><strong>Assigning unequal anisotropy:</strong> Conductivities are
applied either anisotropically using the default openCARP values to
generate a dogbone pattern:</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.g_il</span>          = 0.174</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.g_it</span>          = 0.019</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.g_in</span>          = 0.019</span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.g_el</span>          = 0.625</span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.g_et</span>          = 0.236</span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.g_en</span>          = 0.236</span></code></pre></div>
<p>or to a single conductivity values of 0.625 to generate an elliptic
pattern.</p>
<h2 id="usage">Usage</h2>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--visualize</span>         <span class="co"># visualize results with meshalyzer</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--pt_stim</span>           = anode <span class="er">(</span><span class="ex">default</span><span class="kw">)</span><span class="ex">,</span> cathode <span class="co">#type of applied point source stimulus</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--no_anisotropy</span>     <span class="co"># turn off conductive anisotropy</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--res</span>               <span class="co"># changes the resolution of the mesh [0.25,0.75] with default 0.5.</span></span></code></pre></div>
<p>If the program is run with the <code>--visualize</code> option,
meshalyzer will automatically load the model with applied activation
data showing the occurrence of the dog bone (8 seconds) in the presence
of anisotropy or an elliptical pattern.</p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>This mesh has a fairly high resolution and may take longer to run.
Try increasing the number of cores using the ''--np'' option or changing
the resolution of the mesh with the <code>--res</code> option.</p>
</div>
<h2 id="expected-results">Expected Results</h2>
<p>The expected outcomes of the four cases of applying a cathode or
anode point stimulus with or without anisotropy are shown:</p>
<figure>
<img src="02_16_dogbone_results.png" class="align-center"
style="width:100.0%" />
</figure>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
