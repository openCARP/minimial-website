---
description: This example shows how to use polling files to sweep parameters
image: ''
title: Parameter sweeps
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Parameter Sweeps</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/20_parameter_sweep/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Christoph Augustin <christoph.augustin@medunigraz.at></i>
<h2 id="tutorial_parameter-sweeps">Parameter sweeps</h2>
<p>Transmembrane voltage can be changed by applying an extracellular
field in the extracellular space. As outlined in Sec. <code
class="interpreted-text" role="ref">electrical-stimulation</code>, an
electric field can be set up either by injection/withdrawal of currents
in the extracellular space or by changing the extracellular potential
with voltage sources.</p>
<p>For testing the influence of the instant of stimulation, we generate
a thin strand of tissue of 1 cm length. Electrodes are located at both
caps of the strand and we vary the instant of the stimulations at these
electrodes over time.</p>
<h2 id="experiment-options">Experiment Options</h2>
<p>Several types of stimulation setups are predefined. To run these
experiments, execute</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/20_parameter_sweep</span></code></pre></div>
<p>and run</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span></span></code></pre></div>
<p>to see all experimental parameters. In this example we will use the
carputils options</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">--polling-param</span> POLLING_PARAM [POLLING_PARAM ...]</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Polling</span> parameter</span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a><span class="ex">--polling-range</span> min:max:num [min:max:num ...]</span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Define</span> polling parameter range</span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a><span class="ex">--polling-file</span> POLLING_FILE</span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">File</span> including polling data for parameter sweeps</span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a><span class="ex">--sampling-type</span> <span class="dt">{linear</span><span class="op">,</span><span class="dt">lhs}</span></span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Sampling</span> type for parameter sweeps. Choose between</span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a>                      <span class="st">&quot;linear&quot;</span> and latin hypercube <span class="er">(</span><span class="st">&quot;lhs&quot;</span><span class="kw">)</span> <span class="ex">sampling</span></span></code></pre></div>
<h2 id="generation-of-a-polling-file">Generation of a polling file</h2>
<p>Generate a polling file where the start times of
<code>stimulus[0]</code> and <code>stimulus[1]</code> are linearly
interpolated in the intervals [20ms, 60ms] and [50ms, 70ms],
respectively. A total of 25 sampling points is used. Note that the
number of sampling points has to be the same for each polling parameter.
The result is stored in the polling file <code>stimulus.poll</code>.</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span>  <span class="at">--polling-file</span> stimulus.poll <span class="dt">\</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a>          <span class="at">--polling-param</span> stimulus[0].start stimulus[1].start <span class="dt">\</span></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a>          <span class="at">--polling-range</span> 20:60:25 50:70:25</span></code></pre></div>
<p>To use <span class="title-ref">latin hypercube sampling
&lt;https://en.wikipedia.org/wiki/Latin_hypercube_sampling&gt;</span> as
the sampling type instead of linear interpolation run the following
code. Note that this requires the <code>pyDOE</code> python package
which is not part of the standard libraries but can be installed using
<code>pip</code>.</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span>  <span class="at">--polling-file</span> stimulus.poll <span class="dt">\</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a>          <span class="at">--polling-param</span> stimulus[0].start stimulus[1].start <span class="dt">\</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a>          <span class="at">--polling-range</span> 20:60:25 50:70:25 <span class="dt">\</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a>          <span class="at">--sampling-type</span> lhs</span></code></pre></div>
<p><strong>Remarks:</strong></p>
<ul>
<li>An arbitrary number of parameters can be added to
<code>--polling-param</code>. Each parameter requires a polling-range
and the number of samples has to be the same.</li>
<li>It is checked automatically if the polling-param is actually set by
the user in the run script or in any .par file. If it is not set, an
error is thrown.</li>
<li>You can write your own python function to generate a polling file;
e.g., for parameters that cannot easily be iterated or for parameters
that are not set via the openCARP simulator (e.g., dynamically generated
meshes). <strong>If the layout of the file is similar to the files
generated by carputils, all of the following simulation functionality
including job submission on clusters will still work!</strong></li>
</ul>
<h2 id="run-a-simulation-with-the-generated-polling-file">Run a
simulation with the generated polling file</h2>
<p>To run the simulations in serial with the parameters set in the file
<code>stimulus.poll</code> execute</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span>  <span class="at">--polling-file</span> stimulus.poll</span></code></pre></div>
<p>Usually, we want to run parameter sweeps in parallel. On the desktop,
to run four simulations in parallel each using 2 processes use</p>
<div class="sourceCode" id="cb7"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span>  <span class="at">--polling-file</span> stimulus.poll <span class="at">--np</span> 8 <span class="at">--np-job</span> 2</span></code></pre></div>
<p><strong>Remarks:</strong></p>
<ul>
<li><code>--np</code> has to be a multiple of <code>--np-job</code>.
Else an error is thrown.</li>
<li>The subdirectories for each simulation are generated automatically.
To prevent this behavior you can include the parameter
<code>--simID &lt;some-simulation-id&gt;</code> in the polling file. The
system checks automatically if <code>--simID</code> is set there and if
that is the case the simulation ID in the polling file is used.</li>
<li>Alternatively, you can set
<code>job.carp(cmd, polling_subdirs=False)</code> in the run script to
prevent the generation of subdirectories in the simulation directory.
<strong>Be aware that files may be overwritten by this
mode</strong>.</li>
</ul>
<h2 id="run-parameter-sweeps-using-carputils-on-clusters">Run parameter
sweeps using carputils on clusters</h2>
<p>Most clusters allow to run parameter sweeps using their job
submission system. See for example</p>
<ul>
<li>Section 4.1.4 in the <a
href="http://www.archer.ac.uk/documentation/best-practice-guide/batch.php">Archer
Best Practice Guide</a></li>
<li>Scheduler chains in the <a
href="https://wiki.vsc.ac.at/doku.php?id=doku:slurm">VSC3 Wiki</a></li>
</ul>
<p>To run a parameter sweep on VSC3 with a total of 512 cores and 2
cores per simulation run the following command. <code>--dry</code> will
give you the opportunity to check the file before submission.</p>
<div class="sourceCode" id="cb8"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span>  <span class="at">--polling-file</span> stimulus.poll <span class="at">--np</span> 512 <span class="at">--np-job</span> 2 <span class="dt">\</span></span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true" tabindex="-1"></a>          <span class="at">--runtime</span> 24:00:00 <span class="at">--platform</span> vsc3 <span class="at">--dry</span></span></code></pre></div>
<p>To immediately submit a parameter sweep job on Archer with a total of
1152 cores and 24 cores per simulation run without
<code>--dry</code></p>
<div class="sourceCode" id="cb9"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span>  <span class="at">--polling-file</span> stimulus.poll <span class="at">--np</span> 1152 <span class="at">--np-job</span> 24 <span class="dt">\</span></span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true" tabindex="-1"></a>          <span class="at">--runtime</span> 24:00:00 <span class="at">--platform</span> archer</span></code></pre></div>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<ul>
<li>You cannot use aprun - which is used e.g., on Archer -to run more
than one application on a single node (= 24 cores) at the same
time.</li>
<li>If the polling functionality is not yet included on the cluster you
want to use, I can help to set that up.</li>
</ul>
</div>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>