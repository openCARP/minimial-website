<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Electrical Mapping</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/08_lats/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Fernando Campos <fernando.campos@kcl.ac.uk></i>
<p>This example demonstrates how to compute local activation times
(LATs) and action potential durations (APDs) of cells in a cardiac
tissue. To run the experiments of this example change directories as
follows:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/08_lats</span></code></pre></div>
<h2 id="tutorial-electrical-mapping">Electrical Mapping</h2>
<p>The wavelength of the cardiac impulse, given by the product of
conduction velocity (CV) and refractory period, is of utmost importance
for the study of arrhythmia mechanisms. The assessment of the CV as well
as the refractory period of the cardiac action potential (AP) relies on
the determination of activation and repolarization times, respectively.
Experimentally, these are usually calculated based on 1) transmembrane
voltages <span class="math inline">\(V_{\mathrm m}\)</span> measured by
glass micro-electrodes or in optical mapping; or 2) extracellular
potentials <span class="math inline">\(\phi_{\mathrm e}\)</span>
measured at the surface of the tissue.</p>
<p>The estimation of activation/repolarization times is based on an
event detector that records the instants when the selected event type
occurs. Local activation time (LAT), for instance, is usually taken as
the time of maximum <span class="math inline">\(\mathrm{d}V_{\mathrm
m}/\mathrm{d}t\)</span> or the minimum <span
class="math inline">\(\mathrm{d}\phi_{\mathrm
e}/\mathrm{d}t\)</span>:</p>
<figure>
<img src="02_08_lats_detec_LATs_derivatives.png" class="align-center"
alt="Action potential in an uncoupled cell. Upper panel: transmembrane potential V_{\mathrm m} and its time derivative \mathrm{d}V_{\mathrm m}/\mathrm{d}t. Lower panel: extracellular potential \phi_{\mathrm e} and its time derivative \mathrm{d}\phi_{\mathrm e}/\mathrm{d}t. LAT is usually taken as the time of maximum \mathrm{d}V_{\mathrm m}/\mathrm{d}t or the minimum \mathrm{d}\phi_{\mathrm e}/\mathrm{d}t." />
<figcaption aria-hidden="true">Action potential in an uncoupled cell.
Upper panel: transmembrane potential <span
class="math inline">\(V_{\mathrm m}\)</span> and its time derivative
<span class="math inline">\(\mathrm{d}V_{\mathrm
m}/\mathrm{d}t\)</span>. Lower panel: extracellular potential <span
class="math inline">\(\phi_{\mathrm e}\)</span> and its time derivative
<span class="math inline">\(\mathrm{d}\phi_{\mathrm
e}/\mathrm{d}t\)</span>. LAT is usually taken as the time of maximum
<span class="math inline">\(\mathrm{d}V_{\mathrm m}/\mathrm{d}t\)</span>
or the minimum <span class="math inline">\(\mathrm{d}\phi_{\mathrm
e}/\mathrm{d}t\)</span>.</figcaption>
</figure>
<p>Instead of using derivatives, LATs can also been estimated by the
crossing of a threshold value provided that the chosen value reflects
the sodium channel activation since they are responsible for the rising
phase of AP. One could, for instance, pick the time instant when <span
class="math inline">\(V_{\mathrm m}\)</span> crosses -10 mV with a
positive slope. Equally, one could look for crossing of <span
class="math inline">\(V_{\mathrm m}\)</span> with a negative slope to
detect repolarization events. AP duration (APD) can then be computed as
the difference between repolarization and activation times. The
detection of activation (<span
class="math inline">\(t_{\mathrm{act}}\)</span>) and repolarization
(<span class="math inline">\(t_{\mathrm{rep}}\)</span>) times based on
threshold crossing is illustrated below:</p>
<figure>
<img src="02_08_lats_detect_LATs_threshold.png" class="align-center"
alt="Activation (LAT) and repolarization times t_{\mathrm{act}} and t_{\mathrm{rep}} computed using the threshold crossing method. APD = t_{\mathrm{rep}} - t_{\mathrm{act}}." />
<figcaption aria-hidden="true">Activation (LAT) and repolarization times
<span class="math inline">\(t_{\mathrm{act}}\)</span> and <span
class="math inline">\(t_{\mathrm{rep}}\)</span> computed using the
threshold crossing method. <span class="math inline">\(APD =
t_{\mathrm{rep}} - t_{\mathrm{act}}\)</span>.</figcaption>
</figure>
<h2 id="problem-setup">Problem Setup</h2>
<p>This example will run a simulation using a 2D sheet model (1 cm x 1
cm). A transmembrane stimulus current of 100 uA/cm^2 is applied for a
period of 1 ms at the middle of the left side of the tissue. A ground
electrode is placed at the opposite side of the stimulating electrode.
Simulation of electrical activity is based on the bidomain equations.
Dynamics of the cardiac myocytes within the tissue are described using
the Modified Beeler-Router Drouhard-Roberge (DrouhardRoberge) model.
Activation times are obtained either from <span
class="math inline">\(V_{\mathrm m}\)</span> or <span
class="math inline">\(\phi_{\mathrm e}\)</span> using a threshold
crossing method, whereas repolarization can only be obtained by
threshold crossing from <span class="math inline">\(V_{\mathrm
m}\)</span>. Estimation of repolarization times from <span
class="math inline">\(\phi_{\mathrm e}\)</span> is out of the scope of
this example.</p>
<h2 id="usage">Usage</h2>
<p>To run this example, execute:</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/08_lats</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a> <span class="ex">./run.py</span> <span class="at">--help</span> </span></code></pre></div>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">--quantity</span>          Quantity/variable/signal used to compute activation time</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Options:</span> <span class="dt">{vm</span><span class="op">,</span><span class="dt">phie}</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Default:</span> vm</span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a><span class="ex">--act-threshold</span>     The threshold value for determining activation from vm or phie</span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Default:</span> <span class="at">-10</span> mV</span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a><span class="ex">--rep-threshold</span>     The threshold value for determining repolarization from vm</span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Default:</span> <span class="at">-70</span> mV</span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a><span class="ex">--show-APDs</span>         Visualize APDs instead of LATs</span></code></pre></div>
<p>If the program is run with the <code>--visualize</code> option,
meshalyzer will automatically load the computed activation sequence
(LATs). If the <code>--show_APDs</code> is given, then the computed APDs
will be loaded:</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">Visualize</span> LATs:</span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--visualize</span></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a><span class="ex">Visualize</span> APDs:</span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--show-APDs</span></span></code></pre></div>
<h2 id="interpreting-results">Interpreting Results</h2>
<p>Differences between activation sequences computed from <span
class="math inline">\(V_{\mathrm m}\)</span> or <span
class="math inline">\(\phi_{\mathrm e}\)</span> and/or with different
thresholds can be appreciated with meshalyzer
(<code>--visualize</code>). The activation sequence for the default
parameters looks like this:</p>
<figure>
<img src="02_08_lats_results_LATs_vm.png" class="align-center"
alt="Activation sequence computed from V_{\mathrm m} (--act-threshold=-10) obtained after delivering a stimulus current in the middle of the left side tissue." />
<figcaption aria-hidden="true">Activation sequence computed from <span
class="math inline">\(V_{\mathrm m}\)</span>
(<code>--act-threshold=-10</code>) obtained after delivering a stimulus
current in the middle of the left side tissue.</figcaption>
</figure>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<ul>
<li>It is important to note that while computing activation from <span
class="math inline">\(V_{\mathrm m}\)</span> -lats[].mode has to be set
to zero. This is because <span class="math inline">\(V_{\mathrm
m}\)</span> is about -85 mV in cardiac cells at rest and rapidly rises
to about +40 mV during the activation phase. This means <span
class="math inline">\(V_{\mathrm m}\)</span> will cross the threshold
with a positive slope. While computing repolarization, on the other
hand, <span class="math inline">\(V_{\mathrm m}\)</span> goes from
positive values back to the rest. The threshold will be then crossed
with a negative slope.</li>
</ul>
</div>
<figure>
<img src="02_08_lats_results_LATs_phie.png" class="align-center"
alt="Activation sequence computed from \phi_{\mathrm e} (--act-threshold=-10) obtained after delivering a stimulus current in the middle of the left side tissue." />
<figcaption aria-hidden="true">Activation sequence computed from <span
class="math inline">\(\phi_{\mathrm e}\)</span>
(<code>--act-threshold=-10</code>) obtained after delivering a stimulus
current in the middle of the left side tissue.</figcaption>
</figure>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<ul>
<li>Unlike the shape of <span class="math inline">\(V_{\mathrm
m}\)</span> which remains pretty much the same in all cells, <span
class="math inline">\(\phi_{\mathrm e}\)</span> morphology may vary
depending on its position in relation to both stimulation and ground
electrodes. Thus, LATs obtained based on <span
class="math inline">\(\phi_{\mathrm e}\)</span> might differ from those
obtained from <span class="math inline">\(V_{\mathrm m}\)</span>.</li>
<li>If the criteria for threshold crossing detection is never satisfied,
the resulting entry in the output file will be -1. See action sequence
obtained from <span class="math inline">\(\phi_{\mathrm
e}\)</span>.</li>
</ul>
</div>
<p>The computed APDs using the default parameters looks like this (use
<code>--show-APDs</code> instead of <code>--visualize</code>):</p>
<figure>
<img src="02_08_lats_results_APDs.png" class="align-center"
alt="Action potential durations computed using the default thresholds (--act-threshold=-10 and --rep-threshold=-70) after delivering a stimulus current in the middle of the left side of the tissue." />
<figcaption aria-hidden="true">Action potential durations computed using
the default thresholds (<code>--act-threshold=-10</code> and
<code>--rep-threshold=-70</code>) after delivering a stimulus current in
the middle of the left side of the tissue.</figcaption>
</figure>
<h2 id="opencarp-parameters">openCARP Parameters</h2>
<p>The relevant parameters used in the tissue-scale simulation are shown
below:</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Number of events to detect</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a><span class="ex">-num_LATs</span>  = 2</span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a><span class="co"># Event 1: activation</span></span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[0].ID</span>         = <span class="st">&#39;ACTs&#39;</span></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[0].all</span>        = 0</span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[0].measurand</span>  = 0</span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[0].threshold</span>  = <span class="at">-10</span></span>
<span id="cb5-9"><a href="#cb5-9" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[0].mode</span>       = 0</span>
<span id="cb5-10"><a href="#cb5-10" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-11"><a href="#cb5-11" aria-hidden="true" tabindex="-1"></a><span class="co"># Event 2: repolarization</span></span>
<span id="cb5-12"><a href="#cb5-12" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[1].ID</span>         = <span class="st">&#39;REPs&#39;</span></span>
<span id="cb5-13"><a href="#cb5-13" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[1].all</span>        = 0</span>
<span id="cb5-14"><a href="#cb5-14" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[1].measurand</span>  = 0</span>
<span id="cb5-15"><a href="#cb5-15" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[1].threshold</span>  = <span class="at">-70</span></span>
<span id="cb5-16"><a href="#cb5-16" aria-hidden="true" tabindex="-1"></a><span class="ex">-lats[1].mode</span>       = 1</span></code></pre></div>
<p>num_LATs referes to the number of events we want to detect. In this
example there are two events: activation and repolarization. -lats[0]
are options to compute activation while -lats[1] are options to compute
repolarization. -lats[].ID is the name of output file; -lats[].all tells
openCARP if it should detect only the first event (-lats[].all = 0),
i.e., first beat, of all of them (-lats[].all = 1); -lats[].measurand
refers to the quantity: <span class="math inline">\(V_{\mathrm
m}\)</span> (0) or <span class="math inline">\(\phi_{\mathrm e}\)</span>
(1); -lats[].threshold is the threshold value for determining the event;
finally, -lats[].mode tells openCARP to look for threshold crossing with
a either a positive or negative slope.</p>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
