<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Smooth gradient heterogeneities (ionic adjustment)</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/05E_Smooth_Gradient_Heterogeneities/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Patrick Boyle <pmjboyle@gmail.com></i>
<p>This example details how to assign a gradient of cell-scale
properties using the adjustments interface. To run the experiments of
this example change directories as follows:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/05E_Smooth_Gradient_Heterogeneities</span></code></pre></div>
<h2 id="problem-setup">Problem Setup</h2>
<p>This example will run one simulation using a 2D sheet model (1 cm x 1
cm) in which all elements and nodes are in the same imp_region[] and
gregion[] but heterogeneity in cell-scale dynamics is imposed at the
cell scale in gradient patterns using the adjustments interface.</p>
<p>By default, <span class="math inline">\(I_{Kr}\)</span> is modulated
from 0x to 5x from left to right and <span
class="math inline">\(I_{to}\)</span> is modulated from 0x to 5x from
bottom to top. Both gradients are linear. The resulting pattern of
excitation is shown below:</p>
<div id="fig-smooth-gradient-heterogeneities">
<figure>
<img src="02_05E_Smooth_Gradient_Heterogeneities_Fig1_Animation.gif"
class="align-center"
alt="Membrane voltage over time (V_m(t)) for the example in which I_{Kr} is modulated from 0x to 5x from left to right and I_{to} is modulated from 0x to 5x from bottom to top. Different-coloured stars indicate points where AP traces are shown in the graph below (fig-smooth-gradient-heterogeneities-APs)." />
<figcaption aria-hidden="true">Membrane voltage over time (<span
class="math inline">\(V_m(t)\)</span>) for the example in which <span
class="math inline">\(I_{Kr}\)</span> is modulated from 0x to 5x from
left to right and <span class="math inline">\(I_{to}\)</span> is
modulated from 0x to 5x from bottom to top. Different-coloured stars
indicate points where AP traces are shown in the graph below (<code
class="interpreted-text"
role="numref">fig-smooth-gradient-heterogeneities-APs</code>).</figcaption>
</figure>
</div>
<p>The stars are colour-coded and indicate the approximate locations
from which the action potential traces shown below were extracted:</p>
<div id="fig-smooth-gradient-heterogeneities-APs">
<figure>
<img src="02_05E_Smooth_Gradient_Heterogeneities_Fig2_Example_APs.png"
class="align-center"
alt="Action potential traces extracted from the four points indicated in fig-smooth-gradient-heterogeneities." />
<figcaption aria-hidden="true">Action potential traces extracted from
the four points indicated in <code class="interpreted-text"
role="numref">fig-smooth-gradient-heterogeneities</code>.</figcaption>
</figure>
</div>
<p>As expected, <span class="math inline">\(I_{to}\)</span> modulation
leads to an abolished notch along the bottom edge of the sheet but an
exaggerated notch along the top edge. <span
class="math inline">\(I_{kr}\)</span> modulation in the sheet leads to a
progressive shortening of action potential duration from left to right
as more repolarizing current becomes available to the cells.</p>
<h2 id="usage">Usage</h2>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span> </span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--xgrad_var</span>         Options: <span class="dt">{GCaL</span><span class="op">,</span><span class="dt">GKs</span><span class="op">,</span><span class="dt">GKr</span><span class="op">,</span><span class="dt">Gto}</span>, Default: GKr</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">parameter</span> that should be varied along the left-to-right <span class="er">(</span><span class="ex">X</span><span class="kw">)</span> <span class="ex">gradient</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--xgrad_left_scf</span>    Default: 0.0</span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">scaling</span> factor at left side of the X gradient</span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--xgrad_right_scf</span>   Default: 5.0</span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">scaling</span> factor at right side of the Y gradient</span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--xgrad_flip</span>        left becomes right, right becomes left</span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-10"><a href="#cb2-10" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--ygrad_var</span>         Options: <span class="dt">{GCaL</span><span class="op">,</span><span class="dt">GKs</span><span class="op">,</span><span class="dt">GKr</span><span class="op">,</span><span class="dt">Gto}</span>, Default: Gto</span>
<span id="cb2-11"><a href="#cb2-11" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">parameter</span> that should be varied along the bottom-to-top <span class="er">(</span><span class="ex">Y</span><span class="kw">)</span> <span class="ex">gradient</span></span>
<span id="cb2-12"><a href="#cb2-12" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--ygrad_bottom_scf</span>  Default: 0.0</span>
<span id="cb2-13"><a href="#cb2-13" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">scaling</span> factor at bottom of Y gradient</span>
<span id="cb2-14"><a href="#cb2-14" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--ygrad_top_scf</span>     Default: 5.0</span>
<span id="cb2-15"><a href="#cb2-15" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">scaling</span> factor at top of Y gradient</span>
<span id="cb2-16"><a href="#cb2-16" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--ygrad_flip</span>        down becomes up, up becomes down</span></code></pre></div>
<p>If the program is run with the <code>--visualize</code> option,
meshalyzer will automatically load the <span
class="math inline">\(V_m(t)\)</span> sequence corresponding to the run
simulation. Output files for activation and repolarisation sequences as
well as APD are produced for each simulation and can be found in the
output directory and loaded into meshalyzer.</p>
<p>The --xgrad[...] and --ygrad[...] parameters can be modified to
impose different gradients in the x and y directions (i.e.,
left-to-right and bottom-to-top, respectively) in four different
parameters: <span class="math inline">\(G_{CaL}\)</span>, <span
class="math inline">\(G_{Ks}\)</span>, <span
class="math inline">\(G_{Kr}\)</span>, and <span
class="math inline">\(G_{to}\)</span>, which correspond to the L-type
<span class="math inline">\(Ca^{2+}\)</span>, slow delayed rectifier
<span class="math inline">\(K^{+}\)</span>, rapid delayed rectifier
<span class="math inline">\(K^{+}\)</span>, and transient outward <span
class="math inline">\(K^{+}\)</span> currents, respectively.</p>
<h2 id="notes-and-precautions">Notes and Precautions</h2>
<ul>
<li>The <code>--xgrad_var</code> and <code>--ygrad_var</code> parameters
should not be set to the same value, otherwise the simulation will not
behave as expected.</li>
<li>The <code>-adjustments[]</code> interface in openCARP can only be
used to modify cell-scale properties that correspond to a state variable
in the ionic model being used.</li>
</ul>
<p>Use the --imp-info argument of <code>bench</code> to find out which
variables are eligible to bet set on a nodal basis. There are actually
two types of variables listed as <em>state variables</em>, (1) actual
state variables which describe the ionic model state and evolve with
each time step, and (2) model parameters which affect behaviour but do
not change. Thus, state variables can be intialized on a nodal basis.
Parameters which can be set on a nodal basis must be listed as
parameters, i.e., being listed up top, as well as being listed as state
variables. For example, in the case of the
<code>tenTusscherPanfilov</code> ionic model used in this example, the
following are parameters which can be set nodally:</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="op">&gt;</span> bench <span class="ex">--imp=tenTusscherPanfilov</span> <span class="at">--imp-info</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="ex">tenTusscherPanfilov:</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a><span class="ex">[...]</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a>      <span class="ex">State</span> variables:</span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a>                               <span class="ex">GCaL</span></span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a>                               <span class="ex">GKr</span></span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a>                               <span class="ex">GKs</span></span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a>                               <span class="ex">Gto</span></span></code></pre></div>
<h2 id="under-the-hood">Under the Hood</h2>
<p>In the parameter file, the following options are used to adjust nodal
values. First, if you wish to adjust N variables:</p>
<pre><code>num_adjustments = N</code></pre>
<p>For each adjustment structure, i.e.,
<code>adjustment[0], ..., adjustment[N-1]</code>, the following fields
are set:</p>
<table>
<thead>
<tr class="header">
<th>Field</th>
<th>Meaning</th>
<th>Value</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>file</td>
<td>file specifying adjustment</td>
<td>see the <code class="interpreted-text"
role="ref">vertex-adj-file</code> format</td>
</tr>
<tr class="even">
<td><p>variable</p></td>
<td><p>variable to adjust</p></td>
<td><p>There are 2 forms. For global variables
(Vm,Lambda,delLambda,Na_e,Ca_e,...) it is simply the variable. For ionic
model state variables, it takes the form <code>X.Y</code> where
<code>X</code> is the ionic model and <code>Y</code> is the
state_variable, eg., <code>tenTusscherPanfilov.Gto</code></p></td>
</tr>
<tr class="odd">
<td>dump</td>
<td>output adjusted values on grid</td>
<td>0|1</td>
</tr>
</tbody>
</table>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
