<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Region tagging</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/04_tagging/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Edward Vigmond <edward.vigmond@u-bordeaux.fr></i>
<div id="region-tagging-tutorial">
<p>In openCARP regions are used to manage the assignment of
heterogeneous tissue properties. This tutorial explains the different
approches of how regions can be defined. To run the experiments of this
example do</p>
</div>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/04_tagging</span></code></pre></div>
<h2 id="regions">Regions</h2>
<p>Regions are a general concept in openCARP for volumes which share
common properties, be they ionic, mechanical or conductivity. For a
general introduction of this concept see the <a
href="/documentation/examples/02_ep_tissue/04_tagging">region</a>
section of the manual. In the mesh files, elements may be given a region
tag as an optional field in the <code>.elem</code> file.</p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>If no region tag is specified, the element is assigned a region tag
of 0.</p>
</div>
<p>A region tag is a simply an integer associated with an element.
openCARP regions are then conglomerations of element region tags. The
following is an example of how to assign some region tags to IMP and
conductivity regions:</p>
<pre><code>imp_region[1].num_IDS = 2
imp_region[1].ID[0] = 1000 
imp_region[1].ID[1] = 1100

gregion[3].num_IDS = 5
gregion[3].ID = 100 200 201 1000 2000 </code></pre>
<p>Two slightly different syntaxes are shown for specifying the region
tags.</p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>If a region tag is not explicitly included in any region, it will be
assigned to region 0.</p>
</div>
<h2 id="dynamic-retagging">Dynamic Retagging</h2>
<p>Each element in a mesh is designated as being in a region, and
properties, be they electrical or mechanical, are assigned based on
region. It may be desired to change properties in a small volume which
is not defined in the original mesh. To avoid touching the mesh files,
dynamic tagging was introduced.</p>
<p>Essentially, a geometrical volume is defined, and all elements wthin
the volume are given a new tag. Overlaps are governed by a simple rule:
<strong>the highest region tag wins</strong>. This also implies that
your reassigned tags have to be higher than the original model tags. The
case where some but not all of an element's nodes are contained with the
tagging volume must be handled. By default, elements with at least one
node in the tagging volume are included, but ensuring that only elements
with all nodes contained can be enabled by setting an option
below.(<em>no_split_elem</em>)</p>
<p>Tag volumes can also be created by simply specifying a list of
elements within the volume.</p>
<h2 id="region-definitions">Region Definitions</h2>
<p>The number of new tag regions is specified by <code>numtagreg</code>.
For each region defined by <code>tagreg[*]</code>, the table below lists
the available fields to set. Bolded fields need to be specified and
depending on the value of <em>type</em>, different fields define the
geometrical volume. Note that the units are micrometers. See the figure
below:</p>
<table>
<thead>
<tr class="header">
<th>Field</th>
<th>Meaning</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><strong>tag</strong></td>
<td>numerical value of tag region</td>
</tr>
<tr class="even">
<td>name</td>
<td>string to identify retagged region</td>
</tr>
<tr class="odd">
<td><strong>type</strong></td>
<td>how to define region (1=sphere,2=block,3=cylinder,4=elemfile)</td>
</tr>
<tr class="even">
<td>no_elem_split</td>
<td>all nodes of an element must be contained</td>
</tr>
<tr class="odd">
<td>p0</td>
<td>first point to define volume. <em>type=1|2|3</em></td>
</tr>
<tr class="even">
<td>p1</td>
<td>second point to define volume. <em>type=2|3</em></td>
</tr>
<tr class="odd">
<td>radius</td>
<td>radius for spherical/cylindrical volumes. <em>type=1|3</em></td>
</tr>
<tr class="even">
<td><p>elemfile</p></td>
<td><p>file with elements contained in tag region. <em>type=4</em> The
file needs the extension <code>.regele</code> with the format being the
number of elements followed by one element number per line.</p></td>
</tr>
</tbody>
</table>
<p>The tagging volumes are defined according to the following
diagram:</p>
<figure>
<img src="02_04_retag-vols.png" class="align-center" style="width:50.0%"
alt="Geometry definitions for dynamic retagging volumes sphere, block and cylinder." />
<figcaption aria-hidden="true">Geometry definitions for dynamic
retagging volumes sphere, block and cylinder.</figcaption>
</figure>
<h2 id="mesher-tagging">Mesher Tagging</h2>
<p>When using the mesher program, tag volumes can be assigned to the
blocks of tissue but the rules are slightly different.</p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>With mesher, bounds are defined in units of <span
class="title-ref">cm</span>, not micrometers and the cylinder definition
is different.</p>
</div>
<p>For each tag volume defined by <code>regdef[*]</code>, the table
below lists the available fields to set. Bolded fields need to be
specified and depending on the value of <span
class="title-ref">type</span>, different fields define the geometrical
volume.</p>
<table>
<thead>
<tr class="header">
<th>Field</th>
<th>Meaning</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><strong>tag</strong></td>
<td>numerical value of the tag</td>
</tr>
<tr class="even">
<td><strong>type</strong></td>
<td>how to define tag volume (0=block,1=sphere,2=cylinder)</td>
</tr>
<tr class="odd">
<td><em>p0</em></td>
<td>first point to define volume. <em>type=0|1|2</em></td>
</tr>
<tr class="even">
<td>p1</td>
<td>UR point for block (<em>type=0</em>), or axis for cylinder which
will be normalized (<em>type=2</em>)</td>
</tr>
<tr class="odd">
<td>rad</td>
<td>radius for spherical/cylindrical volumes. <em>type=1|2</em></td>
</tr>
<tr class="even">
<td>cyllen</td>
<td>cylinder length for cylindrical volumes. <em>type=2</em></td>
</tr>
<tr class="odd">
<td>bath</td>
<td>set true for a bath region</td>
</tr>
</tbody>
</table>
<figure>
<img src="02_04_regions-mesher-cyl.png" class="align-center"
style="width:25.0%"
alt="Geometry definition for mesher cylinder. Spheres and blocks are defined as in dynamic retagging above." />
<figcaption aria-hidden="true">Geometry definition for mesher cylinder.
Spheres and blocks are defined as in dynamic retagging
above.</figcaption>
</figure>
<p>If an element was originally created as bath, it cannot be turned
into myocardium but myocardium can be turned into bath. To deal with
overlapping regions, the rules are also different from dynamic tagging.
For mesher, an element belongs to the first region which contains it.
Thus, the order of region definitions is important. An option is also
available <code>-first_reg</code>, which if set false, scans the region
definitions and assigns the last region which contains an element.</p>
<h2 id="example">Example</h2>
<p>The following options are the pertinent ones for running the example
.. code-block:: bash</p>
<blockquote>
<dl>
<dt>run.py -h</dt>
<dd>
<dl>
<dt>--mesher [{sphere,block,steps} [{sphere,block,steps} ...]]</dt>
<dd>
<p>regions implemented in mesher</p>
</dd>
<dt>--dynamic [{cylinder,sphere,block} [{cylinder,sphere,block}
...]]</dt>
<dd>
<p>regions implemented dynamically</p>
</dd>
</dl>
<p>--add-bath mesher regions are bath</p>
</dd>
</dl>
</blockquote>
<p>Multiple regions can be created in mesher or dynamically afterwards.
Start with a single region implemented in mesher</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--mesher</span> sphere <span class="at">--visualize</span></span></code></pre></div>
<p>To see the regions better, click
<code>Image/Randomly color/surfaces</code> from the menu ar at the top.
The colors are random, so if you do not like them, randomly color again
(and again ...). You should see 3 regions: the bath, the original block
of tissue, and the sherical region; To see the points in the region,</p>
<ol type="1">
<li>turn off clipping (open the clipping window under
<code>Data/Clipping</code>)</li>
<li>In the <code>Region</code> section in the control window, check
<em>only</em> the region <strong>10</strong></li>
<li>Check the <code>Vertices</code> and <code>visible</code>
buttons</li>
<li>To control the size of the points, click on <code>props</code>
beside the <em>Vertices</em> button.</li>
<li>Reduce the opacity of the surfaces (set it in
<code>Fill color</code> of the <code>Surfaces</code> tab)</li>
</ol>
<p>It should now resemble the figure below.</p>
<figure>
<img src="02_04_regions_sphpts.png" class="align-center"
style="width:50.0%" />
</figure>
<p>Try adding another sphere, this time dynamically added afterwards at
a different location</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--mesher</span> sphere <span class="at">--dynamic</span> sphere <span class="at">--visualize</span></span></code></pre></div>
<p>Experiment with adding different combinations of regions. The order
of multiple regions specified with the <code>--mesher</code> and
<code>--dynamic</code> options matters. With this script, the first
contained region is used for dynamic retagging. To see the points
contained in the regions, display vertices as above, and click
<code>Image/Randomly color/surfaces</code> to colour them differently
for each tag region.</p>
<p>For a more detailed introduction to the region concept as implemented
in openCARP see the <a
href="/documentation/examples/02_ep_tissue/04_tagging">Region Tagging
Section</a> of the manual.</p>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
