<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Adjusting wavelength to experimental data</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/03C_tuning_wavelength/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Jason Bayer <jason.bayer@ihu-liryc.fr></i>
<div id="tutorial_wavelength-tuning">
<p>This tutorial demonstrates how to adjust parameters in tissue
simulations to match experimental data for conduction velocity, action
potential duration, and wavelength. To run the experiments of this
tutorial change directories as follows:</p>
</div>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03C_tuning_wavelength</span></code></pre></div>
<h2 id="introduction">Introduction</h2>
<p>It is important that computer simulations in cardiac tissue
corroborate experimental data. In particular, conduction velocity (CV)
is the speed at which electrical waves propagate in cardiac tissue, and
action potential duration (APD) is the time duration a cardiac myocyte
repolarizes after excitation. The multiplication of CV and APD is termed
the wavelength for reentry (<span
class="math inline">\(\lambda\)</span>), which is the minimum length
cardiac tissue dimensions have to be in order to support reentry from
unidirectional conduction block. In this exercise, the user will perform
a basic example of how to adjust CV and APD in a 2D cardiac sheet of
ventricular tissue to match CV and APD from experimental data recorded
on the ventricular epicardium of human ventricles with optical
imaging.</p>
<h2 id="problem-setup">Problem setup</h2>
<p>To execute simulations of this tutorial do</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03C_tuning_wavelength</span></code></pre></div>
<h2 id="experimental-data">Experimental data</h2>
<p>The mapping of electrical activity with optical imaging on the
epicardium of human ventricles provides an accurate measurement of CV<a
href="#fn1" class="footnote-ref" id="fnref1"
role="doc-noteref"><sup>1</sup></a> and APD<a href="#fn2"
class="footnote-ref" id="fnref2" role="doc-noteref"><sup>2</sup></a>.
The data from these studies is shown below and were recorded during
baseline pacing with a cycle length of 1000 ms.</p>
<table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<thead>
<tr class="header">
<th>Tissue state</th>
<th><span class="math inline">\(CV_{l}\)</span> (cm/s)</th>
<th><span class="math inline">\(CV_{t}\)</span> (cm/s)</th>
<th><span class="math inline">\(APD_{80}\)</span> (ms)</th>
<th><span class="math inline">\({\lambda}_{l}\)</span> (cm)</th>
<th><span class="math inline">\({\lambda}_{t}\)</span> (cm)</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><blockquote>
<p>Nonfailing</p>
</blockquote></td>
<td><blockquote>
<p>92</p>
</blockquote></td>
<td><blockquote>
<p>22</p>
</blockquote></td>
<td><blockquote>
<p>340</p>
</blockquote></td>
<td><blockquote>
<p>34</p>
</blockquote></td>
<td><blockquote>
<p>8</p>
</blockquote></td>
</tr>
</tbody>
</table>
<h2 id="d-cable-model">1D cable model</h2>
<p>A 1.5 cm cable of epicardial ventricular myocytes is used to
initially adjust CV and APD to experimentally derived values. The model
domain was discretized with linear finite elements with an average edge
length of 0.02 cm.</p>
<h2 id="d-sheet-model">2D sheet model</h2>
<p>A 1.5 cm x 1.5 cm sheet of epicardial tissue is used to verify CV and
APD derived in the 1D cable model. The model domain is discretized using
quadrilateral element finite elements with an average edge length of
0.02 cm, and a longitudinal fiber direction in each element parallel to
the X-axis of the sheet. The mesh was generated using the command below
according to the mesher tutorial.</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./mesher</span> <span class="at">-size[0]</span> 1.5 <span class="at">-size[1]</span> 1.5 <span class="at">-size[2]</span> 0.0 <span class="at">-resolution[0]</span> 200.0 <span class="at">-resolution[1]</span> 200.0 <span class="at">-resolution[2]</span> 0.0</span></code></pre></div>
<h2 id="ionic-model">Ionic model</h2>
<p>For this example, we use the most recent version of the ten Tusscher
ionic model for human ventricular myocytes<a href="#fn3"
class="footnote-ref" id="fnref3" role="doc-noteref"><sup>3</sup></a> .
This ionic model is labeled tenTusscherPanfilov in openCARP's LIMPET
library. After a quick literature search, one will find that the slow
outward rectifying potassium current <span
class="math inline">\(I_{K_{s}}\)</span> is heterogeneous across the
human ventricular wall<a href="#fn4" class="footnote-ref" id="fnref4"
role="doc-noteref"><sup>4</sup></a>. Therefore, the maximal conductance
<span class="math inline">\(G_{K_{s}}\)</span> of <span
class="math inline">\(I_{K_{s}}\)</span> is adjusted in order to match
the APD=340 ms derived experimentally. Note, the default value for <span
class="math inline">\(G_{K_{s}}\)</span> in epicardial ventricular
myocytes is 0.392 nS/pF.</p>
<h2 id="pacing-protocol">Pacing protocol</h2>
<p>The left side of the 1D cable model and the center of the 2D sheet
model is paced with 5-ms-long stimuli at twice capture amplitude for a
cycle length and number of beats chosen by the user.</p>
<h2 id="conduction-velocity">Conduction velocity</h2>
<p>To determine initial conditions for the tissue conductivities along
(<span class="math inline">\(\sigma_{il}\)</span>, <span
class="math inline">\(\sigma_{el}\)</span>) and transverse (<span
class="math inline">\(\sigma_{it}\)</span>, <span
class="math inline">\(\sigma_{et}\)</span>) the fibers in the models,
tuneCV is used as described in the tutorial Tuning Conduction
Velocities. The commands to obtain the two conductivities are listed
below.</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./tuneCV</span> <span class="at">--converge</span> true <span class="at">--tol</span> 0.0001 <span class="at">--velocity</span> 0.92 <span class="at">--model</span> tenTusscherPanfilov <span class="at">--sourceModel</span> monodomain <span class="at">--resolution</span> 200.0</span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a><span class="ex">./tuneCV</span> <span class="at">--converge</span> true <span class="at">--tol</span> 0.0001 <span class="at">--velocity</span> 0.22 <span class="at">--model</span> tenTusscherPanfilov <span class="at">--sourceModel</span> monodomain <span class="at">--resolution</span> 200.0</span></code></pre></div>
<p>The initial conductivities resulting from tuneCV are <span
class="math inline">\(\sigma_{il}=0.4134\)</span> S/m, <span
class="math inline">\(\sigma_{el}=1.4849\)</span> S/m, <span
class="math inline">\(\sigma_{it}=0.0379\)</span> S/m, and <span
class="math inline">\(\sigma_{et}=0.1361\)</span> S/m.</p>
<div id="tutorial-electrical-mapping">
<p>To compute CV, activation times are computed for the last beat of the
pacing protocol using the openCARP option LATs (see tutorial on <a
href="/documentation/examples/02_ep_tissue/08_lats">electrical
mapping</a>), with activation time recorded at the threshold crossing of
-10 mV. CV is then computed along the cable by taking the difference in
activation times at the locations 1.0 cm and 0.5 cm divided by the
distance between the two points. For the sheet model, CV is computed
along the longitudinal and transverse fiber directions by taking the
difference in activation times at the locations illustrated in the
figure below. Specifically, <span class="math inline">\(CV_{l}\)</span>
= 0.25/(L2-L1) and <span class="math inline">\(CV_{t}\)</span> =
0.25/(T2-T1). The locations of L1 and T1 are 0.25 cm away from the
tissue center, and L2 and T2 are 0.5 cm away from the tissue center.</p>
</div>
<div id="fig-cv_calculation-2D">
<figure>
<img src="02_03C_cv_calc.png" class="align-center"
style="width:50.0%" />
</figure>
</div>
<h2 id="action-potential-duration">Action potential duration</h2>
<p>Activation potential duration is computed at 80% repolarization
(<span class="math inline">\(APD_{80}\)</span>) according to <a
href="">[Bayer2016]</a>. This is achieved by using the igbutils function
igbapd as illustrated below.</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./igbapd</span> <span class="at">--repol</span><span class="op">=</span>80 <span class="at">--vup</span><span class="op">=</span>-10 <span class="at">--peak-value</span><span class="op">=</span>plateau ./vm.igb </span></code></pre></div>
<h2 id="usage">Usage</h2>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span> </span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--dimension</span>         Options: <span class="dt">{cable</span><span class="op">,</span><span class="dt">sheet}</span>, Default: cable</span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Choose</span> cable for quick 1D parameter adjustments, </span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a>                      <span class="cf">then</span> <span class="ex">the</span> 2D sheet to verify the adjustments.</span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--GKs</span>               Default: 0.392  nS/pF</span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Maximal</span> conductance of IKs</span>
<span id="cb6-7"><a href="#cb6-7" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--Gil</span>               Default: 0.3544 S/m</span>
<span id="cb6-8"><a href="#cb6-8" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Intracellular</span> longitudinal tissue conductivity</span>
<span id="cb6-9"><a href="#cb6-9" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--Gel</span>               Default: 1.27 S/m</span>
<span id="cb6-10"><a href="#cb6-10" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Extracellular</span> longitudinal tissue conductivity</span>
<span id="cb6-11"><a href="#cb6-11" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--Git</span>               Default: 0.024 S/m</span>
<span id="cb6-12"><a href="#cb6-12" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Intracellular</span> transverse tissue conductivity</span>
<span id="cb6-13"><a href="#cb6-13" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--Get</span>               Default: 0.0862 S/m</span>
<span id="cb6-14"><a href="#cb6-14" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Extracellular</span> transverse tissue conductivity</span>
<span id="cb6-15"><a href="#cb6-15" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--nbeats</span>            Default: 3</span>
<span id="cb6-16"><a href="#cb6-16" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Number</span> of beats for pacing protocol. This number</span>
<span id="cb6-17"><a href="#cb6-17" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">should</span> be much larger to achieve steady-state</span>
<span id="cb6-18"><a href="#cb6-18" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--bcl</span>               Default: 1000</span>
<span id="cb6-19"><a href="#cb6-19" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Basic</span> cycle length for pacing protocol</span>
<span id="cb6-20"><a href="#cb6-20" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--timess</span>            Default: 1000</span>
<span id="cb6-21"><a href="#cb6-21" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Time</span> before applying pacing protocol</span></code></pre></div>
<p>After running run.py, the results for APD, CV, and (<span
class="math inline">\(\lambda\)</span>) can be found in the file
adjustment_results.txt within the output subdirectory for the
simulation.</p>
<p>If the program is ran with the <code>--visualize</code> option,
meshalyzer will automatically load the <span
class="math inline">\(V_m(t)\)</span> for the last beat of the pacing
protocol. Output files for activation and APD are also produced for each
simulation and can be found in the output directory and loaded into
meshalyzer.</p>
<h2 id="tasks">Tasks</h2>
<ol type="1">
<li>Determine the value for IKs to obtain the experimental value for APD
in the cable.</li>
<li>Place this value in the sheet model to verify the model is behaving
in the same manner as the simple cable model.</li>
<li>Determine a set of Gil and Gel (keep ratio for Gil/Git the same) so
that both the longitudinal and transverse wavelengths are less than 8
cm.</li>
</ol>
<h2 id="solutions-to-the-tasks">Solutions to the tasks</h2>
<ol type="1">
<li>A GKs=0.25 nS/pF is needed to obtain the APD of 340 ms.</li>
</ol>
<div class="sourceCode" id="cb7"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--GKs</span> 0.25 <span class="at">--dimension</span> cable</span></code></pre></div>
<ol start="2" type="1">
<li>The following command produces the same APD in the sheet model.</li>
</ol>
<div class="sourceCode" id="cb8"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--GKs</span> 0.25 <span class="at">--dimension</span> sheet</span></code></pre></div>
<ol start="3" type="1">
<li>The following commands produce longitudinal and transverse
wavelengths less than 8 cm.</li>
</ol>
<div class="sourceCode" id="cb9"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--GKs</span> 0.25 <span class="at">--Gil</span> 0.02 <span class="at">--Git</span> 0.0135 <span class="at">--dimension</span> cable</span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb9-3"><a href="#cb9-3" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--GKs</span> 0.25 <span class="at">--Gil</span> 0.02 <span class="at">--Git</span> 0.0135 <span class="at">--dimension</span> sheet</span></code></pre></div>
<p><strong>References</strong></p>
<section class="footnotes footnotes-end-of-document"
role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>Glukhov AV, Fedorov VV, Kalish PW,
Ravikumar VK, Lou Q, Janks D, Schuessler RB, Moazami N, Efimov IR.
<strong>Conduction remodeling in human end-stage nonischemic left
ventricular cardiomyopathy.</strong> <em>Circulation</em>,
125(15):1835-1847, 2012. <a
href="https://www.ncbi.nlm.nih.gov/pubmed/22412072">[Pubmed]</a><a
href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn2" role="doc-endnote"><p>Glukhov AV, Fedorov VV, Lou Q,
Ravikumar VK, Kalish PW, Schuessler RB, Moazami N, and Efimov IR.
<strong>Transmural dispersion of repolarization in failing and
nonfailing human ventricle.</strong> <em>Circ Res</em>, 106(5):981-991,
2010. <a
href="https://www.ncbi.nlm.nih.gov/pubmed/20093630">[Pubmed]</a><a
href="#fnref2" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn3" role="doc-endnote"><p>ten Tusscher KHWJ, Panfilov AV.
<strong>Alternans and spiral breakup in a human ventricular tissue
model.</strong> <em>Am J Physiol Heart Circ Physiol</em>,
291(3):H1088-H1100, 2006. <a
href="https://www.ncbi.nlm.nih.gov/pubmed/16565318">[Pubmed]</a><a
href="#fnref3" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn4" role="doc-endnote"><p>Pereon Y, Demolombe S, Baro I, Drouin
E, Charpentier F, Escande D. <strong>Differential expression of kvlqt1
isoforms across the human ventricular wall.</strong> <em>Am J Physiol
Heart Circ Physiol</em>, 278(6):H1908-H1915, 2000. <a
href="https://www.ncbi.nlm.nih.gov/pubmed/10843888">[Pubmed]</a><a
href="#fnref4" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
