---
description: 'This tutorial introduces the concepts of region-based and gradient-based
  heterogeneities for assigning spatially varying properties '
image: 02_05A_Regions_vs_Gradients_Fig4_APDComparison.png
title: Region vs. gradient heterogeneities
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Concept: regions vs. gradients</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/05A_Regions_vs_Gradients/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Patrick Boyle <pmjboyle@gmail.com></i>
<p>This example introduces the concepts of region-based and
gradient-based heterogeneities for assigning spatially varying
properties such as tissue conductivities and ion channel conductances.
To run the experiments of this example do</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/05A_Regions_vs_Gradients</span></code></pre></div>
<h2 id="conceptual-overview">Conceptual overview</h2>
<ol>
<li><p><strong>Region-wise assignment of electrophysiological
properties</strong></p>
<p>In any particular finite element mesh, each element is part of a
region defined by its tag (see <a
href="/documentation/examples/02_ep_tissue/04_tagging">defining
regions</a>). When cell- or tissue-scale properties are assigned on a
region-wise basis, the property in question is assigned homogeneously to
all nodes and elements that belong to that region.</p>
<p>One very important thing to note is that finite element nodes along
the boundary between two different element regions will be assigned by
openCARP to being in one region or the other. The system responsible for
deciding which boundary nodes belong to which regions is predictible but
not necessarily intuitive. When using region-based tagging, it is
important to remain mindful of the consequences of this process.
Detailed information is available in the example on <a
href="/documentation/examples/02_ep_tissue/05c_cellular_dynamics_heterogeneity">Cellular
Dynamics Heterogeneity</a>.</p></li>
<li><p><strong>Gradient-wise assignment of electrophysiological
properties</strong></p>
<p>As an alternative to the above-described approach, sometimes it is
useful or relevant to assign properties on a node-wise basis. For
example, the value of an ion channel conductance at various points
across the myocardial wall might vary linearly between two values as a
function of distance from the epicardium to the endocardium. This
approach utilizes the openCARP adjustments interface (see adjustments)
and is well-suited for defining smooth variations in
electrophysiological properties. Tagging is a poor option for this type
of problem, since it might require a large number of tags OR different
tag-wise division for gradients in different properties (e.g., a
transmural gradient in one conductance and an apicobasal gradient in
another).</p></li>
</ol>
<h2 id="problem-setup">Problem Setup</h2>
<p>This example will run two simulations using a 2D sheet model (1 cm x
1 cm) in which GKr and GKs values are smaller at the bottom of the sheet
(y = 0) and larger at the top of the sheet (y = 1 cm). In the first
simulation, the domain will be subdivided into four regions, each one
with a distinct scalar multiplier for GKr and GKs:</p>
<figure>
<img
src="05a_regions_vs_gradients/02_05A_Regions_vs_Gradients_Fig1_RegionWiseGKr+GKs.png"
class="align-center"
alt="Region-wise assignment of properties. Tissue is subdivided into four regions each of which is assigned different properties." />
<figcaption aria-hidden="true">Region-wise assignment of properties.
Tissue is subdivided into four regions each of which is assigned
different properties.</figcaption>
</figure>
<p>In the second simulation, the domain will consist of one unified
region but the GKr and GKs values will be scaled continuously (via
linear gradient) from the bottom to the top:</p>
<figure>
<img
src="05a_regions_vs_gradients/02_05A_Regions_vs_Gradients_Fig2_GradientWiseGKr+GKs.png"
class="align-center"
alt="Gradient-wise assignment of properties. There is only one tissue region and GKr/GKs values vary continuously from low (bottom) to high (top)." />
<figcaption aria-hidden="true">Gradient-wise assignment of properties.
There is only one tissue region and GKr/GKs values vary continuously
from low (bottom) to high (top).</figcaption>
</figure>
<h2 id="usage">Usage</h2>
<p>To run the experiments of this example do</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/05A_Regions_vs_Gradients</span></code></pre></div>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--min_scf</span>     = 0.1      <span class="co"># lower bound scaling factor (min = 0.000, max = 0.999)</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--max_scf</span>     = 2.0      <span class="co"># upper bound scaling factor (min = 1.000, max = 10.00)</span></span></code></pre></div>
<p>If the program is run with the <code>--visualize</code> option,
meshalyzer will automatically load a map showing the difference in
action potential durations between the two simulations:</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--visualize</span></span></code></pre></div>
<h2 id="interpreting-results">Interpreting Results</h2>
<p>With the default parameters, the program will perform the two
simulations as described above. When the vm.igb files are visualized, it
will be apparent that the depolarisation patterns are approximately the
same but the repolarisation sequences are markedly different due to the
variable representations of GKr/GKs heterogeneity. The side-by-side
animation shown here is for the vm sequences produced by the test using
the default parameters:</p>
<div id="fig-region-vs-gradient-Vm-comparison">
<figure>
<img
src="05a_regions_vs_gradients/02_05A_Regions_vs_Gradients_Fig3_VmComparison.gif"
class="align-center"
alt="Comparision of membrane voltage over time (V_m(t)) for Region-wise (left) and Gradient-wise (right) variability in GKr/GKs." />
<figcaption aria-hidden="true">Comparision of membrane voltage over time
(<span class="math inline">\(V_m(t)\)</span>) for Region-wise (left) and
Gradient-wise (right) variability in GKr/GKs.</figcaption>
</figure>
</div>
<p>The program will also produce APD.dat files in the relevant
simulation subdirectories, which can also be visualized in meshalyzer to
appreciate the exact distributions of APD:</p>
<div id="fig-region-vs-gradient-APD-comparison">
<figure>
<img
src="05a_regions_vs_gradients/02_05A_Regions_vs_Gradients_Fig4_APDComparison.png"
class="align-center"
alt="Comparision of action potential duration for Region-wise (left) and Gradient-wise (right) variability in GKr/GKs." />
<figcaption aria-hidden="true">Comparision of action potential duration
for Region-wise (left) and Gradient-wise (right) variability in
GKr/GKs.</figcaption>
</figure>
</div>
<p>Notably, although the effect on repolarisation is definitely more
pronounced than that on depolarisation, the resulting differences in APD
are subtle, as shown by the maps that can be seen using the --visualize
function:</p>
<div id="fig-Region-vs-Gradient-DeltaAPD">
<figure>
<img
src="05a_regions_vs_gradients/02_05A_Regions_vs_Gradients_Fig5_DeltaAPD.png"
class="align-center"
alt="Difference in action potential duration (APD) between region-based and gradient based repolarisation heterogeneity" />
<figcaption aria-hidden="true">Difference in action potential duration
(APD) between region-based and gradient based repolarisation
heterogeneity</figcaption>
</figure>
</div>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>