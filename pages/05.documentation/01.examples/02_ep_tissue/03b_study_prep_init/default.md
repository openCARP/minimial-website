---
description: This tutorial demonstrates how to initialize a cardiac tissue with state
  variables obtained from a single-cell stimulation
image: 02_03B_study_prep_init_sv_init_vs_prepace.png
title: Init tissue from cell
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Limit cycle initialization</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/03B_study_prep_init/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Fernando Campos <fernando.campos@kcl.ac.uk></i>
<p>This tutorial demonstrates how to initialize a cardiac tissue with
state variables obtained from a single-cell stimulation. To run the
experiments of this tutorial change directories as follows:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03B_study_prep_init</span></code></pre></div>
<h2 id="conceptual-overview">Conceptual Overview</h2>
<p>Similar to in-vitro experiments, cardiac tissue is usually stabilized
by pacing it at basic cycle length (BCL) in order to reduce beat-to-beat
changes. However, stabilization might require a few hundred beats which
can take days or even weeks to complete in organ-scale models. Unlike
tissue simulations, pacing an isolated cell until it achieves its stable
cycle limit is computationally feasible. In order to save computational
efforts, the single-cell model state variables (<span
class="math inline">\(V_{\mathrm m}\)</span>, channel gating variables,
etc.) at the end of the pacing protocol can be stored and used to
initialize a tissue model. This initialization procedure is equivalent
to pacing the entire tissue model in a space-clamped mode. Although this
procedure is computationally cheap, it is biologically unrealistic since
it ignores any differences due to the activation sequence of a previous
beat. If information on the activation sequence in the cardiac tissue is
available before hand, it can be used in combination with the
single-cell initialization method to overcome this limitation.</p>
<h2 id="problem-setup">Problem Setup</h2>
<p>This example will run a simulation using a 2D sheet model (1 cm x 1
cm) in which all cells are initialized with single-cell model states
stored in file <code>INIT.sv</code>. The file contains initial
conditions of all ordinary differential equations (ODEs) describing
channel gating and ionic concentrations in the chosen cellular model.
The initial conditions are obtained after pacing the cell model at a
prescribed BCL for a number N of beats.</p>
<h2 id="usage">Usage</h2>
<p>To run this tutorial :</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03B_study_prep_init</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a> <span class="ex">./run.py</span> <span class="at">--help</span> </span></code></pre></div>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">--model</span>             Model of the cellular action potential</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Options:</span> {DrouhardRoberge, tenTusscherPanfilov}</span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Default:</span> DrouhardRoberge</span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a><span class="ex">--init-method</span>       Method to initialize the tissue model</span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Options:</span> {None, sv_init or prepace}</span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Default:</span> None</span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a><span class="ex">--bcl</span>               Basic cycle length for repetitive single-cell stimulation</span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Default:</span> 500 ms <span class="er">(</span><span class="ex">2</span> Hz<span class="kw">)</span> </span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a><span class="ex">--npls</span>              Number of pulses applied to the single-cell with the given bcl</span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a>                    <span class="ex">Default:</span> 10</span></code></pre></div>
<p>If the program is run with the <code>--visualize</code> option,
meshalyzer will automatically load transmembrane potentials <span
class="math inline">\(V_{\mathrm m}\)</span>:</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--visualize</span></span></code></pre></div>
<h2 id="key-parameters">Key Parameters</h2>
<p>The key parameter is <code>--init-method</code>. This will be set to
one of three possible values in order to initialize the 2D sheet
model:</p>
<ul>
<li>none: all cells are initialized with the cellular model default
states that are hard coded in openCARP</li>
<li>sv_init: all cells are initialized with the cellular model states
stored in file <code>INIT.sv</code>. Model states are computed here by
running a single-cell simulation, where the cell is paced
<code>npls</code> (default: 10 pulses) times with the prescribed
<code>bcl</code> (default: 500ms). Model states are taken at the end of
the simulation protocol.</li>
<li>prepace: unlike in the sv_init initialization method, where state
variables are saved into a file at a prescribed time instant (usually at
the end of a single-cell pacing protocol), in the prepace method cells
are initialized with model states taken at different time instants
depending on their a priori known activation time. The difference
between the two methods is illustrated below:</li>
</ul>
<figure>
<img
src="03b_study_prep_init/02_03B_study_prep_init_sv_init_vs_prepace.png"
class="align-center"
alt="Tissue initialization methods. A) A 1D tissue model with all cells initialized with the same V_{\mathrm m} as well as other model state variables (not shown) taken at the end of the single-cell simulation. B) The same 1D tissue as in A) but cells are initialized with V_{\mathrm m} and other state variables (not shown) taken at different time instants. The time instants are determined based on the activation sequence know before hand: cell 1 activates at 0, cell 2 at t1, ..., and finally cell 5 at time t4." />
<figcaption aria-hidden="true">Tissue initialization methods. A) A 1D
tissue model with all cells initialized with the same <span
class="math inline">\(V_{\mathrm m}\)</span> as well as other model
state variables (not shown) taken at the end of the single-cell
simulation. B) The same 1D tissue as in A) but cells are initialized
with <span class="math inline">\(V_{\mathrm m}\)</span> and other state
variables (not shown) taken at different time instants. The time
instants are determined based on the activation sequence know before
hand: cell 1 activates at 0, cell 2 at t1, ..., and finally cell 5 at
time t4.</figcaption>
</figure>
<h2 id="interpreting-results">Interpreting Results</h2>
<p>The difference between the results produced with each initialization
option (i.e.: none, sv_init and prepace) can be appreciated by looking
at the spatial distribution of <span class="math inline">\(V_{\mathrm
m}\)</span> at time t = 0 with meshalyzer
(<code>--visualize</code>):</p>
<figure>
<img src="03b_study_prep_init/02_03B_study_prep_init_Vm.png"
class="align-center"
alt="Spatial distribution of V_{\mathrm m} at the beggining of the simulation (t = 0 ms). A) All cells in the tissue are initialized with default model states of the DrouhardRoberge cell model. B) All cells in the tissue are initialized with the same model states in file INIT.sv. the same model states in file INIT.sv. C) Cells in the tissue are initialized with model states taken at different time instants depending on their a priori known activation time." />
<figcaption aria-hidden="true">Spatial distribution of <span
class="math inline">\(V_{\mathrm m}\)</span> at the beggining of the
simulation (t = 0 ms). A) All cells in the tissue are initialized with
default model states of the <code>DrouhardRoberge</code> cell model. B)
All cells in the tissue are initialized with the same model states in
file <code>INIT.sv</code>. the same model states in file
<code>INIT.sv</code>. C) Cells in the tissue are initialized with model
states taken at different time instants depending on their a priori
known activation time.</figcaption>
</figure>
<ul>
<li>A) By default (<code>--init-method=none</code>), <span
class="math inline">\(V_{\mathrm m}\)</span> at t = 0 ms will be
-86.9269 mV for all cells in the tissue. This is because -86.9269 is the
default initial condition of <span class="math inline">\(V_{\mathrm
m}\)</span> in the <code>DrouhardRoberge</code> model (the initial
condition of <span class="math inline">\(V_{\mathrm m}\)</span> in the
<code>tenTusscherPanfilov</code> model is -86.2).</li>
<li>B) By running with the <code>--init-method=sv_init</code> option,
<span class="math inline">\(V_{\mathrm m}\)</span> at t = 0 ms will be
-85.7103 mV for all cells in the tissue. This is because -85.7103 is the
initial condition of <span class="math inline">\(V_{\mathrm m}\)</span>
in file <code>INIT.sv</code>, all model states were obtained after
pacing the <code>DrouhardRoberge</code> model for 10 times with a bcl of
500 ms.</li>
<li>C) By running with the <code>--init-method=prepace</code> option,
<span class="math inline">\(V_{\mathrm m}\)</span> at t = 0 ms will vary
between -85.7108 mV (left lower corner) and -85.6336 mV (right upper
corner). This is because, unlike the previous methods where all cells
are initialized with the same cellular model states, cells are
initialized according with model states obtained at different time
instants (see above) according to the activation sequence in file
<code>LATs.dat</code>:</li>
</ul>
<figure>
<img src="03b_study_prep_init/02_03B_study_prep_init_prepace_LATs.png"
class="align-center"
alt="Activation sequence used to prepace the cells in the tissue." />
<figcaption aria-hidden="true">Activation sequence used to prepace the
cells in the tissue.</figcaption>
</figure>
<p>Although the differences in <span class="math inline">\(V_{\mathrm
m}\)</span> between the three initialization methods seem negligible,
they can be substantial in other state variables of the ionic model with
a slower time decay such as the intracellular calcium <span
class="math inline">\(Ca_{\mathrm i}\)</span>. <span
class="math inline">\(Ca_{\mathrm i} = 0.30\)</span> if
<code>--init-method=none</code> and <span
class="math inline">\(Ca_{\mathrm i} = 0.172033\)</span> (see file
<code>INIT.sv</code>) if <code>--init-method=sv_init</code> with the bcl
= 500 ms and npls = 10. These differences increase for all state
variables as the bcl is reduced.</p>
<h2 id="opencarp-parameters">openCARP Parameters</h2>
<p>The relevant parameters used in the tissue-scale simulation are shown
below:</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Model of the cellular action potential</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a><span class="ex">-imp_region[0].im</span> = model</span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a><span class="co"># Used by the sv_init method</span></span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a><span class="ex">-imp_region[0].im_sv_init</span> = <span class="st">&#39;INIT.sv&#39;</span></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true" tabindex="-1"></a><span class="co"># Used by the prepace method</span></span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true" tabindex="-1"></a><span class="ex">-prepacing_beats</span> = 10</span>
<span id="cb5-9"><a href="#cb5-9" aria-hidden="true" tabindex="-1"></a><span class="ex">-prepacing_lats</span>  = <span class="st">&#39;LATs.dat&#39;</span></span>
<span id="cb5-10"><a href="#cb5-10" aria-hidden="true" tabindex="-1"></a><span class="ex">-prepacing_bcl</span>   = 500</span></code></pre></div>
<p>imp_region[] is used to assign a model of cellular action potential
to all cells in the tissue. If the <code>sv_init</code> initialization
method is choosen, then the tissue will be initialized with cellular
model states in file <code>INIT.sv</code>. However, if the
<code>prepace</code> method is choosen, then the cell model in
imp_region[] will be paced <code>npls</code> times with the given
<code>bcl</code>. Tissue activation times in file <code>LATs.dat</code>
are used to guide state set-up for prepacing.</p>
<p>Once the initial conditions are prescribed, the tissue simulation is
started. In this example, a transmembrane current of 100 uA/cm^2 is
applied for a period of 1 ms to the cells located at the middle of the
left side of the tissue.</p>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>