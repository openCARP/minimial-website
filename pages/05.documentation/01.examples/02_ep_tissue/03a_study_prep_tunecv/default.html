<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Tuning velocities and anisotropy ratios</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/03A_study_prep_tuneCV/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Caroline Mendonca Costa <caroline.mendonca-costa@kcl.ac.uk></i>
<p>To run the experiments of this tutorial do</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03A__study_prep_tuneCV</span></code></pre></div>
<h2 id="concept">Concept</h2>
<p>This tutorial introduces the theoretical background for the
relationship between conduction velocity and tissue conductivity and how
a iterative method to tune the conductivity values to yield a desired
velocity for a given setup can be derived. In addition, the definition
of anisotropy ratio and its impact on simulation results are presented.
A simple strategy to enforce desired anisotropy ratio and conduction
velocities is also presented.</p>
<h2 id="tuning-conduction-velocity">Tuning conduction velocity</h2>
<h2
id="the-relationship-between-conduction-velocity-and-conductivity">The
relationship between conduction velocity and conductivity</h2>
<p>A minimum requirement in modeling studies which aim at making
case-specific predictions on ventricular electrophysiology is that
activation sequences are carefully matched. Conduction velocity in the
ventricles is orthotropic and may vary in space, thus profoundly
influencing propagation patterns.</p>
<p>As described in Section <a
href="https://carp.medunigraz.at/carputils/cme-manual.html#tissue-scale">[Tissue
scale]</a>, the monodomain model <a
href="https://carp.medunigraz.at/carputils/cme-manual.html#monodomain-equation">[1]</a>
is equivalent to the bidomain model <a
href="https://carp.medunigraz.at/carputils/cme-manual.html#equation-eq-bidm">[2]</a>
if the monodomain conductivity tensor is given by the harmonic mean
tensor, <span class="math inline">\({\sigma_m}\)</span></p>
<p><span class="math display">\[\mathbf{\sigma_m} =
(\mathbf{\sigma_i}+\mathbf{\sigma_e})*(\mathbf{\sigma_i}*\mathbf{\sigma_e})^{-1}\]</span></p>
<p>Conduction velocity, CV , is not a parameter in the bidomain
equations <a
href="https://carp.medunigraz.at/carputils/cme-manual.html#equation-eq-bidm">[2]</a>
and as such cannot be directly parametrized. However, assuming a
continuously propagating planar wavefront along a given direction, <span
class="math inline">\(\zeta\)</span>, one can derive a proportionality
relationship<a href="#fn1" class="footnote-ref" id="fnref1"
role="doc-noteref"><sup>1</sup></a> between <span
class="math inline">\(CV_\zeta\)</span> and <span
class="math inline">\(\sigma_{m\zeta}\)</span></p>
<p><span class="math display">\[CV_\zeta \propto
\sqrt{{\sigma_{m\zeta}}}\]</span></p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>For simplicity, the surface-to-volume ratio, <span
class="math inline">\(\beta\)</span>, was ommited in this tutorial, as
it is kept constant when tuning conductivities<a href="#fn2"
class="footnote-ref" id="fnref2" role="doc-noteref"><sup>2</sup></a></p>
</div>
<h2
id="conduction-velocity-as-a-function-of-simulation-parameters">Conduction
velocity as a function of simulation parameters</h2>
<p>Experimental measurements of conductivity values are scarce and the
variation in measured values across studies is vast. These uncertainties
inevitably arise due to the significant degree of biological variation
and the substantial errors in the measurement techniques themselves.
Modeling and technical uncertainties may also have an impact on model
predictions. Particularly, CV depends on the specific model used to
describe cellular dynamics, <span
class="math inline">\(I_{ion}\)</span>, the spatial discretization,
<span class="math inline">\(\Delta_x\)</span>, and on several
implementation choices including the time-stepping scheme used, which we
represent as <span class="math inline">\(\xi\)</span>. Thus, CV can be
represented as a function</p>
<p><span class="math display">\[CV_\zeta =
CV_\zeta({\sigma_{m\zeta}},I_{ion},\Delta_x,\xi)\]</span></p>
<h2 id="iterative-scheme-for-conduction-velocity-tuning">Iterative
scheme for conduction velocity tuning</h2>
<p>In most practical scenarios, <span
class="math inline">\(I_{ion}\)</span>, <span
class="math inline">\(\Delta_x\)</span>, and <span
class="math inline">\(\xi\)</span> are parameters defined by users in
the course of selecting a simulation software, an ionic model and a
provided mesh to describe the geometry. Thus, only <span
class="math inline">\({\sigma_{m\zeta}}\)</span> is left which can be
tuned to achieve a close match between the pre-specified conduction
velocity, <span class="math inline">\(CV_\zeta\)</span>, and the
velocity, <span class="math inline">\(\bar{CV_\zeta}\)</span>, predicted
by the simulation.</p>
<p>Using <code class="interpreted-text" role="eq">equ-sigma-cv</code>,
and <code class="interpreted-text" role="eq">equ-sigma-m</code> one can
find unique monodomain conductivities along all axes <span
class="math inline">\(\zeta\)</span>, which yield the prescribed
conduction velocities, <span class="math inline">\(CV_\zeta\)</span>, by
iteratively refining conductivities based on <span
class="math inline">\(\bar{CV_\zeta}\)</span>, measured in simple 1D
cable simulations. The iterative update scheme we propose is given
as</p>
<div id="fig-algorithm">
<figure>
<img src="02_03A_tuneCV-algorithm.png" class="align-center"
style="width:40.0%"
alt="Iterative update scheme to tune conductivity values based on a prescribed velocity CV_\zeta" />
<figcaption aria-hidden="true">Iterative update scheme to tune
conductivity values based on a prescribed velocity <span
class="math inline">\(CV_\zeta\)</span><a href="#fn3"
class="footnote-ref" id="fnref3"
role="doc-noteref"><sup>3</sup></a></figcaption>
</figure>
</div>
<p>The algorithm is implemented as follows. Given an initial
conductivity "guess", <span class="math inline">\({\sigma_i}^0\)</span>
and <span class="math inline">\({\sigma_e}^0\)</span> and all other
simulation parameters defined by <span
class="math inline">\(I_{ion}\)</span>, <span
class="math inline">\(\Delta_x\)</span>, and <span
class="math inline">\(\xi\)</span>, a simulation is run using a 1D cable
and the conduction velocity is computed, as described in the figure
below:</p>
<div id="fig-setup">
<figure>
<img src="02_03A_tuneCV-Setup1D.png" class="align-center"
style="width:50.0%"
alt="Setup for convergence testing. A pseudo 1D cable is created, which is in fact a 1 element tick cable of hexahedral elements, where h is the edge length of each hexahedral, which also defines the spatial resolution. Propagation is initiated by applying a transmembrane stimulus current at the left corner. T_0, and T_1 correspond to wavefront arrival times recorded at locations x_0 = -0.25 cm and x_1 = 0.25 cm, respectively. CV is computed as CV = (x_1 - x_0)/(T_1 - T_0)." />
<figcaption aria-hidden="true">Setup for convergence testing. A pseudo
1D cable is created, which is in fact a 1 element tick cable of
hexahedral elements, where <span class="math inline">\(h\)</span> is the
edge length of each hexahedral, which also defines the spatial
resolution. Propagation is initiated by applying a transmembrane
stimulus current at the left corner. <span
class="math inline">\(T_0\)</span>, and <span
class="math inline">\(T_1\)</span> correspond to wavefront arrival times
recorded at locations <span class="math inline">\(x_0 = -0.25\)</span>
cm and <span class="math inline">\(x_1 = 0.25\)</span> cm, respectively.
CV is computed as <span class="math inline">\(CV = (x_1 - x_0)/(T_1 -
T_0)\)</span>.</figcaption>
</figure>
</div>
<p>Using <span class="math inline">\(factor =
(CV_\zeta/\bar{CV_\zeta})^2\)</span>, the conductivities <span
class="math inline">\({\sigma_i}[i+1]\)</span> and <span
class="math inline">\({\sigma_e}[i+1]\)</span> are updated. A new
simulation is then run with the new conductivities. These steps are
repeated until the error in CV is below a given tolerance, <span
class="math inline">\(stop_{tol}\)</span></p>
<h2 id="problem-setup">Problem Setup</h2>
<p>This tutorial will run a simulation on a 1D cable and compute the
simulated CV, <span class="math inline">\(\bar{CV}\)</span>, and/or run
the iterative scheme shown Figure <code class="interpreted-text"
role="numref">fig-algorithm</code> to compute the conductivity <span
class="math inline">\({\sigma_m}\)</span> which yields the prescribed CV
for the chosen simulation setup.</p>
<h2 id="usage">Usage</h2>
<p>The experiment specific options are:</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">--resolution</span> RESOLUTION</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Spatial</span> resolution</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="ex">--velocity</span> VELOCITY   Desired conduction velocity <span class="er">(</span><span class="ex">m/s</span><span class="kw">)</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a><span class="ex">--gi</span> GI               Intracellular conductivity <span class="er">(</span><span class="ex">S/m</span><span class="kw">)</span></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a><span class="ex">--ge</span> GE               Extracellular conductivity <span class="er">(</span><span class="ex">S/m</span><span class="kw">)</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a><span class="ex">--ts</span> TS               Choose time stepping method. 0: explicit Euler, 1:</span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">crank</span> nicolson, 2: second-order time stepping</span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a><span class="ex">--dt</span> DT               Integration time step on micro-seconds</span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true" tabindex="-1"></a><span class="ex">--converge</span> <span class="dt">{0</span><span class="op">,</span><span class="dt">1}</span>      0: Measure velocity with given setup or 1: Compute</span>
<span id="cb2-10"><a href="#cb2-10" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">conductivities</span> that yield desired velocity with given</span>
<span id="cb2-11"><a href="#cb2-11" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">setup</span></span>
<span id="cb2-12"><a href="#cb2-12" aria-hidden="true" tabindex="-1"></a><span class="ex">--compareTuning</span>       run tuneCV with <span class="at">--resolution</span> 100 200 and 400 with and</span>
<span id="cb2-13"><a href="#cb2-13" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">without</span> tuning and make comparison plot</span>
<span id="cb2-14"><a href="#cb2-14" aria-hidden="true" tabindex="-1"></a><span class="ex">--compareTimeStepping</span></span>
<span id="cb2-15"><a href="#cb2-15" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">run</span> tuneCV with <span class="at">--resolution</span> 100 200 and 400 with</span>
<span id="cb2-16"><a href="#cb2-16" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Explicit</span> Euler and Crank Nicolson and make comparison</span>
<span id="cb2-17"><a href="#cb2-17" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">plot</span></span>
<span id="cb2-18"><a href="#cb2-18" aria-hidden="true" tabindex="-1"></a><span class="ex">--compareMassLumping</span>  run tuneCV with <span class="at">--resolution</span> 100 200 and 400 with and</span>
<span id="cb2-19"><a href="#cb2-19" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">without</span> mass lumping and make comparison plot</span>
<span id="cb2-20"><a href="#cb2-20" aria-hidden="true" tabindex="-1"></a><span class="ex">--compareModelPar</span>     run tuneCV with <span class="at">--resolution</span> 100 200 and 400 with</span>
<span id="cb2-21"><a href="#cb2-21" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">original</span> Ten Tusher cell model and with reduced</span>
<span id="cb2-22"><a href="#cb2-22" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">sodium</span> conductance and make comparison plot</span>
<span id="cb2-23"><a href="#cb2-23" aria-hidden="true" tabindex="-1"></a><span class="ex">--ar</span> AR               run tuneCV with for CV_f = 0.6 and CV_s = 0.3 m/s with</span>
<span id="cb2-24"><a href="#cb2-24" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">given</span> anisotropy ratio <span class="er">(</span><span class="fu">ar</span><span class="kw">)</span></span></code></pre></div>
<p>The user can either run single experiments, do a comparison of the
multiple resolutions with the options <code>--compareTuning</code>,
<code>--compareTimeStepping</code>, <code>--compareMassLumping</code>,
and <code>--compareModelPar</code>, or compare the change in
conductivities with different anisotropy ratios</p>
<h2 id="run-1d-examples">Run 1D examples</h2>
<p>To run the experiments of this tutorial do</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03A__study_prep_tuneCV</span></code></pre></div>
<h3 id="comparetuning"><strong>compareTuning:</strong></h3>
<p>To compute the conductivities for resolutions of 100, 200, and 400 um
with and without tuning, run: For webGUI, remove RESOLUTION value ..
code-block:: bash</p>
<blockquote>
<p>./run.py --compareTuning --visualize</p>
</blockquote>
<p>After running this command you should see the following plot</p>
<div id="fig-tuning">
<figure>
<img src="02_03A_tuneCV-compareTuning.png" class="align-center"
style="width:50.0%"
alt="Simulated CV for different resolutions with and without the iterative tuning scheme." />
<figcaption aria-hidden="true">Simulated CV for different resolutions
with and without the iterative tuning scheme.</figcaption>
</figure>
</div>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>Notice that CV varies with resolution in a non-linear fashion and CV
is constant when the iterative scheme is applied.</p>
</div>
<h3 id="comparetimestepping"><strong>compareTimeStepping:</strong></h3>
<p>To compare the effect using the Explicit Euler or Crank Nicolson
methods for the resolutions of 100, 200, and 400 um run: For webGUI,
remove RESOLUTION and TS values .. code-block:: bash</p>
<blockquote>
<p>./run.py --compareTimeStepping --visualize</p>
</blockquote>
<div id="fig-timestepping">
<figure>
<img src="02_03A_tuneCV-compareTimeStepping.png" class="align-center"
style="width:50.0%"
alt="Simulated CV for different resolutions with and without the iterative tuning scheme." />
<figcaption aria-hidden="true">Simulated CV for different resolutions
with and without the iterative tuning scheme.</figcaption>
</figure>
</div>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>Notice that CV varies with resolution virtually identically with both
time-stepping schemes.</p>
</div>
<h3 id="comparemasslumping"><strong>compareMassLumping:</strong></h3>
<p>Mass lumping is a common numerical technique in FEM to speed up
computation. The mass matrix, <span class="math inline">\(M\)</span> ,
is made diagonal, implying that its inverse is also diagonal, and
solving the system is trivial. The lumped mass matrix, <span
class="math inline">\(M^L\)</span> , is computed by setting its main
diagonal to be</p>
<p><span class="math display">\[M_{ii}^L = \Sigma_{j=1}^N
M_{ij}\]</span></p>
<p>To compare the effect of using mass lumping on CV for the resolutions
of 100, 200, and 400 um run: For webGUI, remove RESOLUTION and LUMPING
values .. code-block:: bash</p>
<blockquote>
<p>./run.py --compareMassLumping --visualize</p>
</blockquote>
<div id="fig-lumping">
<figure>
<img src="02_03A_tuneCV-compareMassLumping.png" class="align-center"
style="width:50.0%"
alt="Simulated CV for different resolutions with and without mass lumping." />
<figcaption aria-hidden="true">Simulated CV for different resolutions
with and without mass lumping.</figcaption>
</figure>
</div>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>Notice that CV varies with resolution in both cases, but the decrease
in CV for coarser resolutions is much steeper with mass lumping.</p>
</div>
<h3 id="comparemodelpar"><strong>compareModelPar:</strong></h3>
<p>To compare the effect of modifying the sodium conduction on CV for
the resolutions of 100, 200, and 400 um run: For webGUI, remove
RESOLUTION, MODEL and MODELPAR values .. code-block:: bash</p>
<blockquote>
<p>./run.py --compareModelPar --visualize</p>
</blockquote>
<div id="fig-modelpar">
<figure>
<img src="02_03A_tuneCV-compareModelPar.png" class="align-center"
style="width:50.0%"
alt="Simulated CV for different resolutions with the original Ten Tusher model and with reduce sodium conductance" />
<figcaption aria-hidden="true">Simulated CV for different resolutions
with the original Ten Tusher model and with reduce sodium
conductance</figcaption>
</figure>
</div>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>Notice that CV varies with resolution similarly in both cases, but
the CV with reduced sodium conductance is lower than with the original
model.</p>
</div>
<h2 id="run-2d-and-3d-examples">Run 2D and 3D examples</h2>
<p>As previously mentioned, conduction velocity in the ventricles is
orthotropic, that is, CV is different in each axis <span
class="math inline">\(\zeta\)</span>. Estimated average CVs in the
longitudinal, transverse, and sheet normal directions, <span
class="math inline">\(CV_f\)</span>, <span
class="math inline">\(CV_s\)</span> and <span
class="math inline">\(CV_n\)</span>, are about 0.67 m/s, 0.3 m/s, and
0.17 m/s, respectively<a href="#fn4" class="footnote-ref" id="fnref4"
role="doc-noteref"><sup>4</sup></a>.</p>
<h3 id="d"><strong>2D:</strong></h3>
<p>To run a 2D simulation where <span class="math inline">\(CV_f &gt;
CV_s\)</span>, that is, an anisotropic setup, <strong>two</strong> sets
of conductivities must be defined, one for each axis <span
class="math inline">\(\zeta=f,s\)</span>. To compute two sets of
conductivities with tuneCV, call the example without the "compare" flags
twice with two different velocities and look at the conductivities
output in the screen</p>
<p>For the longitudinal direction, run</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--velocity</span> 0.6 <span class="at">--converge</span> 1</span></code></pre></div>
<p>You should get an output close to the following:</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.6398 m/s [gi=0.1740, ge=0.6250, gm=0.1361]</span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.6006 m/s [gi=0.1530, ge=0.5497, gm=0.1197]</span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.6001 m/s [gi=0.1527, ge=0.5486, gm=0.1195]</span></code></pre></div>
<p>where gi, ge, and gm are <span
class="math inline">\({\sigma_i}\)</span>, <span
class="math inline">\({\sigma_e}\)</span>, and <span
class="math inline">\({\sigma_m}\)</span>, respectively. The <span
class="math inline">\({\sigma_i}\)</span> and :math:`{sigma_e}`pair will
yield a CV of 0.6 m/s in both monodomain and bidomain simulations.</p>
<p>Now, for the transverse direction, run</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--velocity</span> 0.3 <span class="at">--converge</span> 1</span></code></pre></div>
<p>You should get an output close to the following:</p>
<div class="sourceCode" id="cb7"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.6398 m/s [gi=0.1740, ge=0.6250, gm=0.1361]</span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.3070 m/s [gi=0.0383, ge=0.1374, gm=0.0299]</span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.3001 m/s [gi=0.0365, ge=0.1312, gm=0.0286]</span>
<span id="cb7-4"><a href="#cb7-4" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.3000 m/s [gi=0.0365, ge=0.1311, gm=0.0285]</span></code></pre></div>
<p>In this case, the <span class="math inline">\({\sigma_i}\)</span> and
:math:`{sigma_e}`pair will yield a CV of 0.3 m/s.</p>
<h3 id="d-1"><strong>3D:</strong></h3>
<p>Similarly, to run a 3D simulation where <span
class="math inline">\(CV_f &gt; CV_s &gt; CV_n\)</span>, that is, an
orthotropic setup, <strong>three</strong> sets of conductivities must be
defined, one for each axis <span
class="math inline">\(\zeta=f,s,n\)</span>. To compute the set of
conductivities for the sheet normal direction, <span
class="math inline">\(\zeta=n\)</span>, call the example once more with
<span class="math inline">\(CV_n = 0.2\)</span></p>
<div class="sourceCode" id="cb8"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--velocity</span> 0.2 <span class="at">--converge</span> 1</span></code></pre></div>
<p>You should get an output close to the following:</p>
<div class="sourceCode" id="cb9"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.6398 m/s [gi=0.1740, ge=0.6250, gm=0.1361]</span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.2038 m/s [gi=0.0170, ge=0.0611, gm=0.0133]</span>
<span id="cb9-3"><a href="#cb9-3" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.1998 m/s [gi=0.0164, ge=0.0588, gm=0.0128]</span>
<span id="cb9-4"><a href="#cb9-4" aria-hidden="true" tabindex="-1"></a><span class="ex">Conduction</span> velocity: 0.2000 m/s [gi=0.0164, ge=0.0590, gm=0.0128]</span></code></pre></div>
<p>In this case, the <span class="math inline">\({\sigma_i}\)</span> and
<span class="math inline">\({\sigma_e}\)</span> pair will yield a CV of
0.2 m/s.</p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>Notice that for <span class="math inline">\(CV_s = 0.3\)</span> and
<span class="math inline">\(CV_n = 0.2\)</span>, four iterations were
required to converge to the desired CV, whereas <span
class="math inline">\(CV_f = 0.6\)</span> required three. This is
because, in this example, the initial conductivity <span
class="math inline">\({\sigma_{m,\zeta}}^0\)</span> (see Figure <code
class="interpreted-text" role="numref">fig-algorithm</code>) yields a
velocity close to 0.6 m/s and is the same in all cases. Thus, the
initial error in CV is larger for <span
class="math inline">\(CV_s=0.3\)</span> and <span
class="math inline">\(CV_n=0.2\)</span> than for <span
class="math inline">\(CV_f=0.6\)</span>.</p>
</div>
<h2 id="tuning-anisotropy-ratios">Tuning Anisotropy Ratios</h2>
<h2 id="defining-anisotropy-ratios">Defining anisotropy ratios</h2>
<p>According to experimental measurements, the intracellular domain is
more anisotropic than the extracellular domain, that is, the ratio
between the longitudinal and transverse conductivities is larger in the
intracellular space than in the extracellular space.</p>
<p>In the intracellular domain, the anisotropy ratio between the
longitudinal, <span class="math inline">\(\zeta=f\)</span>, and
transverse, <span class="math inline">\(\zeta=s\)</span>, directions is
defined as</p>
<p><span class="math display">\[\alpha_{ifs} =
{\sigma_{if}}/{\sigma_{is}}\]</span></p>
<p>where <span class="math inline">\({\sigma_{if}}\)</span> and <span
class="math inline">\({\sigma_{is}}\)</span> are the intracellular
conductivities in the longitudinal and transverse direction,
respectively.</p>
<p>Similarly, in the extracellular domain, the anisotropy ratio between
the longitudinal, <span class="math inline">\(\zeta=f\)</span>, and
transverse, <span class="math inline">\(\zeta=s\)</span>, directions is
defined as</p>
<p><span class="math display">\[\alpha_{efs} =
{\sigma_{ef}}/{\sigma_{es}}\]</span></p>
<p>where <span class="math inline">\({\sigma_{ef}}\)</span> and <span
class="math inline">\({\sigma_{es}}\)</span> are the extracellular
conductivities in the longitudinal and transverse direction,
respectively.</p>
<p>Now, we can express the anisotropy ratio between the two domains as
in the longitudinal and transverse directions as</p>
<p><span class="math display">\[\alpha_{lt} = \alpha_{ifs}/\alpha_{efs}
=
({\sigma_{if}}*{\sigma_{es}})/({\sigma_{ef}}*{\sigma_{is}})\]</span></p>
<h2
id="computing-conductivities-with-a-fixed-anisotropy-ratio">Computing
conductivities with a fixed anisotropy ratio</h2>
<p>There are many ways to enforce anisotropy ratios between conductivity
values. In this tutorial, we use a fixed anisotropy strategy, where,
given a anisotropy ratio, we compute a initial transverse conductivity
for extracellular domain. The intracellular conductivities as well as
the longitudinal extracellular conductivity is kept fixed at default
values. Using Equation <code class="interpreted-text"
role="eq">equ-aniso</code> we obtain:</p>
<p><span class="math display">\[{\sigma_{es}} =
(\alpha_{lt}*{\sigma_{ef}}*{\sigma_{is}})/{\sigma_{if}}\]</span></p>
<p>We then tune the longitudinal and transverse conductivities
separately using the Iterative scheme. In this manner, we enforce both
the desired CV and anisotropy ratio.</p>
<h2 id="the-effect-of-equal-and-unequal-anisotropy-ratios">The effect of
equal and unequal anisotropy ratios</h2>
<p>If <span class="math inline">\(\alpha_{ifs} = \alpha_{efs}\)</span>,
then <span class="math inline">\(\alpha_{lt} = 1\)</span>. In this case,
the two domains have <strong>equal</strong> anisotropy ratios. On the
other hand, if <span class="math inline">\(\alpha_{ifs} &gt;
\alpha_{efs}\)</span>, then <span class="math inline">\(\alpha_{lt} &gt;
1\)</span>. In this case, the two domains have <strong>unequal</strong>
anisotropy ratios. unequal anisotropy ratios can only be represented
with the bidomain model <a
href="https://carp.medunigraz.at/carputils/cme-manual.html#equation-eq-bidm">[2]</a>.</p>
<p>While anisotropy ratios are of rather minor relevance when simulating
impulse propagation in tissue<a href="#fn5" class="footnote-ref"
id="fnref5" role="doc-noteref"><sup>5</sup></a>, they play a prominent
role when the stimulation of cardiac tissue via externally applied
electric fields is studied In this case, virtual electrodes appear
around the stimulus site, as show in the figure below.</p>
<div id="fig-anisotropy-vep">
<figure>
<img src="02_03A_tuneCV-anisotropy-vep.png" class="align-center"
alt="Induced virtual electrodes in response to a strong hyperpolarizing extracellular stimulus for the equal and unequal anisotropy ratios." />
<figcaption aria-hidden="true">Induced virtual electrodes in response to
a strong hyperpolarizing extracellular stimulus for the equal and
unequal anisotropy ratios.</figcaption>
</figure>
</div>
<h2 id="run-anisotropy-ratio-examples">Run anisotropy ratio
examples</h2>
<p>To compute longitudinal and transverse conductivities for the case of
<strong>equal</strong> anisotropy ratios and <span
class="math inline">\(CV_f = 0.6\)</span> and <span
class="math inline">\(CV_s = 0.3\)</span>, run For webGUI, remove
VELOCITY, GI, GE and CONVERGE values .. code-block:: bash</p>
<blockquote>
<p>./run.py --ar=1.0</p>
</blockquote>
<p>You should get conductivities similar to these:</p>
<div class="sourceCode" id="cb10"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a><span class="ex">g_if:</span> 0.152700  g_is: 0.036500  g_ef: 0.548500  g_es: 0.131100</span></code></pre></div>
<p>To compute longitudinal and transverse conductivities for the case of
<strong>unequal</strong> anisotropy ratios and <span
class="math inline">\(CV_f = 0.6\)</span> and <span
class="math inline">\(CV_s = 0.3\)</span>, run</p>
<div class="sourceCode" id="cb11"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--ar</span><span class="op">=</span>3.0</span></code></pre></div>
<p>You should get conductivities similar to these:</p>
<div class="sourceCode" id="cb12"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true" tabindex="-1"></a><span class="ex">g_if:</span> 0.152700  g_is: 0.031200  g_ef: 0.548500  g_es: 0.336100</span></code></pre></div>
<p><strong>References</strong></p>
<section class="footnotes footnotes-end-of-document"
role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p><em>Costa CM, Hoetzl E, Rocha BM,
Prassl AJ, Plank G.</em>, <strong>Automatic Parameterization Strategy
for Cardiac Electrophysiology Simulations</strong>, Comput Cardiol
40:373-376, 2013. <a
href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3980367">[Pubmed]</a><a
href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn2" role="doc-endnote"><p><em>Costa CM, Hoetzl E, Rocha BM,
Prassl AJ, Plank G.</em>, <strong>Automatic Parameterization Strategy
for Cardiac Electrophysiology Simulations</strong>, Comput Cardiol
40:373-376, 2013. <a
href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3980367">[Pubmed]</a><a
href="#fnref2" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn3" role="doc-endnote"><p><em>Costa CM, Hoetzl E, Rocha BM,
Prassl AJ, Plank G.</em>, <strong>Automatic Parameterization Strategy
for Cardiac Electrophysiology Simulations</strong>, Comput Cardiol
40:373-376, 2013. <a
href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3980367">[Pubmed]</a><a
href="#fnref3" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn4" role="doc-endnote"><p><em>B. J. Caldwell, M. L. Trew, G. B.
Sands, D. A. Hooks, I. J. LeGrice, B. H. Smaill</em>, <strong>Three
distinct directions of intramural activation reveal nonuniform
side-to-side electrical coupling of ventricular myocytes.</strong>, Circ
Arrhythm Electrophysiol, vol. 2, no. 4, pp. 433-440, Aug 2009. <a
href="https://www.ncbi.nlm.nih.gov/pubmed/19808500">[Pubmed]</a><a
href="#fnref4" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn5" role="doc-endnote"><p><em>Costa CM, Hoetzl E, Rocha BM,
Prassl AJ, Plank G.</em>, <strong>Automatic Parameterization Strategy
for Cardiac Electrophysiology Simulations</strong>, Comput Cardiol
40:373-376, 2013. <a
href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3980367">[Pubmed]</a><a
href="#fnref5" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
