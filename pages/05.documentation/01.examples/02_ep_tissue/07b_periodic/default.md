---
description: Periodic boundary conditions connect the left edge of a sheet to the
  right, or the top to the bottom
image: 02_07B_perCnnx.png
title: Periodic boundary conditions
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Periodic Boundary Conditions</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/07B_periodic/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Edward Vigmond <edward.vigmond@u-bordeaux.fr></i>
<h2 id="periodic-bc-tutorial">Periodic Boundary Conditions</h2>
<div class="sectionauthor">
<p>Edward Vigmond &lt;<a
href="mailto:edward.vigmond@u-bordeaux.fr">edward.vigmond@u-bordeaux.fr</a>&gt;</p>
</div>
<p>Periodic boundary conditions connect the left edge of a sheet to the
right, or the top to the bottom. This, for example models a cylinder
very simply while allowing visualization of the whole domain. The intact
atria or ventricles are better approximated by sheets with periodic
boundary conditions and not the simpler no flux boundaries. Indeed,
reentries last longer on periodic domains since they do not annilhilmate
at boundary edges.</p>
<p>The implementation of periodic b.c.'s is not straightforward since
adding the equations equating nodes on the left boundary to those on the
right boundary (or top and bottom) is mathematically trivial but
destroys the symmetry of the system, and, thereby, invalidates many
conjugate gradient preconditioners. A workaround is to define line
connections linking the edges of the tissue, and assigning ridiculously
large conductivity values to these links, effectively short circuiting
the nodes. The mesher program will generate these links. To run the
experiments of this example change directories as follows:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/07B_periodic</span></code></pre></div>
<h2 id="experimental-setup">Experimental Setup</h2>
<div id="fig-period-bcs">
<figure>
<img src="07b_periodic/02_07B_perCnnx.png" class="align-center"
style="width:30.0%"
alt="A sheet showing periodic boundary conditions in X implemented with linear elements (in pink) joining the left and right edges." />
<figcaption aria-hidden="true">A sheet showing periodic boundary
conditions in X implemented with linear elements (in pink) joining the
left and right edges.</figcaption>
</figure>
</div>
<figure>
<img src="07b_periodic/02_07B_perbcs.gif" class="align-center"
style="width:30.0%"
alt="A point stimulus delivered off-center on a piece of tissue with periodic boundary conditions in both directions." />
<figcaption aria-hidden="true">A point stimulus delivered off-center on
a piece of tissue with periodic boundary conditions in both
directions.</figcaption>
</figure>
<h2 id="initiating-reentry">Initiating Reentry</h2>
<p>The standard cross-shock method of reentry initiation will fail with
periodic b.c.'s. The initial line stimulus will produce two wavefronts
which collide, and the quarter sheet S2 will quickly go everywhere and
annihilate itself. There are two simple ways to induce reentry.</p>
<ol type="1">
<li>We define a mesh without the periodic connections and run an intial
portion of the silulation on that. Just after the S2, we save the state
and restart on the mesh with the periodic b.c.'s enforced.</li>
<li>We temporarily block propagation across an edge by clamping the
transmembrane voltage and then turning off the clamp after the S2.</li>
</ol>
<h2 id="experiments">Experiments</h2>
<p>The below defined experiments demonstrate initiating reentry in a
sheet of tissue with periodic boundary conditions. To run these
experiments</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> tutorials/02_EP_tissue/07B_periodic</span></code></pre></div>
<p>Run</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span></span></code></pre></div>
<p>to see all exposed experimental parameters</p>
<pre><code>--bc              boundary condition imposed (noflux,Xper,Yper,XYper)
--tend TEND       Duration of simulation (ms)
--size SIZE       size of square sheet (cm)
--S2-start        time to apply quarter sheet S2 stimulus (&lt;0=no S2)
--block-Vm         transmembrane voltage of block line [-86 mV]
--block-dur        duration of block [0 ms]
--cnnxG           conductivity of periodic connections [10000 S/m]
--monitor [delay] monitor progress Press &quot;r&quot; in the model window to refresh. 
                  Default wait 30s seconds to launch.</code></pre>
<h3 id="experiment-exp01"><strong>Experiment exp01</strong></h3>
<p>Try to create a wave which propagates forever. Find a suitable time
and Vm clamp voltage for turning off the block.</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--tend</span> 300 <span class="at">--size</span> 2 <span class="at">--monitor</span><span class="op">=</span>10 <span class="at">--np</span> 10 <span class="at">--bc</span><span class="op">=</span>Yper <span class="at">--block-dur</span><span class="op">=</span><span class="pp">?</span> <span class="at">--block-Vm</span> <span class="pp">??</span> </span></code></pre></div>
<p>One solution is here: <code class="interpreted-text"
role="abbr">block-Vm (-80)</code> and <code class="interpreted-text"
role="abbr">block-dur (90)</code></p>
<h3 id="experiment-exp02"><strong>Experiment exp02</strong></h3>
<p>Create a rotor on a block using the cross-shock protocol</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--size</span> 2 <span class="at">--np</span><span class="op">=</span>4 <span class="at">--monitor</span><span class="op">=</span>15 <span class="at">--block-Vm</span> <span class="pp">?</span> <span class="at">--block-dur</span><span class="op">=</span><span class="pp">?</span> <span class="at">--S2-start</span> <span class="pp">?</span> </span></code></pre></div>
<p>A larger sheet may make it easier.</p>
<h2 id="under-the-hood">Under the Hood</h2>
<p>The <code>mesher</code> program takes the argument
<code>-periodic</code> with the following possible values</p>
<table>
<thead>
<tr class="header">
<th>Value</th>
<th>Periodic BC's</th>
<th>Region ID</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>0</td>
<td>None</td>
<td><blockquote>
<ul>
<li></li>
</ul>
</blockquote></td>
</tr>
<tr class="even">
<td>1</td>
<td>in X</td>
<td><blockquote>
<p>1234</p>
</blockquote></td>
</tr>
<tr class="odd">
<td>2</td>
<td>in Y</td>
<td><blockquote>
<p>1235</p>
</blockquote></td>
</tr>
<tr class="even">
<td>3</td>
<td>in X and Y</td>
<td><blockquote>
<p>1234/1235</p>
</blockquote></td>
</tr>
</tbody>
</table>
<p>The longitudinal conductivity for <code>g_il</code> in the
appropriate conductivity regions should then be set very high so that
there is very little voltage drop across the connection :</p>
<pre><code>gregion[1].numIDs = 2
gregion[1].ID     = 1234 1235
gregion[1].g_il   = 1.0e4</code></pre>
<p>For the temporary block, use a stimulus with
<code>stimtype=9</code>.</p>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>