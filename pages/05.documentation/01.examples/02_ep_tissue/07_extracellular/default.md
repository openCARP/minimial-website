---
description: This tutorial explains the background of computing extracellular potentials
  and ECG using different techniques
image: 02_07_extPotentials_ECG.png
title: Extracellular potentials and ECGs
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Extracellular potentials and ECGs</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/07_extracellular/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Gernot Plank <gernot.plank@medunigraz.at>, Fernando Campos <fernando.campos@kcl.ac.uk></i>
<h2 id="tutorial-ecg">Overview</h2>
<p>This tutorial explains the background of computing extracellular
potentials and electrocardiograms (ECGs) using different techniques. The
techniques used have their specific advantages depending on the
application scenario. In this tutorial the focus is on using <span
class="math inline">\(\phi_{\rm e}\)</span> to compute unipolar
extracellular electrograms and compare these with fullblown bidomain
simulations.</p>
<ul>
<li><strong>Recovery of extracellular potentials:</strong> Recovery
techniques are efficient when potentials shall be computed for a limited
number of field points. An important limitation is the underlying
assumption that cardiac electric sources are immersed in an unbounded
conductive medium of conductivity <span
class="math inline">\(\sigma_\mathrm{b}\)</span>. The technique is not
overly efficient for a large number of field points such as in a body
surface potential mapping application. There BEM-based or lead field
approaches are better suitable.</li>
<li><strong>bidomain:</strong> When using a bidomain formulation,
extracellular current flow and potentials are computed everywhere in the
entire domain. This approach is most accurate and considered a gold
standard for ECG modeling. However, the main disadvantage is the
significantly increased computational cost. First, the entire domain
must be explicitly discretized, that is, the tissue domain plus a bath
or torso domain where extracellular potentials can be recorded.
Secondly, bidomain requires the solution of an elliptic PDE which tends
to be more costly than the plain reaction-diffusion monodomain equation,
roughly by one order of magnitude.</li>
<li><strong>pseudo bidomain:</strong> In this case very similar
considerations hold as in the bidomain case. The main advantage of a
pseudo-bidomain approach as published by <a
href="https://www.ncbi.nlm.nih.gov/pubmed/21292591">Bishop &amp;
Plank</a> are the significant savings in computational cost. A
pseudo-bidomain is only marginally more expensive than a monodomain
simulation as extracellular potentials are only solved at output
granularity. The main disadvantage of the pseudo-bidomain approach is
that any feedback of current flow in the extracellular medium upon
excitation spread remains unaccounted for.</li>
</ul>
<p>Theoretical background and more rigorous validation of the methods
are found in the electrocardiogram section of the manual and in<a
href="#fn1" class="footnote-ref" id="fnref1"
role="doc-noteref"><sup>1</sup></a> and<a href="#fn2"
class="footnote-ref" id="fnref2"
role="doc-noteref"><sup>2</sup></a>.</p>
<h2 id="objectives">Objectives</h2>
<p>This tutorial aims to achieve the following objectives:</p>
<ul>
<li>Understand how to use the openCARP <span
class="math inline">\(\phi_{\rm e}\)</span>-recovery feature</li>
<li>Analyse differences between <span class="math inline">\(\phi_{\rm
e}\)</span>-recovery and full bidomain-based extracellular
potentials</li>
<li>Examine the effect of bath size upon extracellular potentials in the
bath medium and its relationship to recovered potentials.</li>
</ul>
<h2 id="setup">Setup</h2>
<p>A simple reduced wedge setup is used for this tutorial. While a
reasonable transmural width of 10 mm is used, in the circumferential
direction the spatial extent is limited to the used spatial resolution
for the sake of computational efficiency. That is, the wedge is a 3D
object, but comprises only one layer of elements along the
circumferential direction (x-axis), rendering the setup almost 2D. The
wedge contains transmural as well as apico-basal heterogeneities which
are based on the study of Boukens et al<a href="#fn3"
class="footnote-ref" id="fnref3" role="doc-noteref"><sup>3</sup></a>.
The wedge uses standard transmural fiber rotation. Stimulation sites to
initiate propagation can be chosen at six different locations along the
endocardial and epicardial surface. The size of the bath is exposed as
an input parameter and can be varied. An overview of the experimental
setup is given in <code class="interpreted-text"
role="numref">fig-tutorial-ecg</code> A). Default visualization outputs
showing local activation time, action potential propagation and
extracellular potentials are illustrated in <code
class="interpreted-text" role="numref">fig-tutorial-ecg</code> B).</p>
<div id="fig-tutorial-ecg">
<figure>
<img src="07_extracellular/02_07_extPotentials_ECG.png"
id="fig-tutorial-ecg" class="align-center" style="width:90.0%"
alt="Computing extracellular potentials and ECGs. A) Setup B) Visualization Extracellular potentials \phi_{\mathrm 0} - \phi_{\mathrm 6} are recovered at the indicated locations along the apico-basal centerline to be compared with bidomain-based potentials. Three recovery sites are located inside the tissue, one in the midmyocardium, and one at endocardial and epicardial surfaces, respectively. In the presence of a bath (i.e., bath&gt;0) extra recovery sites are located at the surface of the bath and in the center of the bath for both bath compartments interfacing with endocardial and epicardial surfaces." />
<figcaption aria-hidden="true">Computing extracellular potentials and
ECGs. A) Setup B) Visualization Extracellular potentials <span
class="math inline">\(\phi_{\mathrm 0} - \phi_{\mathrm 6}\)</span> are
recovered at the indicated locations along the apico-basal centerline to
be compared with bidomain-based potentials. Three recovery sites are
located inside the tissue, one in the midmyocardium, and one at
endocardial and epicardial surfaces, respectively. In the presence of a
bath (i.e., <code>bath&gt;0</code>) extra recovery sites are located at
the surface of the bath and in the center of the bath for both bath
compartments interfacing with endocardial and epicardial
surfaces.</figcaption>
</figure>
</div>
<h2 id="input-parametes">Input Parametes</h2>
<p>The following input parameters are exposed to steer the
experiment:</p>
<pre><code>--duration DURATION     
                        Duration of simulation [ms].
                        Choosing a value of around 70 ms covers activation only
                        and thus yields only the depolarization complex of the ECG.
                        A value of 350 ms covers also repolarization and T-waves.

--resolution RESOLUTION
                        choose mesh resolution in [um] (default is 250 um).

--tm-stim-location {endo,epi}

                         choose transmural stimulus location (default is endo)

--ab-stim-location {apex,mid,base}

                         choose apicobasal stimulus location (default is mid)

--bath BATH           
                         choose size of bath to attach at epi and endo [mm]
                         (default is 0.0 mm). With increasing the bath size
                         the difference between recovered extracellular potentials 
                         and (pseudo-)bidomain potentials diminishes.

--sourceModel {monodomain,bidomain,pseudo_bidomain}

                         pick type of electrical source model (default is monodomain).
                         For comparing, pseudo_bidomain is most appropriate 
                         as compute time with bidomain is markedly longer.</code></pre>
<h2 id="expected-results">Expected Results</h2>
<p>In the monodomain case where we do not compute potentials in the bath
domain, only recovered extracellular potentials can be inspected. In
<code class="interpreted-text"
role="numref">fig-tutorial-ecg-monodomain</code> expected results are
illustrated. Recovered extracellular potentials and corresponding field
points are shown.</p>
<div id="fig-tutorial-ecg-monodomain">
<figure>
<img src="07_extracellular/unipolar_electrograms_ini_uni_term.png"
class="align-center" style="width:100.0%"
alt="Visualization of recovered extracellular potentials at sites of initiating propagation (\phi_{\rm 0} in fig-tutorial-ecg B), uniform undisturbed propagation (\phi_{\rm 1} in fig-tutorial-ecg B) and terminating (or colliding) propagation (\phi_{\rm 2} in fig-tutorial-ecg B)." />
<figcaption aria-hidden="true">Visualization of recovered extracellular
potentials at sites of initiating propagation (<span
class="math inline">\(\phi_{\rm 0}\)</span> in <code
class="interpreted-text" role="numref">fig-tutorial-ecg</code> B),
uniform undisturbed propagation (<span class="math inline">\(\phi_{\rm
1}\)</span> in <code class="interpreted-text"
role="numref">fig-tutorial-ecg</code> B) and terminating (or colliding)
propagation (<span class="math inline">\(\phi_{\rm 2}\)</span> in <code
class="interpreted-text" role="numref">fig-tutorial-ecg</code>
B).</figcaption>
</figure>
</div>
<p>Activation spread in terms of transmembrane voltage <span
class="math inline">\(V_{\rm m}\)</span> and extracellular potential
<span class="math inline">\(\phi_{\rm e}\)</span> are shown in <code
class="interpreted-text"
role="numref">fig-tutorial-ecg-excitation-spread</code>.</p>
<div id="fig-tutorial-ecg-excitation-spread">
<figure>
<img src="07_extracellular/wedge_ecg.gif" class="align-center"
style="width:75.0%"
alt="Action potential propagation is elicited at the endocardial apical edge of the wedge. The spread of activation and repolarization is shown in terms of extracellular potential \phi_{\rm e} and transmembrane voltage V_{\rm m}. Field points at which extracellular potentials were recovered are shown in the right panel." />
<figcaption aria-hidden="true">Action potential propagation is elicited
at the endocardial apical edge of the wedge. The spread of activation
and repolarization is shown in terms of extracellular potential <span
class="math inline">\(\phi_{\rm e}\)</span> and transmembrane voltage
<span class="math inline">\(V_{\rm m}\)</span>. Field points at which
extracellular potentials were recovered are shown in the right
panel.</figcaption>
</figure>
</div>
<h2 id="experiments">Experiments</h2>
<h2 id="exp-em-ti-ecg-01"><strong>Experiment exp01 (central endocardial
activation (monodomain) with ecg recovery)</strong></h2>
<p>In a first step we omit a bath and use a <em>monodomain</em> as a
source model. Extracellular potentials are computed using the <span
class="math inline">\(\phi_{\rm e}\)</span> recovery technique.
Propagation is initiated at the center of the endocardial surface as if
activated by a Purkinje fascicle. Only 60 ms of activation are computed
as we are only interested in the depolarization complex. Details on the
openCARP-specific definition of this method are found in the manual.</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--sourceModel</span> monodomain <span class="at">--duration</span> 60 <span class="at">--visualize</span></span></code></pre></div>
<p>In this case, the extracellular potentials at the sites of stimulus
delivery (vertex 0, initiating propagation), in the mid-myocardium
(vertex 1, undisturbed uniform propagation) and at the epicardial
surface (vertex 2, terminating propagation) shall be examined.</p>
<h2 id="exp-em-ti-ecg-02"><strong>Experiment exp02 (apical endocardial
activation (monodomain) with ecg recovery)</strong></h2>
<p>In experiment exp02 we shift the stimulus towards the apical edge of
the wedge at the endocardium. The observation sites for recovering <span
class="math inline">\(\phi_{\rm e}\)</span> remain the same, but local
activation patterns underneath the recording electrodes are changed.
This is reflected in the waveforms of the unipolar extracellular
electrograms. Total activation time in this case is longer, which is why
we pick a larger duration to allow for the entire wedge being
activated.</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--ab-stim-location</span> apex <span class="at">--sourceModel</span> monodomain <span class="at">--duration</span> 100 <span class="at">--visualize</span></span></code></pre></div>
<h2 id="exp-em-ti-ecg-03"><strong>Experiment exp03 (apical endocardial
activation (pseudo-bidomain) with ecg recovery, small
bath)</strong></h2>
<p>We repeat exp02 using a pseudo-bidomain source model. This allows the
comparison between extracellular potentials computed by the bidomain
method with those computed by the recovery method. We start with a very
small bath size of only 1 mm.</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--ab-stim-location</span> apex <span class="at">--sourceModel</span> pseudo_bidomain <span class="at">--bath</span> 1.0 <span class="at">--duration</span> 60 <span class="at">--visualize</span></span></code></pre></div>
<h2 id="exp-em-ti-ecg-04"><strong>Experiment exp04 (apical endocardial
activation (pseudo-bidomain) with ecg recovery, larger
bath)</strong></h2>
<p>We repeat exp03 using a larger bath of 20 mm width to examine how
bath size effects extracellular potential waveforms, their magnitude and
differences between bidomain and recovery potentials.</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--ab-stim-location</span> apex <span class="at">--sourceModel</span> pseudo_bidomain <span class="at">--bath</span> 20.0 <span class="at">--duration</span> 60 <span class="at">--visualize</span></span></code></pre></div>
<h2 id="exp-em-ti-ecg-05"><strong>Experiment exp05 (apical endocardial
activation (monodomain) with ecg recovery, repolarization)</strong></h2>
<p>We repeat exp02, but run for longer to observe unipolar electrograms
during repolarization.</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--ab-stim-location</span> apex <span class="at">--sourceModel</span> monodomain <span class="at">--duration</span> 400 <span class="at">--visualize</span></span></code></pre></div>
<p>In this case it is of interest to examine so-called transmural ECGs,
that is, the difference between unipolar electrograms recorded from
endocardial and epicardial surfaces. The recovered unipolar ECG traces
are stored in the file phie_recovery.igb in igb format in the respective
output directories. We use igbextract to compute the differences from
the igb file and store the output in a text file for easier
visualization. For this end we run:</p>
<div class="sourceCode" id="cb7"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--tmECG</span> simID</span></code></pre></div>
<p>where <code>simID</code> is the name of the output directory for
which we want to compute the transmural ECG. The computed transmural ECG
can be visualized, for instance, with gnuplot.</p>
<p><strong>References</strong></p>
<section class="footnotes footnotes-end-of-document"
role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p><em>Bishop MJ, Plank G.</em>,
<strong>Representing cardiac bidomain bath-loading effects by an
augmented monodomain approach: application to complex ventricular
models.</strong>, IEEE Trans Biomed Eng 58(4):1066-75, 2011. <a
href="https://www.ncbi.nlm.nih.gov/pubmed/21292591">[Pubmed]</a> <a
href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3075562/">[PMC]</a><a
href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn2" role="doc-endnote"><p><em>Bishop MJ, Plank G.</em>
<strong>Bidomain ECG simulations using an augmented monodomain model for
the cardiac source.</strong> IEEE Trans Biomed Eng 58(4):1066-75, 2011.
<a href="https://www.ncbi.nlm.nih.gov/pubmed/21536529">[Pubmed]</a> <a
href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3378475/">[PMC]</a><a
href="#fnref2" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn3" role="doc-endnote"><p><em>Boukens B, Sulkin MS, Gloschat
CR, Ng FS, Vigmond EJ, Efimov IR.</em> <strong>Transmural APD gradient
synchronizes repolarization in the human left ventricular wall.</strong>
Cardiovasc Res 108(1):188-96, 2015. <a
href="https://www.ncbi.nlm.nih.gov/pubmed/26209251">[Pubmed]</a> <a
href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4571834/">[PMC]</a><a
href="#fnref3" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>