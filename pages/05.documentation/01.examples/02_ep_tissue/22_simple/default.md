---
description: This is a simple example covering mesh generation, monodomain simulation
  and local activation time extraction during postprocessing. It can be a good starting
  point to base your own experiment on.
image: 02_22_code.png
title: Simple example to base your own experiment on
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Simple carputils Example</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/22_simple/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Andrew Crozier <andrew.crozier@medunigraz.at>, Axel Loewe <axel.loewe@kit.edu></i>
<p>This is a simple monodomain example that is primarily a demonstration
of the carputils framework. It includes all typical building blocks of a
cardiac electrophysiology experiment starting from mesh generation /
loading, stimulus definition, conductivity and ionic model definition
and launching the simulation. Finally, local activation times are
determined as a postprocessing step and the results are visualized using
meshalzyer. As such, this script can be a good starting point to build
your own experiment using carputils.</p>
<p>This webpage only shows the basic documentation, to use it as a
starting point for building your own experiment, you should look at the
<a
href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/22_simple/run.py">Python
code</a> of this experiment.</p>
<p>For detailed information on the different functions and parameters,
consult the <a
href="https://opencarp.org/documentation/carputils-documentation">carputils
documentation</a>.</p>
<h2 id="problem-setup">Problem Setup</h2>
<p>This example defines a small cuboid on the domain:</p>
<p><span class="math display">\[-1.0  \leq x \leq 1.0\]</span><span
class="math display">\[-0.25 \leq y \leq 0.25\]</span><span
class="math display">\[-0.25 \leq z \leq 0.25\]</span></p>
<p>And applies an electrical stimulus on the <span
class="math inline">\(x = -1.0\)</span> face.</p>
<h2 id="usage">Usage</h2>
<p>This example specifies just one optional argument,
<code>--tend</code>. Use it to specify how long to simulate for after
stimulus, in milliseconds:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--tend</span> 100</span></code></pre></div>
<p>As with other examples, add the <code>--visualize</code> option to
automatically load the results in meshalyzer:</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--tend</span> 100 <span class="at">--visualize</span></span></code></pre></div>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>