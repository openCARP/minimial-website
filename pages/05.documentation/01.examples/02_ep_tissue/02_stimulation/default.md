---
description: In this example you learn how to stimulate a tissue from the extracellular
  space
image: 02_02_ExtracellularStimulationSetup.png
title: Extracellular stimulation
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Stimulation</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/02_stimulation/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Gernot Plank <gernot.plank@medunigraz.at></i>
<div id="tutorial_extracellular-stimulation">
<p>To run the experiments of this tutorial change directories as
follows:</p>
</div>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/02_stimulation </span></code></pre></div>
<h2 id="extracellular-stimulation">Extracellular stimulation</h2>
<p>Transmembrane voltage can be changed by applying an extracellular
field in the extracellular space. As outlined in Sec. <code
class="interpreted-text" role="ref">electrical-stimulation</code>, an
electric field can be set up either by injection/withdrawal of currents
in the extracellular space or by changing the extracellular potential
with voltage sources.</p>
<p>For testing the various types of extracellular stimulation we
generate a thin strand of tissue of 1 cm length. Electrodes are located
at both caps of the strand with an additional auxiliary electrode in the
very center of the strand. An illustration of the setup is given in
<code class="interpreted-text"
role="numref">fig-extra-stim-setup</code>. Extracellular voltage and
current stimuli are pre-configured to generate an extracellular voltage
drop across the strand of about 2 V. This corresponds to an electric
field magnitude of 2 V/cm which is sufficient to initiate action
potential propagation under the cathode.</p>
<div id="fig-extra-stim-setup">
<figure>
<img src="02_stimulation/02_02_ExtracellularStimulationSetup.png"
class="align-center" style="width:50.0%"
alt="Simple setup for testing the various configurations for extracellular stimulation. Three electrodes are pre-defined, electrode A and B located at the left hand face and right hand face of the tissue strand, respectively, and an additional auxiliary electrode C sitting on top of the strand in its center." />
<figcaption aria-hidden="true">Simple setup for testing the various
configurations for extracellular stimulation. Three electrodes are
pre-defined, electrode A and B located at the left hand face and right
hand face of the tissue strand, respectively, and an additional
auxiliary electrode C sitting on top of the strand in its
center.</figcaption>
</figure>
</div>
<h2 id="experiments">Experiments</h2>
<p>Several types of stimulation setups are predefined. To run these
experiments</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/02_stimulation</span></code></pre></div>
<p>Run</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span> </span></code></pre></div>
<p>to see all exposed experimental parameters.</p>
<pre><code>--stimulus {extra_V,extra_V_bal,extra_V_OL,extra_I,extra_I_bal,extra_I_mono}
                      pick stimulus type

--grounded {on,off}   turn on/off use of ground whereever possible</code></pre>
<p>The geometry of the three electrodes A, B and C are defined as
follows:</p>
<pre><code># electrode A
stimulus[0].xd =   100. 
stimulus[0].y0 =  -100. 
stimulus[0].yd =   200. 
stimulus[0].z0 =  -100. 
stimulus[0].zd =   200. 

# electrode B
stimulus[1].x0 =  4950.
stimulus[1].xd =   100.
stimulus[1].y0 =  -100.
stimulus[1].yd =   200.
stimulus[1].z0 =  -100.
stimulus[1].zd =   200.

# electrode C
stimulus[2].x0 =    -10.
stimulus[2].xd =     20.
stimulus[2].y0 =  -5000.
stimulus[2].yd =  10000.
stimulus[2].z0 =  -5000.
stimulus[2].zd =  10000.</code></pre>
<p>The input parameter <strong>stimulus</strong> selects a stimulus
configuration among the following available options:</p>
<h3 id="exp-stim-extra-V"><strong>Experiment exp01 (extracellular
voltage stimulation)</strong></h3>
<p>The stimulus option <code>extra_V</code> sets up stimulation through
application of extracellular voltage corresponding to <code
class="interpreted-text" role="numref">fig-extraV-wgnd</code>. The
potential electrode A is clamped to 2000 mV for a duration of 2 ms,
electrode B is grounded, i.e. <span class="math inline">\(\phi_{\mathrm
e} = 0\)</span>. Electrode C is not used here, electrodes A and B are
configured as follows:</p>
<pre><code>numstim = 2   # use two electrodes, A and B

# electrode A
stimulus[0].stimtype = 2        # extracellular voltage stimulus
stimulus[0].strength = 2e3      # voltage at A in mV 

# electrode B
stimulus[1].stimtype = 3        # extracellular ground</code></pre>
<p>Run this experiment by</p>
<div class="sourceCode" id="cb7"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--stimulus</span> extra_V <span class="at">--visualize</span></span></code></pre></div>
<h3 id="exp-stim-extra-V_bal"><strong>Experiment exp02 (balanced
extracellular voltage stimulation)</strong></h3>
<p>As experiment exp01, the stimulus option <code>extra_V_bal</code>
sets up stimulation through the application of extracellular voltage.
The potential on the left cap is clamped to 1000 mV for a duration of 2
ms and the right hand electrode to -1000mV. This stimulation circuit
corresponds to the setup shown in <code class="interpreted-text"
role="numref">fig-extraV-symm-wgnd</code>.</p>
<pre><code>numstim = 3   # use all three electrodes, A, B and C

# electrode A
stimulus[0].stimtype =  2       # extracellular voltage stimulus
stimulus[0].strength =  1e3     # voltage at A in mV 

# electrode B
stimulus[1].stimtype =  2       # extracellular voltage stimulus
stimulus[1].strength = -1e3     # voltage at B in mV

# electrode C
stimulus[2].stimtype =  3       # extracellular ground at C</code></pre>
<p>Run this experiment by</p>
<div class="sourceCode" id="cb9"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--stimulus</span> extra_V_bal <span class="at">--visualize</span></span></code></pre></div>
<h3 id="exp-stim-extra-V_OL"><strong>Experiment exp03 (extracellular
voltage stimulation with circuit break)</strong></h3>
<p>As in experiment exp01, the stimulus option <code>extra_V_OL</code>
sets up stimulation through the application of extracellular voltage.
The extracellular potential at electrode A is clamped to 2000 mV for a
duration of 2 ms. In contrast to the <code>extra_V</code> electrode A is
disconnected from the source after stimulus delivery. Therefore the
potental at A is allowed to float after the end of the stimulus, that
is, electrode A won't act as a ground after delivery of the voltage
pulse. This stimulation circuit corresponds to the setup shown in <code
class="interpreted-text"
role="numref">fig-extraV-wgnd-switched</code>.</p>
<pre><code>numstim = 2   # use two electrodes, A and B

# electrode A
stimulus[0].stimtype = 5        # extracellular voltage stimulus, switched
stimulus[0].strength = 2e3      # voltage at A in mV 

# electrode B
stimulus[1].stimtype = 3        # extracellular ground</code></pre>
<p>Run this experiment by</p>
<div class="sourceCode" id="cb11"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--stimulus</span> extra_V_OL <span class="at">--visualize</span></span></code></pre></div>
<h3 id="exp-stim-extra-I"><strong>Experiment exp04 (extracellular
current stimulation)</strong></h3>
<p>The stimulus option <code>extra_I</code> sets up stimulation through
the application of an extracellular current. Current is injected into
electrode A in the extracellular medium and withdrawn at electrode B,
which is grounded. The extracellular current strength is chosen to
induce an extracellular potential drop of 2000 mV across the strand as
before with extracellular voltage stimulation in <code>extra_V</code>.
This configuration corresponds to the setup shown in <code
class="interpreted-text" role="numref">fig-extraI-wGND</code>. Electrode
C is not used here, electrodes A and B are configured as follows:</p>
<pre><code>numstim = 2   # use two electrodes, A and B

# electrode A
stimulus[0].stimtype = 1        # extracellular current stimulus
stimulus[0].strength = 3.1e6    # current density at A in :math:`\mu A/cm^3`

# electrode B
stimulus[1].stimtype = 3        # extracellular ground</code></pre>
<p>Run this experiment by</p>
<div class="sourceCode" id="cb13"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--stimulus</span> extra_I <span class="at">--visualize</span></span></code></pre></div>
<h3 id="exp-stim-extra-I-bal"><strong>Experiment exp05 (balanced
extracellular current stimulation)</strong></h3>
<p>Similar to experiment exp04, the stimulus option
<code>extra_I_bal</code> sets up stimulation through the injection of
extracellular current with the difference being that B is not grounded.
Extracellular current is injected into electrode A and the same amount
is withdrawn from electrode B. As this constitutes a pure Neumann
problem to scenarios can be considered. Either electrode C is used as a
grounding electrode corresponding to <code class="interpreted-text"
role="numref">fig-extraI-symm-wgnd</code>, or we refrain from defining
an explicit grounding electrode and use a diffuse ground, that is, we
apply an additional constraint to deal with the Nullspace of the
problem. The latter option corresponds to <code class="interpreted-text"
role="numref">fig-extraI-symm-diffuse</code>. Both configuration also
induce an extracellular potential drop of 2000 mV across the strand,
albeit in a symmetric way. Note that in both cases the compatibility
condition must be satisfied. This is taken care of by CARPentry
internally by balancing the total current injected/withdrawn through
electrodes A and B. If balancing is imperfect a stimulation would also
occur around electrode C as all the excess current would drain
there.</p>
<p>In the setup with an explicit ground electrodes are defined as:</p>
<pre><code>numstim = 3   # use all three electrodes, A, B and C

# electrode A
stimulus[0].stimtype =  1       # extracellular voltage stimulus
stimulus[0].strength =  3.1e6   # current density at A in :math:`\mu A/cm^3` 

# electrode B
stimulus[1].stimtype =  1       # extracellular voltage stimulus
stimulus[1].balance  =  0       # no strength prescribed, balance with stimulus[0]

# electrode C
stimulus[2].stimtype =  3       # extracellular ground at C</code></pre>
<p>To solve the pure Neumann problem without an explicit ground the
input parameter <code>grounded</code> is set to <code>on</code>. In
fact, CARPentry turns on this mode automatically if a pure Neumann
configuration is detected.</p>
<pre><code>numstim = 2   # use electrodes A and B 

# electrode A
stimulus[0].stimtype =  1       # extracellular voltage stimulus
stimulus[0].strength =  3.1e6   # current density at A in :math:`\mu A/cm^3` 

# electrode B
stimulus[1].stimtype =  1       # extracellular voltage stimulus
stimulus[1].balance  =  0       # no strength prescribed, balance with stimulus[0]</code></pre>
<div class="sourceCode" id="cb16"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb16-1"><a href="#cb16-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--stimulus</span> extra_I_bal <span class="at">--visualize</span></span></code></pre></div>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>