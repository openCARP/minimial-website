<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Reentry induction protocols</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/21_reentry_induction/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Luca Azzolin <luca.azzolin@kit.edu></i>
<h2 id="tutorial-protocols">Overview</h2>
<p>Computational studies can be a useful tool to get deeper insights
into reentrant arrhythmias, such as atrial fibrillation (AF). Many
different protocols have been proposed to test AF inducibility both in
vivo and in silico. In this tutorial, we introduce openCARP examples of
some state-of-the-art methods as the rapid pacing (RP) and the phase
singularity distribution method (PSD). Furthermore, we cover the
recently proposed PEERP (pacing at the end of the effective period)
protocol to induce arrhythmia<a href="#fn1" class="footnote-ref"
id="fnref1" role="doc-noteref"><sup>1</sup></a>. In<a href="#fn2"
class="footnote-ref" id="fnref2" role="doc-noteref"><sup>2</sup></a>,
all these protocols were applied to bi-atrial volumetric models. Here,
we present a simple tissue patch setup simulation to reduce the
computational cost but still demonstrate the fundamental properties of
each method.</p>
<h2 id="objectives">Objectives</h2>
<p>This tutorial aims to:</p>
<ul>
<li>Provide access to state-of-the-art reentry induction protocols.</li>
<li>Examine the effect of the different parameters in each protocol on
arrhythmia induction.</li>
<li>Highlight that the choice of the inducing protocol has an influence
on both initiation and maintenance of arrhythmia.</li>
</ul>
<h2 id="setup">Setup</h2>
<p>A 2D tissue patch of size 5 cm x 5 cm and average edge length of 0.4
mm is generated. We include a variant of the Courtemanche et al. ionic
model reflecting AF-induced remodeling<a href="#fn3"
class="footnote-ref" id="fnref3" role="doc-noteref"><sup>3</sup></a> to
simulate the case of persistent AF. A conduction velocity of 0.3 m/s is
obtained by adjusting intra- and extracellular conductivities. A
circular fibrotic region of radius 1.42 cm is added at the center of the
tissue. To account for the presence of scar tissue, we set 30% of the
elements in this fibrotic region to almost non-conductive (1e-7 S/m). In
the other 70%, several ionic conductances were rescaled (50%
<code>gK1</code>, 60% <code>gNa</code> and 50% <code>gCaL</code>) to
consider effects of cytokines<a href="#fn4" class="footnote-ref"
id="fnref4" role="doc-noteref"><sup>4</sup></a>.</p>
<h2 id="protocols">Protocols</h2>
<p>The protocols included in this tutorial are:</p>
<ul>
<li><dl>
<dt><strong>Prepace:</strong></dt>
<dd>
<p>consists of a series of pulses at a fixed basic cycle length to let
the tissue reach a stable limit cycle. An activation time map is
computed for the last beat. This method will generate an intermediate
state to be loaded in the other protocols. This is not a protocol to
induce arrhythmia.</p>
</dd>
</dl></li>
<li><dl>
<dt><strong>Rapid pacing:</strong></dt>
<dd>
consists of a train of electrical stimulations with decreasing coupling
interval. You can give multiple beats with the same cycle length and
check for arrhythmia induction:
<ol>
<li>at the end of the protocol (RP<span
class="math inline">\(_\mathrm{E}\)</span>)</li>
<li>after each beat (RP<span
class="math inline">\(_\mathrm{B}\)</span>).</li>
</ol>
</dd>
</dl></li>
<li><dl>
<dt><strong>Phase singularity distribution:</strong></dt>
<dd>
<p>consists of manually placing phase singularities on the geometrical
model and then solving the Eikonal equation to estimate the activation
time map <a href="#fn5" class="footnote-ref" id="fnref5"
role="doc-noteref"><sup>5</sup></a>. Based on this initial state, you
can simulate electrical wave propagation by solving the monodomain
equation.</p>
</dd>
</dl></li>
<li><dl>
<dt><strong>Pacing at the end of the effective refractory
period:</strong></dt>
<dd>
<p>triggers ectopic beats at the end of the effective refractory period,
automatically computed as the minimum coupling interval at which the
action potential could propagate in the tissue. Method presented in
Azzolin et al.<a href="#fn6" class="footnote-ref" id="fnref6"
role="doc-noteref"><sup>6</sup></a>.</p>
</dd>
</dl></li>
</ul>
<p>We suggest to run the experiments with multiple processors to speed
up the simulation (<code>--np &gt;2</code>).</p>
<h2 id="main-input-parameters">Main Input Parameters</h2>
<p>The following input parameters are exposed to steer the
experiment:</p>
<pre><code>--protocol {prepace,RP_E,RP_B,PSD,PEERP}     
                      Protocol to run (default: PEERP)
--slabsize SLABSIZE 
                      Block side length in [um] (default: 50000 um).
--resolution RESOLUTION
                      mesh resolution in [um] (default: 400 um).
--cv cv
                      conduction velocity in m/s (default: 0.3 m/s)              </code></pre>
<h2 id="prepace_exp">Prepace experiment</h2>
<p>We can get close to a stable limit cycle in the tissue simulation by
running a prepace protocol to prepare for the actual arrhythmia
induction protocols. By default, it will stimulate 5 planar waves beats
at a basic cycle length of 500 ms from the left side of the block and
compute an activation map of the last beat.</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--np</span> 2 <span class="at">--protocol</span> prepace <span class="at">--prepace_bcl</span> 500 <span class="at">--prebeats</span> 4 <span class="at">--visualize</span></span></code></pre></div>
<p>We show the activation spread in terms of transmembrane voltage
V<span class="math inline">\(_\mathrm{m}\)</span> during prepacing in
<code class="interpreted-text"
role="numref">fig-tutorial-protocols-prepace</code>.</p>
<div id="fig-tutorial-protocols-prepace">
<figure>
<img src="02_21_prepace.gif" class="align-center" style="width:75.0%"
alt="Prepace of the tissue patch. The spread of activation and repolarization is shown in terms of transmembrane voltage V_\mathrm{m}." />
<figcaption aria-hidden="true">Prepace of the tissue patch. The spread
of activation and repolarization is shown in terms of transmembrane
voltage V<span class="math inline">\(_\mathrm{m}\)</span>.</figcaption>
</figure>
</div>
<h2 id="phase-singularity-distribution-experiment">Phase singularity
distribution experiment</h2>
<p>We place a phase singularity at the center of the tissue patch and
solve the monodomain system using the last activation time map obtained
by solving the Eikonal solution as initial state<a href="#fn7"
class="footnote-ref" id="fnref7"
role="doc-noteref"><sup>7</sup></a>].</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--np</span> 2 <span class="at">--protocol</span> PSD <span class="at">--visualize</span></span></code></pre></div>
<p>The phase map produced by solving the Laplace equation after placing
a phase singularity in the center is shown in <code
class="interpreted-text"
role="numref">fig-tutorial-protocols-laplace</code>.</p>
<div id="fig-tutorial-protocols-laplace">
<figure>
<img src="02_21_phase_laplace.png" class="align-center"
style="width:75.0%" alt="The phase ranges from -pi to +pi." />
<figcaption aria-hidden="true">The phase ranges from -pi to
+pi.</figcaption>
</figure>
</div>
<p>The last activation time yielded by the Eikonal system is shown in
<code class="interpreted-text"
role="numref">fig-tutorial-protocols-LAT</code>.</p>
<div id="fig-tutorial-protocols-LAT">
<figure>
<img src="02_21_LAT.png" class="align-center" style="width:75.0%"
alt="Last activation time map ranging from 0 to 168 ms." />
<figcaption aria-hidden="true">Last activation time map ranging from 0
to 168 ms.</figcaption>
</figure>
</div>
<p>The spread of activation in terms of transmembrane voltage V<span
class="math inline">\(_\mathrm{m}\)</span> is shown in <code
class="interpreted-text"
role="numref">fig-tutorial-protocols-PSD</code>. The activation time map
obtained by solving the Eikonal equation was used to initalize a
mondomain simulation.</p>
<div id="fig-tutorial-protocols-PSD">
<figure>
<img src="02_21_PSD.gif" class="align-center" style="width:75.0%"
alt="The spread of activation and repolarization is shown in terms of transmembrane voltage V_\mathrm{m}." />
<figcaption aria-hidden="true">The spread of activation and
repolarization is shown in terms of transmembrane voltage V<span
class="math inline">\(_\mathrm{m}\)</span>.</figcaption>
</figure>
</div>
<h2 id="RP_E">Rapid pacing with arrhythmia checking at the end of the
experiment</h2>
<p>The rapid pacing with arrhythmia checking at the end consists of a
train of pulses with decreasing coupling interval and the success of
arrhythmia initiation is inspected only after all electrical
stimulations were applied. The state saved after prepacing (prepace
experiment) is used as initial state.</p>
<p>To run the example, execute:</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--np</span> 2 <span class="at">--protocol</span> RP_E <span class="at">--start_bcl</span> 200 <span class="at">--end_bcl</span> 130 <span class="at">--max_n_beats_RP</span> 1 <span class="at">--visualize</span></span></code></pre></div>
<p>We will get a sustained reentry after the end of the pacing protocol.
We show the activation spread in terms of transmembrane voltage V<span
class="math inline">\(_\mathrm{m}\)</span> in <code
class="interpreted-text"
role="numref">fig-tutorial-protocols-RP_E_130</code>.</p>
<div id="fig-tutorial-protocols-RP_E_130">
<figure>
<img src="02_21_RP_E_end_bcl_130.gif" class="align-center"
style="width:75.0%"
alt="Reentry induced and maintained by implementing a rapid train of stimulations with shortest coupling interval of 130 ms. The spread of activation and repolarization is shown in terms of transmembrane voltage V_\mathrm{m}." />
<figcaption aria-hidden="true">Reentry induced and maintained by
implementing a rapid train of stimulations with shortest coupling
interval of 130 ms. The spread of activation and repolarization is shown
in terms of transmembrane voltage V<span
class="math inline">\(_\mathrm{m}\)</span>.</figcaption>
</figure>
</div>
<p>In order to investigate what happens if we stop at a coupling
interval of 140 ms, run:</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--np</span> 2 <span class="at">--protocol</span> RP_E <span class="at">--start_bcl</span> 200 <span class="at">--end_bcl</span> 140 <span class="at">--max_n_beats_RP</span> 1 <span class="at">--visualize</span></span></code></pre></div>
<p>In this case, we will not get a sustained reentry. Conversely, we can
notice how the last beat is terminating the arrhythmia initiated by the
beat before. This highlights how during a pacing protocol we can also
terminate a previoulsy initiated reentry. We show the activation spread
in terms of transmembrane voltage V<span
class="math inline">\(_\mathrm{m}\)</span> in <code
class="interpreted-text"
role="numref">fig-tutorial-protocols-RP_E_140</code>.</p>
<div id="fig-tutorial-protocols-RP_E_140">
<figure>
<img src="02_21_RP_E_end_bcl_140.gif" class="align-center"
style="width:75.0%"
alt="No reentry induced by a rapid train of stimulations with shortest coupling interval of 140 ms. The spread of activation and repolarization is shown in terms of transmembrane voltage V_\mathrm{m}." />
<figcaption aria-hidden="true">No reentry induced by a rapid train of
stimulations with shortest coupling interval of 140 ms. The spread of
activation and repolarization is shown in terms of transmembrane voltage
V<span class="math inline">\(_\mathrm{m}\)</span>.</figcaption>
</figure>
</div>
<h1
id="rapid-pacing-with-arrhythmia-checking-after-every-beat-experiment">Rapid
pacing with arrhythmia checking after every beat experiment</h2>
<p>The rapid pacing with arrhythmia checking after every beat consists
of a train of pulses with decreasing coupling interval and the success
of arrhythmia initiation is inspected after each stimulation. If an
arrhythmia is induced, we will no longer stimulate. The state saved
after prepacing (prepace experiment) is used as initial state.</p>
<p>To run the example:</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--np</span> 2 <span class="at">--protocol</span> RP_B <span class="at">--start_bcl</span> 200 <span class="at">--end_bcl</span> 130 <span class="at">--max_n_beats_RP</span> 1 <span class="at">--visualize</span></span></code></pre></div>
<p>We get a sustained reentry after the beat with a coupling interval of
150 ms, therefore the algorithm will stop pacing even if the the
shortest coupling interval was not reached yet. This means that it could
happen to induce and maintain a reentrant wave even without giving the
whole train of pulses as in the previous experiment (which gives the
reentry induced by a coupling interval of 130 ms as final output).</p>
<h1
id="pacing-at-the-end-of-the-effective-refractory-period-experiment">Pacing
at the end of the effective refractory period experiment</h2>
<p>We use the state saved after prepacing (prepace experiment) as
initial state. The first ectopic beat is given at the minimum coupling
interval time which allows for wave propagation. The timing of the
ectopic beat V<span class="math inline">\(n+1\)</span> is defined by
identifing the end of the refractory period of beat V<span
class="math inline">\(n\)</span> automatically.</p>
<p>To run the example:</p>
<div class="sourceCode" id="cb7"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--np</span> 2 <span class="at">--protocol</span> PEERP <span class="at">--max_n_beats_PEERP</span> 2 <span class="at">--visualize</span></span></code></pre></div>
<p>We induce a reeentrant wave with only 2 ectopic beats after the last
prepacing beat. We show the activation spread resulting from the first
beat in terms of transmembrane voltage V<span
class="math inline">\(_\mathrm{m}\)</span> in <code
class="interpreted-text"
role="numref">fig-tutorial-protocols-PEERP_1_no</code>.</p>
<div id="fig-tutorial-protocols-PEERP_1_no">
<figure>
<img src="02_21_beat1_no_reentry.gif" class="align-center"
style="width:75.0%"
alt="One beat at the end of the effective refractory period did not induce reentrant arrhythmia." />
<figcaption aria-hidden="true">One beat at the end of the effective
refractory period did not induce reentrant arrhythmia.</figcaption>
</figure>
</div>
<p>We show the activation spread resulting from the second beat in terms
of transmembrane voltage V<span
class="math inline">\(_\mathrm{m}\)</span> in <code
class="interpreted-text"
role="numref">fig-tutorial-protocols-PEERP_2</code>.</p>
<div id="fig-tutorial-protocols-PEERP_2">
<figure>
<img src="02_21_beat2.gif" class="align-center" style="width:75.0%"
alt="The second ectopic beat induced a sustained arrhythmia." />
<figcaption aria-hidden="true">The second ectopic beat induced a
sustained arrhythmia.</figcaption>
</figure>
</div>
<p><strong>References</strong></p>
<section class="footnotes footnotes-end-of-document"
role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p><em>Azzolin L, Doessel O, Loewe
A</em>, <strong>A Reproducible Protocol to Assess Arrhythmia
Vulnerability in silico: Pacing at the End of the Effective Refractory
Period.</strong>, Frontiers in Physiology. 2021 April;12:656411. Doi:
10.3389/fphys.2021.656411 <a
href="https://www.frontiersin.org/articles/10.3389/fphys.2021.656411/full">[Journal]</a><a
href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn2" role="doc-endnote"><p><em>Azzolin L, Doessel O, Loewe
A</em>, <strong>A Reproducible Protocol to Assess Arrhythmia
Vulnerability in silico: Pacing at the End of the Effective Refractory
Period.</strong>, Frontiers in Physiology. 2021 April;12:656411. Doi:
10.3389/fphys.2021.656411 <a
href="https://www.frontiersin.org/articles/10.3389/fphys.2021.656411/full">[Journal]</a><a
href="#fnref2" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn3" role="doc-endnote"><p><em>Loewe A, Wilhelms M, Doessel O,
Seemann G</em> <strong>Influence of chronic atrial fibrillation induced
remodeling in a computational electrophysiological model.</strong>
Biomedical Engineering. 2014 Jan;59(suppl 1):S929--S932. Doi:
10.1515/bmt-2014-5012__<a href="#fnref3" class="footnote-back"
role="doc-backlink">↩︎</a></p></li>
<li id="fn4" role="doc-endnote"><p><em>Roney CH, Bayer JD, Zahid S, Meo
M, Boyle PM, Trayanova NA, Haissaguerre M, Dubois R, Cochet H, Vigmond
EJ</em> <strong>Modelling methodology of atrial fibrosis affects rotor
dynamics and electrograms.</strong> Europace. 2016 Dec;18(suppl
4):iv146-iv155. <a
href="https://pubmed.ncbi.nlm.nih.gov/28011842">[Pubmed]</a> <a
href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6279153/">[PMC]</a><a
href="#fnref4" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn5" role="doc-endnote"><p><em>Jacquemet V</em> <strong>An
Eikonal approach for the initiation of reentrant cardiac propagation in
reaction-diffusion models.</strong> IEEE Trans Biomed Eng.
2010;57(9):2090-2098. <a
href="https://pubmed.ncbi.nlm.nih.gov/20515704">[Pubmed]</a><a
href="#fnref5" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn6" role="doc-endnote"><p><em>Azzolin L, Doessel O, Loewe
A</em>, <strong>A Reproducible Protocol to Assess Arrhythmia
Vulnerability in silico: Pacing at the End of the Effective Refractory
Period.</strong>, Frontiers in Physiology. 2021 April;12:656411. Doi:
10.3389/fphys.2021.656411 <a
href="https://www.frontiersin.org/articles/10.3389/fphys.2021.656411/full">[Journal]</a><a
href="#fnref6" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn7" role="doc-endnote"><p><em>Jacquemet V</em> <strong>An
Eikonal approach for the initiation of reentrant cardiac propagation in
reaction-diffusion models.</strong> IEEE Trans Biomed Eng.
2010;57(9):2090-2098. <a
href="https://pubmed.ncbi.nlm.nih.gov/20515704">[Pubmed]</a><a
href="#fnref7" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
