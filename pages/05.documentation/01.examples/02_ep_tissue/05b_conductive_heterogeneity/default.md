---
description: This tutorial details how to assign different conductivities to different
  parts of a simulated tissue slice using region-wise tagging
image: 02_05B_Conductive_Heterogeneity_Fig4_nonplanar_stim.png
title: Regions & conductivities
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Conductive heterogeneity (gregion)</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/05B_Conductive_Heterogeneity/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Patrick Boyle <pmjboyle@gmail.com></i>
<div id="tutorial-conductive-heterogeneity">
<p>This example details how to assign different conductivities to
different parts of a simulated tissue slice using region-wise tagging.
To run the experiments of this example change directories as
follows:</p>
</div>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/05B_Conductive_Heterogeneity</span></code></pre></div>
<h2 id="problem-setup">Problem Setup</h2>
<p>This example will run one simulations using a 2D sheet model (1 cm x
1 cm) that has been divided into four regions (striped horizontally from
top to bottom, each occupying 1/4 of the total mesh). Test parameters
can be modified to explore the consequences of setting conductivity
different values, adjusting anisotropy ratio (i.e.,
longitudinal:transverse), adjusting whether the different regions are
electrically coupled to each other or isolated, and choose the
electrical stimulus type.</p>
<h2 id="usage">Usage</h2>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--reg0_giL</span>          = 2.00000  <span class="co"># conductivity along fibre direction (i.e., :math:`\sigma_{iL}`) in region 0 (bottom slice)</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--reg1_giL</span>          = 0.50000  <span class="co"># as above, but for second region</span></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--reg2_giL</span>          = 0.12500  <span class="co"># as above, but for third region</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--reg3_giL</span>          = 0.03125  <span class="co"># as above, but for fourth region</span></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--anisotropy_factor</span> = 4        <span class="co"># factor by which transverse conductivities are smaller than longitudinal (i.e., :math:`\sigma_{iL}`::math:`\sigma_{iT}` = [factor]:1)</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--split</span>             = 0        <span class="co"># flag that can be asserted to electrically isolate regions instead of having them coupled to each other</span></span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a>         <span class="ex">--non_planar_stim</span>   = 0        <span class="co"># flag that can be asserted to use one point stimuli in each region instead of a planar stimulus</span></span></code></pre></div>
<p>If the program is run with the <code>--visualize</code> option,
meshalyzer will automatically load a map showing the activation sequence
in response to electrical stimulation.</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--visualize</span></span></code></pre></div>
<h2 id="interpreting-results">Interpreting Results</h2>
<p>By default, the activation sequence in response to simulation will be
as below:</p>
<div id="fig-conductive-heterogeneity-default-acts">
<figure>
<img
src="05b_conductive_heterogeneity/02_05B_Conductive_Heterogeneity_Fig1_default_acts.png"
class="align-center"
alt="2D map of activation times (t_{act}) in response to stimulation at the left-hand side of the sheet with default parameters. Note the delayed activation in the top part of the sheet." />
<figcaption aria-hidden="true">2D map of activation times (<span
class="math inline">\(t_{act}\)</span>) in response to stimulation at
the left-hand side of the sheet with default parameters. Note the
delayed activation in the top part of the sheet.</figcaption>
</figure>
</div>
<p>There are several noteworthy observations:</p>
<ul>
<li>As expected, the conduction velocity in the bottom region (which has
the highest conductivity value assigned) is faster than in the regions
above.</li>
<li>Since the stimulus is applied to the left-hand side of the sheet,
the resulting wavefronts in each region are all approximately planar. As
such, the effect of tissue anisotropy is relatively minimal in this
example.</li>
<li>The conduction velocity (CV) values in the different regions can be
(very roughly) approximated by taking the activation times along the
righthand side of the sheet and dividing the width of the mesh (1 cm) by
those values:</li>
</ul>
<figure>
<img
src="05b_conductive_heterogeneity/02_05B_Conductive_Heterogeneity_Fig2_approx_CV.png"
class="align-center"
alt="Graph of approximate conduction velocity (CV) as a function of distance along the y-axis, from bottom to top. See text for details." />
<figcaption aria-hidden="true">Graph of approximate conduction velocity
(CV) as a function of distance along the y-axis, from bottom to top. See
text for details.</figcaption>
</figure>
<p>To get a better idea of the exact relationship between conductivity
values and CV, re-run the example with the <code>--split</code>
option:</p>
<figure>
<img
src="05b_conductive_heterogeneity/02_05B_Conductive_Heterogeneity_Fig3_split_acts.png"
class="align-center"
alt="Activation sequence produced by asserting the --split flag. The different regions are now electrically de-coupled from each other." />
<figcaption aria-hidden="true">Activation sequence produced by asserting
the <code>--split</code> flag. The different regions are now
electrically de-coupled from each other.</figcaption>
</figure>
<p>Now that the individual regions are electrically isolated from each
other, we can observe the "native" relationship between conductivity and
CV. Here, we see that the approximate CV values in the four regions are,
from top to bottom:</p>
<ul>
<li>0: 102.7 cm/s (<span class="math inline">\(\sigma_{iL}\)</span> =
2.00000 mS/mm)</li>
<li>1: 77.7 cm/s (<span class="math inline">\(\sigma_{iL}\)</span> =
0.50000 mS/mm)</li>
<li>2: 47.5 cm/s (<span class="math inline">\(\sigma_{iL}\)</span> =
0.12500 mS/mm)</li>
<li>3: 25.6 cm/s (<span class="math inline">\(\sigma_{iL}\)</span> =
0.03125 mS/mm)</li>
</ul>
<p>Since the conductivity values in adjacent regions are separated by a
factor of 4, this serves as a reminder that the approximation <span
class="math inline">\(\Delta\,CV\propto\sqrt{\Delta\,\sigma}\)</span>,
derived from the linear core conductor model of bioelectric propagation,
does not generally hold up very well in the context of mondomain
simulations of cardiac tissue.</p>
<p>Finally, to get a ckear idea of how the anisotropy ratio affects the
properties of impulse propagation, re-run the example with the
<code>--non_planar_stim</code> option:</p>
<figure>
<img
src="05b_conductive_heterogeneity/02_05B_Conductive_Heterogeneity_Fig4_nonplanar_stim.png"
class="align-center"
alt="Activation sequence produced by asserting the --non_planar_stim flag. There is now a discrete point stimulus in each region instead of one big planar stimulus." />
<figcaption aria-hidden="true">Activation sequence produced by asserting
the <code>--non_planar_stim</code> flag. There is now a discrete point
stimulus in each region instead of one big planar stimulus.</figcaption>
</figure>
<p>Note that wavefronts now emanate from four point sources along the
left-hand side of the sheet and the activation isolines are clearly
ellipsoidal, with propagation in the Y-axis direction slower than that
in the X-axis direction. This behaviour arises from the fact that the
fibre orientations in this sheet are uniformly from left to right (i.e.,
all entries in the .lon file are &lt;1 0 0&gt;.</p>
<p>This example highlights the effect of reduced transverse conductivity
(<span class="math inline">\(\sigma_{iT}\)</span>), in this case by a
factor of 4. The <code>--anisotropy_factor</code> argument can be
adjusted to explore the effect of different magnitudes of
adjustment.</p>
<p>One final interesting observation from the last simulation discussed
above, note that although the CV in the bottom-most stripe is the
fastest of the four regions (as expected), the wavefront actually
arrives at the right side of the sheet later than for the stripe above.
This is because the source-sink dynamics at the stimulation site vary
between regions. In this case, we observe that the dramatically
increased coupling in the bottom-most stripe mean that excitation
dissipates more quickly from the stimulus site (i.e., the tissue is more
"sink-ey", meaning that we are closer to the critical threshold below
which the stimulus will not elicit a propagating response.</p>
<h2 id="whats-going-on-under-the-hood">What's Going On Under The
Hood?</h2>
<p>The relevant part of the .par file for this example is shown
below:</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="co">#############################################################</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="ex">num_gregions</span>             = 4</span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a><span class="co">#############################################################</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.g_il</span>          = 2.0</span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.g_it</span>          = 2.0</span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.g_in</span>          = 2.0</span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.num_IDs</span>       = 1</span>
<span id="cb4-8"><a href="#cb4-8" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.ID[0]</span>         = 0</span>
<span id="cb4-9"><a href="#cb4-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-10"><a href="#cb4-10" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.g_il</span>          = 0.5</span>
<span id="cb4-11"><a href="#cb4-11" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.g_it</span>          = 0.5</span>
<span id="cb4-12"><a href="#cb4-12" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.g_in</span>          = 0.5</span>
<span id="cb4-13"><a href="#cb4-13" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.num_IDs</span>       = 1</span>
<span id="cb4-14"><a href="#cb4-14" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.ID[0]</span>         = 1</span>
<span id="cb4-15"><a href="#cb4-15" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-16"><a href="#cb4-16" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.g_il</span>          = 0.125</span>
<span id="cb4-17"><a href="#cb4-17" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.g_it</span>          = 0.125</span>
<span id="cb4-18"><a href="#cb4-18" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.g_in</span>          = 0.125</span>
<span id="cb4-19"><a href="#cb4-19" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.num_IDs</span>       = 1</span>
<span id="cb4-20"><a href="#cb4-20" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.ID[0]</span>         = 2</span>
<span id="cb4-21"><a href="#cb4-21" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-22"><a href="#cb4-22" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.g_il</span>          = 0.03125</span>
<span id="cb4-23"><a href="#cb4-23" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.g_it</span>          = 0.03125</span>
<span id="cb4-24"><a href="#cb4-24" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.g_in</span>          = 0.03125</span>
<span id="cb4-25"><a href="#cb4-25" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.num_IDs</span>       = 1</span>
<span id="cb4-26"><a href="#cb4-26" aria-hidden="true" tabindex="-1"></a><span class="va">gregion</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.ID[0]</span>         = 3</span></code></pre></div>
<p>Each gregion[] structure contains information about a different
stripe in the mesh. The number of gregion[] entries is controlled by the
num_gregions variable. For the purposes of this exercise, the main
variables changed by the command-line arguments are the .g_il and .g_it
entries, which control the conductivity values (<span
class="math inline">\(\sigma_{iL}\)</span> and <span
class="math inline">\(\sigma_{iT}\)</span>, respectively) in the
relevant tagged regions. Although the fibres in this example are
uniformly oriented from left to right, this is frequently not the case
in organ-scale simulations, due to the laminar structure of cardiac
tissue. In general, for each element in a region, the "longitudinal"
direction is defined by the corresponding vector entry in the .lon file.
Also note, the .g_in entries are not used except when simulations are
conducted using meshes that have two vectors defined for each element
instead of just one (i.e., longitudinal instead of transverse) --
otherwise, the assumption is that <span
class="math inline">\(\sigma_{iL} = \sigma_{iT}\)</span>. Example .lon
file headers are shown below:</p>
<p>The relevant part of the .par file for this example is shown
below:</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="co"># .lon file with one set of vectors</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a><span class="ex">1</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a><span class="ex">0.6</span> 0.4 0</span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a><span class="ex">0.4</span> 0.6 0</span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a><span class="co"># ... (one line per element in .elem file)</span></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true" tabindex="-1"></a><span class="co"># .lon file with one set of vectors</span></span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true" tabindex="-1"></a><span class="ex">2</span></span>
<span id="cb5-9"><a href="#cb5-9" aria-hidden="true" tabindex="-1"></a><span class="ex">1</span> 0 0 0 1 0</span>
<span id="cb5-10"><a href="#cb5-10" aria-hidden="true" tabindex="-1"></a><span class="ex">1</span> 0 0 0 1 0</span>
<span id="cb5-11"><a href="#cb5-11" aria-hidden="true" tabindex="-1"></a><span class="co"># ... (one entry per element in .elem file)</span></span></code></pre></div>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>