---
description: This example details how to assign different single cell dynamics to
  different parts of a simulated tissue slice using region-wise tagging
image: 02_05C_Conductive_Heterogeneity_Fig2_Incr-gNa.png
title: EP heterogeneity
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Cellular dynamics heterogeneity (imp_region)</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/05C_Cellular_Dynamics_Heterogeneity/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Patrick Boyle <pmjboyle@gmail.com></i>
<p>This example details how to assign different cell-scale dynamics to
different parts of a simulated tissue slice using region-wise tagging.
To run the experiments of this example change directories as
follows:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/05C_Cellular_Dynamics_Heterogeneity</span></code></pre></div>
<h2 id="problem-setup">Problem Setup</h2>
<p>This example will run one simulation using a 2D sheet model (1 cm x 1
cm) that has been divided into four regions (striped horizontally from
top to bottom, each occupying 1/4 of the total mesh). Test parameters
can be modified to explore the consequences of setting different
cell-scale dynamics in the four regions. Optional flags can be used to
modify the stimulus type or determine whether or not the different
regions are electrically coupled.</p>
<h2 id="usage">Usage</h2>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span> </span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--variable</span>            { GNa OR GKs OR GKr OR GK1 OR Gss OR Gtof OR Gtos }</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>                        <span class="ex">parameter</span> that should be tuned up <span class="er">(</span><span class="ex">or</span> down, if <span class="at">--tune_down</span> is asserted</span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--tune_down</span>           tune selected variable down instead of up</span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--split</span>               electrically isolate regions</span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--non_planar_stim</span>     use point stimuli in each region instead of a planar stimulus</span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--show_vm</span>             visualize vm<span class="er">(</span><span class="ex">t</span><span class="kw">)</span> <span class="ex">instead</span> of depolarization times</span></code></pre></div>
<p>If the program is run with the <code>--visualize</code> option,
meshalyzer will automatically load a map showing the activation sequence
in response to electrical stimulation, unless the <code>--show-vm</code>
argument is given, in which case vm(t) is shown instead. Note that
asserting the latter flag also prompts the program to run a longer
simulation (300 ms instead of 50 ms) so that depolarization and
repolarization can both be appreciated.</p>
<h2 id="key-parameters">Key Parameters</h2>
<p>The first key parameter is <code>--variable</code>. This will be set
to one of seven possible values in order to modulate one of the possible
ion channel conductances in the UCLA rabbit ionic model:</p>
<ul>
<li>GNa: fast sodium current conductance (default)</li>
<li>GKs: slow delayed rectifier potassium current conductance</li>
<li>GKr: rapid delayed rectifier potassium current conductance</li>
<li>GK1: inward rectifier potassium current conductance</li>
<li>Gss: steady-state potassium current conductance</li>
<li>Gtof: fast inactivating transient outward potassium current
conductance</li>
<li>Gtos: slow inactivating transient outward potassium current
conductance</li>
</ul>
<p>The slab is subdivided into four regions. The bottom-most stripe will
be simulated under control conditions. The second stripe from the bottom
will be simulated with the selected <code>--variable</code> either
increased or decreased by a factor of two; this is where the second key
parameter, <code>--tune_down</code>, comes into play: if that flag is
asserted, the conductances of the selected <code>--variable</code> are
decreased in stripes above the bottom-most; otherwise, the values are
increased:</p>
<div id="fig-conductive-heterogeneity">
<figure>
<img
src="05c_cellular_dynamics_heterogeneity/02_05C_Conductive_Heterogeneity_Fig1_schematic.png"
class="align-center"
alt="Schematic showing the division of the tissue sheet into five distinct regions, each of which will have different cellular dynamics. The selected ion channel conductance is scaled UP from bottom to top by default, unless the --tune_down flag is asserted." />
<figcaption aria-hidden="true">Schematic showing the division of the
tissue sheet into five distinct regions, each of which will have
different cellular dynamics. The selected ion channel conductance is
scaled UP from bottom to top by default, unless the
<code>--tune_down</code> flag is asserted.</figcaption>
</figure>
</div>
<h2 id="examples">Examples</h2>
<h3 id="example-1-default-parameters-increased-gna">Example 1: Default
parameters (increased gNa)</h3>
<p>The activation sequence for the default parameters looks like
this:</p>
<div id="fig-conductive-heterogeneity-Incr-gNa">
<figure>
<img
src="05c_cellular_dynamics_heterogeneity/02_05C_Conductive_Heterogeneity_Fig2_Incr-gNa.png"
class="align-center"
alt="Activation sequence in response to stimulation at the left side of the sheet for the default parameter set, for which fast sodium channel conductance (G_{Na}) is scaled up from 1x at the bottom to 8x at the top." />
<figcaption aria-hidden="true">Activation sequence in response to
stimulation at the left side of the sheet for the default parameter set,
for which fast sodium channel conductance (<span
class="math inline">\(G_{Na}\)</span>) is scaled up from 1x at the
bottom to 8x at the top.</figcaption>
</figure>
</div>
<p>Note the marked acceleration of conduction velocity (CV) in the upper
regions, which are simulated with increased gNa.</p>
<h3 id="example-2-decreased-gna">Example 2: Decreased gNa</h3>
<p>If the simulation above is repeated with the <code>--tune_down</code>
flag asserted, the resulting activation sequence looks like this:</p>
<div id="fig-conductive-heterogeneity-Decr-gNa">
<figure>
<img
src="05c_cellular_dynamics_heterogeneity/02_05C_Conductive_Heterogeneity_Fig3_Decr-gNa.png"
class="align-center"
alt="Activation sequence in response to stimulation at the left side of the sheet when the --tune_down flag is asserted, so fast sodium channel conductance (G_{Na}) is down from 1x at the bottom to 1/8 at the top." />
<figcaption aria-hidden="true">Activation sequence in response to
stimulation at the left side of the sheet when the
<code>--tune_down</code> flag is asserted, so fast sodium channel
conductance (<span class="math inline">\(G_{Na}\)</span>) is down from
1x at the bottom to 1/8 at the top.</figcaption>
</figure>
</div>
<p>Note that the scale bar for this figure differs from the image shown
above. Here, there is dramatic CV slowing in the upper regions.</p>
<h3 id="example-3-effect-of-optional-parameters">Example 3: Effect of
optional parameters</h3>
<p>If the <code>--split</code> and <code>--non_planar_stim</code> flags
are asserted instead of the <code>--tune_down</code> flag, the following
activation pattern is observed:</p>
<figure>
<img
src="05c_cellular_dynamics_heterogeneity/02_05C_Conductive_Heterogeneity_Fig4_Incr-gNa+Split+NonPlanar.png"
class="align-center"
alt="Effect of asserting --split and --non_planar_stim flags. Regions are electrically separated and stimulated from point sources." />
<figcaption aria-hidden="true">Effect of asserting <code>--split</code>
and <code>--non_planar_stim</code> flags. Regions are electrically
separated and stimulated from point sources.</figcaption>
</figure>
<h3 id="example-4-varying-repolarizing-currents">Example 4: Varying
repolarizing currents</h3>
<p>Finally, as an initial example of how the experiment can be used to
explore the effects of changing repolarizing current instead of
depolarizing currents, when the <code>--variable=GKr</code> and
<code>--show_vm</code> arguments are given, the following excitation
sequence is seen:</p>
<figure>
<img
src="05c_cellular_dynamics_heterogeneity/02_05C_Conductive_Heterogeneity_Fig5_nosplit_planarstim_Incr-gKr.gif"
class="align-center"
alt="Membrane voltage over time (V_m(t)), which can be visualized instaed of activation sequence, which makes it easier to observe heterogeneities in APD produced by scaling repolarizing current conductance instead of g_{Na}." />
<figcaption aria-hidden="true">Membrane voltage over time (<span
class="math inline">\(V_m(t)\)</span>), which can be visualized instaed
of activation sequence, which makes it easier to observe heterogeneities
in APD produced by scaling repolarizing current conductance instead of
<span class="math inline">\(g_{Na}\)</span>.</figcaption>
</figure>
<p>Note that repolarization occurs much earlier in the upper layers of
the sheet in this case, due to the large increase in IKr resulting from
the change in GKr.</p>
<h2 id="whats-going-on-under-the-hood">What's Going On Under The
Hood?</h2>
<p>The relevant part of the .par file for this example is shown
below:</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="co">#############################################################</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="ex">num_imp_regions</span>          = 4</span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a><span class="co">#############################################################</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.im</span>         = MahajanShiferaw</span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.num_IDs</span>    = 1</span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.ID[0]</span>      = 0</span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.im_param</span>   = <span class="st">&quot;GNa*1&quot;</span></span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.im</span>         = MahajanShiferaw</span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.num_IDs</span>    = 1</span>
<span id="cb3-11"><a href="#cb3-11" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.ID[0]</span>      = 1</span>
<span id="cb3-12"><a href="#cb3-12" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.im_param</span>   = <span class="st">&quot;GNa*2&quot;</span></span>
<span id="cb3-13"><a href="#cb3-13" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-14"><a href="#cb3-14" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.im</span>         = MahajanShiferaw</span>
<span id="cb3-15"><a href="#cb3-15" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.num_IDs</span>    = 1</span>
<span id="cb3-16"><a href="#cb3-16" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.ID[0]</span>      = 2</span>
<span id="cb3-17"><a href="#cb3-17" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.im_param</span>   = <span class="st">&quot;GNa*4&quot;</span></span>
<span id="cb3-18"><a href="#cb3-18" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-19"><a href="#cb3-19" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.im</span>         = MahajanShiferaw</span>
<span id="cb3-20"><a href="#cb3-20" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.num_IDs</span>    = 1</span>
<span id="cb3-21"><a href="#cb3-21" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.ID[0]</span>      = 3</span>
<span id="cb3-22"><a href="#cb3-22" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.im_param</span>   = <span class="st">&quot;GNa*8&quot;</span></span></code></pre></div>
<p>Each imp_region[] structure contains information about a different
stripe in the mesh. The number of imp_region[] entries is controlled by
the num_imp_regions variable. For the purposes of this exercise, the
main variables changed by the command-line arguments are the .im_param
entries, which control the ionic model parameters passed to the LIMPET
interface by openCARP. For any particular ionic model, the list of
possible paramters can be obtained by running:</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">bench</span> <span class="at">--imp</span><span class="op">=</span>[im name] <span class="at">--imp-info</span></span></code></pre></div>
<h2 id="important-note">Important Note</h2>
<p>It is important to appreciate that that there is ambiguity in the
determination of which nodes belong to which regions, since some nodes
are found on the boundary between two or more regions:</p>
<figure>
<img
src="05c_cellular_dynamics_heterogeneity/02_05C_Conductive_Heterogeneity_Fig6_Node-Element-Ambiguity.png"
class="align-center"
alt="Schematic illustrating ambiguity in determination of which nodes belong to which regions." />
<figcaption aria-hidden="true">Schematic illustrating ambiguity in
determination of which nodes belong to which regions.</figcaption>
</figure>
<p>In this case, the red nodes clearly belong to whatever imp_region
includes <code>.ID[] = 1</code> in its list of element IDs. Likewise,
the cell-wise identity of the blue nodes is clear. However, if tags 1
and 2 are assigned to different imp_region[] entries, it is unclear
which entry should apply to the green nodes.</p>
<p>openCARP resolves this ambiguity on the basis of the order of
imp_region[] entries. For this case, let's say "0" is in the tag list
imp_region[X].ID and "1" is in a different tag list imp_region[Y].ID.
Then, the boundary (i.e., green-colored) nodes will be assigned to the
imp_region with the HIGHER index. So, if X&gt;Y, the green nodes are
assigned to imp_region[X], etc. In cases where a node is shared by
elements that are assigned to multiple imp_regions, the highest
imp_region[] index always "wins". The value of the element tags
themselves are NOT taken into account when assinging boundary nodes to
imp_regions.</p>
<p>Note that this assignment process can be queried directly by loading
the output file <code>imp_region.dat</code> in meshalyzer, which
provides the imp_region[] index for each node in the mesh. To illustrate
this point, consider the following .par file section as an alternative
to the one shown above:</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="co">#############################################################</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a><span class="ex">num_imp_regions</span>          = 4</span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a><span class="co">#############################################################</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.im</span>         = MahajanShiferaw</span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.num_IDs</span>    = 1</span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.ID[0]</span>      = 3</span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">0</span><span class="op">]</span><span class="ex">.im_param</span>   = <span class="st">&quot;GNa*8&quot;</span></span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-9"><a href="#cb5-9" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.im</span>         = MahajanShiferaw</span>
<span id="cb5-10"><a href="#cb5-10" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.num_IDs</span>    = 1</span>
<span id="cb5-11"><a href="#cb5-11" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.ID[0]</span>      = 2</span>
<span id="cb5-12"><a href="#cb5-12" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">1</span><span class="op">]</span><span class="ex">.im_param</span>   = <span class="st">&quot;GNa*4&quot;</span></span>
<span id="cb5-13"><a href="#cb5-13" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-14"><a href="#cb5-14" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.im</span>         = MahajanShiferaw</span>
<span id="cb5-15"><a href="#cb5-15" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.num_IDs</span>    = 1</span>
<span id="cb5-16"><a href="#cb5-16" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.ID[0]</span>      = 1</span>
<span id="cb5-17"><a href="#cb5-17" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">2</span><span class="op">]</span><span class="ex">.im_param</span>   = <span class="st">&quot;GNa*2&quot;</span></span>
<span id="cb5-18"><a href="#cb5-18" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-19"><a href="#cb5-19" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.im</span>         = MahajanShiferaw</span>
<span id="cb5-20"><a href="#cb5-20" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.num_IDs</span>    = 1</span>
<span id="cb5-21"><a href="#cb5-21" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.ID[0]</span>      = 0</span>
<span id="cb5-22"><a href="#cb5-22" aria-hidden="true" tabindex="-1"></a><span class="va">imp_region</span><span class="op">[</span><span class="dv">3</span><span class="op">]</span><span class="ex">.im_param</span>   = <span class="st">&quot;GNa*1&quot;</span></span></code></pre></div>
<p>In terms of tags and properties, the same elements are assigned to
groups with the same properties. The bottom stripe (i.e., tag = 0) is a
region with gNa*1 but instead of being in imp_region[0] it is in
imp_region[3]. As a result, all nodes along the boundary betweeen
elements TAGGED 0 and 1 (i.e., the top edge of the bottom-most stripe)
will be assigned to the imp_region[] with gNa*1 (i.e., ID #3). As
illustrated here, similar differences will exist along the boundaries
between other tagged element regions, due exclusively to the change in
parameter ordering:</p>
<figure>
<img
src="05c_cellular_dynamics_heterogeneity/02_05C_Conductive_Heterogeneity_Fig7_ImpRegionOrderingMatters.png"
class="align-center"
alt="Example illustrating consequences of differences in imp_region[] ordering." />
<figcaption aria-hidden="true">Example illustrating consequences of
differences in imp_region[] ordering.</figcaption>
</figure>
<p>It is important to understand this behavior of the simulator, since
it can have major effects on the overall behaviour of simulations,
especially when there are multiple tissue types with dramatically
different electrophysiological properties distributed in a chaotic
pattern (e.g., interdigitated regions of normal tissue, peri-infarct
zone with remodeled EP, and scar with passive electrical
properties).</p>
<p>In other words, cave seduxit astutia.</p>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>