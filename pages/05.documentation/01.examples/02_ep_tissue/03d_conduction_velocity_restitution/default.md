---
description: This example demonstrates how to compute conduction velocity restitution
  in cardiac tissue
image: ''
title: CV restitution
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Computing conduction velocity restitution</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/03D_conduction_velocity_restitution/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Jason Bayer <jason.bayer@ihu-liryc.fr></i>
<div id="tutorial_conduction-velocity-restitution">
<p>This example demonstrates how to compute conduction velocity
restitution in cardiac tissue. To run the experiments of this tutorial
do</p>
</div>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03D_conduction_velocity_restitution</span></code></pre></div>
<h2 id="introduction">Introduction</h2>
<p>Conduction velocity restitution is an important property of cardiac
tissue. As pacing frequency is increased, conduction velocity will
become slower. For this tutorial, the user will shown how to construct
conduction velocity restitution curves to describe the conduction
properties of cardiac tissue in response to various pacing
protocols.</p>
<h2 id="problem-setup">Problem setup</h2>
<h2 id="d-cable-model">1D cable model</h2>
<p>A 1.0 cm cable of epicardial ventricular myocytes is used to generate
a CV restitution curve for a user defined pacing protocol. The model
domain was discretized with linear finite elements with an average edge
length of 0.01 cm.</p>
<h2 id="ionic-model">Ionic model</h2>
<p>This tutorial uses the most recent version of the ten Tusscher ionic
model for human ventricular myocytes <a href="">[Tusscher2006]</a> .
This ionic model is labeled tenTusscherPanfilov in openCARP's LIMPET
library.</p>
<h2 id="pacing-protocol">Pacing protocol</h2>
<p>The left side of the 1D cable model is paced with 5-ms-long stimuli
at twice capture amplitude for an S1S2 restitution pacing protocol
defined by the user inputs.The user sets the cycle length and number of
beats for S1 pacing, and the range of cycle lengths to apply for the
S2.</p>
<h2 id="conduction-velocity">Conduction velocity</h2>
<p>Activation times are computed for each S2 beat of the pacing protocol
using the openCARP option LATs (see tutorial X). CV is then computed
along the cable by taking the difference in activation times at the
locations 0.25 cm and 0.75 cm divided by the distance between the two
points.</p>
<p>To run the experiments of this tutorial do</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03D_conduction_velocity_restitution</span></code></pre></div>
<h2 id="usage">Usage</h2>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span> </span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--Gil</span>               Default: 0.3650 S/m</span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Intracellular</span> longitudinal tissue conductivity</span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--Gel</span>               Default: 1.3111 S/m</span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Extracellular</span> longitudinal tissue conductivity</span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--nbeats</span>            Default: 5</span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Number</span> of beats for S1 pacing at CI1</span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--CI0</span>               Default: 300 ms</span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Shortest</span> S2 coupling interval</span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--CI1</span>               Default: 500 ms</span>
<span id="cb3-11"><a href="#cb3-11" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">S1</span> cycle length and longest S2 coupling interval</span>
<span id="cb3-12"><a href="#cb3-12" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--CIinc</span>             Default: 25 ms</span>
<span id="cb3-13"><a href="#cb3-13" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Decrement</span> for time interval from CI1 to CI0</span></code></pre></div>
<p>After running run.py, the CI and CV are output into the ASCII file
CVrestitution_tenTusscherPanfilov_bcl_.....</p>
<p>If the program is run with the <code>--visualize</code> option, the
CV restitution curve in the file above will be plotted using pythons
plotting functions.</p>
<h2 id="tasks">Tasks</h2>
<ol type="1">
<li>Determine the minimum S2 CI for the default parameters</li>
<li>Determine the effect of decreasing Gil by 75% on the minimum S2
CI.</li>
<li>Determine the effect of increasing Gil by 50% on the minimum S2
CI.</li>
</ol>
<h2 id="solutions-to-the-tasks">Solutions to the tasks</h2>
<ol type="1">
<li>The minimum CI for the default parameters is 325 ms.</li>
</ol>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--CI0</span> 300 <span class="at">--visualize</span> <span class="at">--np</span> 2</span></code></pre></div>
<ol start="2" type="1">
<li>Reducing Gil by 75% slows down CV and shifts the minimum S2 CI to
300 ms.</li>
</ol>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--CI0</span> 275 <span class="at">--Gil</span> 0.09125 <span class="at">--visualize</span> <span class="at">--np</span> 2</span></code></pre></div>
<ol start="3" type="1">
<li>Increasing Gil by 50% speeds up CV and shifts the minimum S2 CI to
375 ms.</li>
</ol>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--CI0</span> 275 <span class="at">--Gil</span> 0.5475 <span class="at">--visualize</span> <span class="at">--np</span> 2</span></code></pre></div>
<p><strong>References</strong></p>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>