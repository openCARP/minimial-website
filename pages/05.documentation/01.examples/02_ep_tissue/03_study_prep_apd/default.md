---
description: This example demonstrates how to adjust ionic model parameters to generate
  a specific action potential duration in your simulations
image: ''
title: APD adjustment
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Adjusting action potential duration</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/03_study_prep_APD/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Jason Bayer <jason.bayer@ihu-liryc.fr></i>
<div id="tutorial_adjusting_APD">
<p>This tutorial demonstrates how to adjust ionic model parameters to
generate a specific action potential duration in your simulations. To
run the experiments of this tutorial change directories as follows:</p>
</div>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03_study_prep_APD </span></code></pre></div>
<h2 id="introduction">Introduction</h2>
<p>In mammalian hearts, cardiac repolarization varies between species,
is spatially heterogeneous, and altered during disease. Thus, it is
important to calibrate your ionic models to account for these
repolarization differences. The easiest way to do this is to modify the
maximum conductance for ionic currents known to alter action potential
duration (APD), i.e. the time duration a cardiac myocyte repolarizes
after excitation. Ideally, these values are obtained from experiments in
the literature. In this exercise, you will learn how to look up the
ionic current variables in your model that you can change to alter APD,
and how to adjust in cable/tissue models.</p>
<h2 id="problem-setup">Problem setup</h2>
<p>To execute simulations of this tutorial do</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/03_study_prep_APD </span></code></pre></div>
<h2 id="ionic-model">Ionic model</h2>
<p>For this example, we use the most recent version of the ten Tusscher
ionic model for human ventricular myocytes<a href="#fn1"
class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a>.
This ionic model is labeled tenTusscherPanfilov in openCARP's LIMPET
library.</p>
<h2 id="d-cable-model">1D cable model</h2>
<p>For this exercise we compute APD in a 1.5 cm cable of epicardial
ventricular myocytes. The model domain was discretized with linear
finite elements with an average edge length of 0.02 cm.</p>
<h2 id="pacing-protocol">Pacing protocol</h2>
<p>The left side of the 1D cable model is paced with 5-ms-long stimuli
at twice capture amplitude for a basic cycle length and number of beats
chosen by the user.</p>
<h2 id="action-potential-duration">Action potential duration</h2>
<p>Activation potential duration is computed at 80% repolarization
(<span class="math inline">\(APD_{80}\)</span>) according to <a
href="">[Bayer2016]</a>. This is achieved by using the igbutils function
igbapd as illustrated below. See the igbutils tutorial for more
information on using igbapd.</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./igbapd</span> <span class="at">-t</span> nbeats-1<span class="pp">*</span>bcl <span class="at">--repol</span><span class="op">=</span>80 <span class="at">--vup</span><span class="op">=</span>-10 <span class="at">--peak-value</span><span class="op">=</span>plateau <span class="at">--plateau-start</span><span class="op">=</span>15 ./vm.igb</span></code></pre></div>
<h2 id="usage">Usage</h2>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span> </span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--Mode</span>              Options: <span class="dt">{default</span><span class="op">,</span><span class="dt">adjust}</span>, Default: default</span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">When</span> in adjust mode, will use im_param options by user</span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--im_param</span>          Is a string for which you can input operations to </span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">perform</span> to specific variables in a chosen ionic model. </span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">For</span> example, <span class="st">&#39;Gmax_1*0.4,Gmax_2=0.4&#39;</span></span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--IMP</span>               Ionic model name from LIMPET library. </span>
<span id="cb4-8"><a href="#cb4-8" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--nbeats</span>            Default: 2</span>
<span id="cb4-9"><a href="#cb4-9" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Number</span> of beats to perform before APD computation. In </span>
<span id="cb4-10"><a href="#cb4-10" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">reality,</span> this should be much larger to reach </span>
<span id="cb4-11"><a href="#cb4-11" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">steady-state.</span> Due to tutorial time constraints, it is </span>
<span id="cb4-12"><a href="#cb4-12" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">very</span> small.</span>
<span id="cb4-13"><a href="#cb4-13" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--bcl</span>               Default: 700</span>
<span id="cb4-14"><a href="#cb4-14" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Basic</span> cycle length of pacing</span></code></pre></div>
<h2 id="tasks">Tasks</h2>
<ol type="1">
<li>List all of the ionic channel conductances in the ten tusscher model
by running the command below</li>
</ol>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">bench</span> <span class="at">--imp-info</span> <span class="at">-I</span> tenTusscherPanfilov</span></code></pre></div>
<ol start="2" type="1">
<li>Determine the baseline APD by running the following command.</li>
</ol>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--Mode</span> default <span class="at">--IMP</span> tenTusscherPanfilov</span></code></pre></div>
<p>Note, you can look at the values for the APD on each node of the
cable by scrolling through the vertex values under the Highlight tab in
meshalyzer.</p>
<ol start="3" type="1">
<li>Shorten APD by &gt;30 milliseconds by adjusting the maximal
conductance for the slow outward potassium current (Iks). What is the
name of the variable from the output in task 1? Did you have to increase
or decrease the value?</li>
<li>This time, prolong APD by &gt;30 altering the plateau of the action
potential by modifying the maximal conductance of the L-type calcium
channel current (ICaL).</li>
</ol>
<p>In the future, you will want to run many more beats than 2. Also, the
process can be automated similar to tuneCV.</p>
<p><strong>References</strong></p>
<section class="footnotes footnotes-end-of-document"
role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>ten Tusscher KHWJ, Panfilov AV.
<strong>Alternans and spiral breakup in a human ventricular tissue
model.</strong> <em>Am J Physiol Heart Circ Physiol</em>,
291(3):H1088-H1100, 2006. <a
href="https://www.ncbi.nlm.nih.gov/pubmed/16565318">[Pubmed]</a><a
href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>