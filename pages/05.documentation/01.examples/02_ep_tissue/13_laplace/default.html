<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Laplace Solver</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/13_laplace/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Anton J Prassl <anton.prassl@medunigraz.at></i>
<h2 id="tutorial-laplace-solver">Introduction</h2>
<p>Computing Laplace-Dirichlet maps provide an elegant tool for
describing the distance between defined boundaries. Specialized software
routines exploiting these maps in order to assign ventricular fibers and
sheets or to determine the set of elements to receive <a
href="/documentation/examples/02_ep_tissue/05b_conductive_heterogeneity">heterogeneous
conductivities</a> are frequently used in the carputils framework. To
run the experiments of this example change directories as follows:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/02_EP_tissue/13_laplace</span></code></pre></div>
<h2 id="experimental-setup">Experimental Setup</h2>
<p>The geometry and the electrodes are defined as follows:</p>
<div id="fig-laplace-solve-setup">
<figure>
<img src="02_13_laplace.png" class="align-center" style="width:50.0%"
alt="An extracellular voltage stimulus (STIM) is applied to the lower boundary of a quadratic tetrahedral FE model with dimensions 10.0mm x 0.1mm x 10.0mm. The ground electrode (GND) is assigned to the left face side of mesh." />
<figcaption aria-hidden="true">An extracellular voltage stimulus (STIM)
is applied to the lower boundary of a quadratic tetrahedral FE model
with dimensions 10.0mm x 0.1mm x 10.0mm. The ground electrode (GND) is
assigned to the left face side of mesh.</figcaption>
</figure>
</div>
<h2 id="problem-specific-opencarp-parameters">Problem-specific openCARP
Parameters</h2>
<p>The relevant part of the .par file for this example is shown
below:</p>
<pre><code>experiment           = 2        # perform Laplace solve only
bidomain             = 1

# ground electrode
stimulus[0].x0       =   -50.   # par-file units are always microns!
stimulus[0].xd       =   100.
stimulus[0].y0       =   -50.
stimulus[0].yd       = 10100.
stimulus[0].z0       =   -50.
stimulus[0].zd       =   200.
stimulus[0].stimtype =     3    # extracellular ground

# stimulus electrode
stimulus[1].x0       =   -50.
stimulus[1].xd       = 10100.
stimulus[1].y0       =   -50.
stimulus[1].yd       =   100.
stimulus[1].z0       =   -50.
stimulus[1].zd       =   200.
stimulus[1].stimtype =     2    # extracellular voltage
stimulus[1].duration =     1.
stimulus[1].strength =     1.

# set isotropic conductivities everywhere
num_gregions         = 1
gregion[0].g_il      = 1
gregion[0].g_it      = 1
gregion[0].g_el      = 1
gregion[0].g_et      = 1</code></pre>
<h2 id="experiment">Experiment</h2>
<p>To run this experiment, do</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--visualize</span></span></code></pre></div>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<ul>
<li>Set uniform conductivities everywhere to not disturb the Laplace
solution</li>
<li>Exclude any geometry parts (e.g. bath) you do not need the Laplace
solution for. <code class="interpreted-text"
role="ref">meshtool&lt;mesh-data-extraction&gt;</code> will help you to
reintegrate any computed features back into its 'parent'.</li>
<li>openCARP needs the bidomain flag to be set to 1. Otherwise the
program will abort with a hint to change this specific setting.</li>
<li>One may scale the values of the Laplace solution through setting the
stimulus strengths to e.g. 0 (stimulus[0], GND) and 1
(stimulus[1].strength) as shown above.</li>
<li>The example collects stimulation nodes by defining an inclosing
volume (x0,x0+xd,y0,y0+yd,z0,z0+zd). Alternatively, vertices can
directly be addressed by including a <code class="interpreted-text"
role="ref">vertex file&lt;vertex-file&gt;</code>.</li>
</ul>
</div>
<p>Expected result:</p>
<div id="fig-laplace-solve-result">
<figure>
<img src="02_13_laplace_result.png" class="align-center"
style="width:50.0%" />
</figure>
</div>
<h2 id="nodal-boundary-conditions">Nodal Boundary Conditions</h2>
<p>Instead of having to define a different stimulus for each different
Dirichlet value, specific nodes can be assigned different values in one
file. The relevant parameters to change are below:</p>
<pre><code>stimulus[1].stimtype =     2    # extracellular voltage
stimulus[1].vtx_file =     BCs
stimulus[1].vtx_fcn  =     1
stimulus[1].strength =     1.</code></pre>
<p>When <code>stimulus[1].vtx_fcn</code> is non-zero, the
<code>vtx_file</code>, <em>BCs.vtx</em> in this case, has vertex
adjustment format in which the nodal strength is specified along with
the node number. The nodal strengths in the file are then multiplied by
<code>stimulus[1].strength</code>.</p>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
