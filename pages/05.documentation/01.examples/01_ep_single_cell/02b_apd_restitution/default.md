---
description: Action potenital duration (APD) restitution example in single cell. As
  pacing frequency is increased, APD shortens to maintain a one to one stimulus to
  responses
image: 01_02_APD_res_curves.png
title: APD restitution
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Single-cell APD restitution</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/01_EP_single_cell/02B_APD_restitution/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Jason Bayer <jason.bayer@ihu-liryc.fr></i>
<div id="tutorial_single-cell-APD-restitution">
<p>This tutorial demonstrates how to compute action potential duration
(APD) restitution in a single cardiac cell.</p>
</div>
<p>To run the experiments of this tutorial change directories as
follows:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/01_EP_single_cell/02B_APD_restitution</span></code></pre></div>
<h2 id="introduction">Introduction</h2>
<p>APD restitution is an important property of cardiac tissue. As pacing
frequency is increased, APD shortens to maintain a one to one stimulus
to response. For this tutorial, the user will be shown how to construct
APD restitution curves to describe the repolarization properties of
cardiac cells in response to various pacing protocols. For details see
the APD restitution section in the manual.</p>
<h2 id="problem-setup">Problem setup</h2>
<h2 id="single-cell-models">Single-cell models</h2>
<p>This tutorial uses the cardiac cell models in openCARP's LIMPET
library and the single-cell tool <a
href="/documentation/examples/01_EP_single_cell/01_basic_bench">bench</a>.
The single-cell model is a user input. To see a list of all available
models, please type the following command.</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">bench</span> <span class="at">--list-imps</span></span></code></pre></div>
<h2 id="restitution-protocols">Restitution protocols</h2>
<p>Single-cell models are paced with 2-ms-long stimuli at twice capture
amplitude with all restitution protocols. Restitutions protocols are
designed to reveal different features of AP restitution. The
<strong>S1S2 protocol</strong> illustrated in <code
class="interpreted-text"
role="numref">fig-apd-restitutions-S1S2-protocol</code> is used to gauge
the propensity towards developing an arrhythmia. The <strong>dynamic
protocol</strong> rather reflects restitution for slower transitions
between BCLs. For an S1S2 pacing protocol, the user sets the basic cycle
length (BCL) and number of beats, <code>nbeats</code>, for the S1
pacing. Following S1 pacing, a single S2 beat is applied at a range of
coupling intervals (CI) chosen by the user. The basics of the S1S2
protocol are illustrated in <code class="interpreted-text"
role="numref">fig-apd-restitutions-S1S2-protocol</code>.</p>
<div id="fig-apd-restitutions-S1S2-protocol">
<figure>
<img src="02b_apd_restitution/01_02B_restitution_S1S2.png"
class="align-center" style="width:100.0%"
alt="Illustration of the S1-S2 pacing protocol: During an initial pre-pacing period where a number of prebeats stimuli are delivered at the chosen BCL, the action potential is stabilized. Alternatively, a snapshot of an AP stabilized at the given BCL can be used and read in using --initial. After stabilization BCL is kept constant, but prematurity of the CI of the S2 is increased by decrementing the CI from the starting CI (CI1) (chosen in this experiment to be CI = CI1 = BCL) with decrements of CIinc down to the final S2 CI of CI0." />
<figcaption aria-hidden="true">Illustration of the S1-S2 pacing
protocol: During an initial pre-pacing period where a number of
<code>prebeats</code> stimuli are delivered at the chosen
<code>BCL</code>, the action potential is stabilized. Alternatively, a
snapshot of an AP stabilized at the given BCL can be used and read in
using <code>--initial</code>. After stabilization <code>BCL</code> is
kept constant, but prematurity of the CI of the S2 is increased by
decrementing the CI from the starting CI (<code>CI1</code>) (chosen in
this experiment to be CI = <code>CI1</code> = <code>BCL</code>) with
decrements of <code>CIinc</code> down to the final S2 CI of
<code>CI0</code>.</figcaption>
</figure>
</div>
<p>When using a dynamic pacing protocol, the S1 cycle length is
decremented sequentially and continuously after a user chosen number of
beats, <code>nbeats</code> until the minimum CI of <code>CI0</code> is
reached. The protocol is graphically illustrated in <code
class="interpreted-text"
role="numref">fig-apd_restitutions-dynamic-protocol</code>.</p>
<div id="fig-apd_restitutions-dynamic-protocol">
<figure>
<img src="02b_apd_restitution/01_02B_restitution_dynamic.png"
class="align-center" style="width:100.0%"
alt="Illustration of the dynamic restitution protocol: Pacing (again after some prepacing phase or restarting from a stabilized checkpoint for a given BCL) starts with a train of nbeats stimuli at the longest CI of CI1. CI is decremented by CIinc after each pulse train until the minimum CI of CI0 is reached." />
<figcaption aria-hidden="true">Illustration of the dynamic restitution
protocol: Pacing (again after some prepacing phase or restarting from a
stabilized checkpoint for a given BCL) starts with a train of
<code>nbeats</code> stimuli at the longest CI of <code>CI1</code>. CI is
decremented by <code>CIinc</code> after each pulse train until the
minimum CI of <code>CI0</code> is reached.</figcaption>
</figure>
</div>
<h2 id="action-potential-duration">Action potential duration</h2>
<p>Activation potential duration is computed as the time it takes to
reach 90% repolarization (<span class="math inline">\(APD_{90}\)</span>)
from the maximum action potential upstroke potential.</p>
<h2 id="usage">Usage</h2>
<p>The following optional arguments are available (default values are
indicated):</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span> </span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--Protocol</span>          Default: S1S2</span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Restitution</span> protocol, either S1S2 or Dynamic</span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--prebeats</span>          Default: 20</span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Number</span> of pre-pacing beats for S1 pacing at BCL</span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--inital</span>            Initial state vector representing limit cycle for chosen BCL</span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--nbeats</span>            Default: 5</span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Number</span> of beats for S1 pacing at BCL</span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--CI0</span>               Default: 50 ms</span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Shortest</span> S2 coupling interval</span>
<span id="cb3-11"><a href="#cb3-11" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--CI1</span>               Default: BCL</span>
<span id="cb3-12"><a href="#cb3-12" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">S1</span> cycle length and longest S2 coupling interval</span>
<span id="cb3-13"><a href="#cb3-13" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--CIinc</span>             Default: 25 ms</span>
<span id="cb3-14"><a href="#cb3-14" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Decrement</span> for time interval from CI1 to CI0</span>
<span id="cb3-15"><a href="#cb3-15" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--imp</span>               Default: tenTusscherPanfilov</span>
<span id="cb3-16"><a href="#cb3-16" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Single-cell</span> model to use</span>
<span id="cb3-17"><a href="#cb3-17" aria-hidden="true" tabindex="-1"></a>  <span class="ex">--params</span>            Default: <span class="st">&#39; &#39;</span></span>
<span id="cb3-18"><a href="#cb3-18" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">Parameters</span> of single-cell model to modify</span></code></pre></div>
<p>The run.py script formats these user inputs into the following file
for an S1S2 pacing protocol using bench,</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">1</span>             <span class="co"># protocol selection 1=S1S2 0=dynamic</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="ex">prebeats</span>      <span class="co"># number of prepacing beats before starting protocol</span></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a><span class="ex">BCL</span>           <span class="co"># basic cycle length</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a><span class="ex">CI1</span>           <span class="co"># S2 prematurity start</span></span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a><span class="ex">CI0</span>           <span class="co"># S2 prematurity end</span></span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a><span class="ex">nbeats</span>        <span class="co"># number of beats preceding premature one</span></span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a><span class="ex">CIinc</span>         <span class="co"># decrement in S2 prematurity in ms</span></span></code></pre></div>
<p>and the following for a dynamic pacing protocol,</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">0</span>             <span class="co"># protocol selection 1=S1S2 0=dynamic</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a><span class="ex">prebeats</span>      <span class="co"># number of prepacing beats before starting protocol</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a><span class="ex">BCL</span>           <span class="co"># initial basic cycle length</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a><span class="ex">CI0</span>           <span class="co"># final basic cycle length</span></span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a><span class="ex">nbeats</span>        <span class="co"># number of beats per basic cycle length</span></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a><span class="ex">CIinc</span>         <span class="co"># decrement of bcl in ms</span></span></code></pre></div>
<p>The script run.py then feeds these user input scripts to the function
--restitute in bench. The results for DI and APD are output into the
ASCII file restout_APD_restitution.dat in the corresponding output
directory for the given input parameters. The format of this file is
described in the openCARP manual under section the describing the
function --restitute in bench (3.1.2). The details of the file format
are shown below.</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">Col</span> 1 <span class="co">#Beat         : Beat number of an AP</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a><span class="ex">Col</span> 2 Prematurity   : P <span class="er">(</span><span class="ex">premature</span> beat<span class="kw">)</span> <span class="ex">or</span> <span class="pp">*</span> <span class="er">(</span><span class="ex">no</span> prematurity<span class="kw">)</span></span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a><span class="ex">Col</span> 3 Steady State  : O <span class="er">(</span><span class="ex">not</span> in steady state<span class="kw">)</span><span class="ex">,</span> 1 <span class="er">(in</span> <span class="ex">steady</span> state<span class="kw">)</span></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a><span class="ex">Col</span> 4 APD           : Action potential duration</span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true" tabindex="-1"></a><span class="ex">Col</span> 5 DI<span class="er">(</span><span class="ex">n</span><span class="kw">)</span>         <span class="bu">:</span> Diastolic interval of current beat</span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true" tabindex="-1"></a><span class="ex">Col</span> 6 DI<span class="er">(</span><span class="ex">n-1</span><span class="kw">)</span>       <span class="bu">:</span> Diastolic interval of previous beat</span>
<span id="cb6-7"><a href="#cb6-7" aria-hidden="true" tabindex="-1"></a><span class="ex">Col</span> 7 Triangulation : APD90-APD30</span>
<span id="cb6-8"><a href="#cb6-8" aria-hidden="true" tabindex="-1"></a><span class="ex">Col</span> 8 Vm_min        : Minimum potential of current beat</span>
<span id="cb6-9"><a href="#cb6-9" aria-hidden="true" tabindex="-1"></a><span class="ex">Col</span> 9 Vm_max        : Maximum potential of current beat</span></code></pre></div>
<p>If run.py is ran with the <code>--visualize</code> option, the APD
restitution curve (APD Col 4 vs. DI Col 6) in the output file will be
plotted using pythons plotting functions.</p>
<p>Note, if the user input for CI0 is too small, you may need to trim
the restout_APD_restitution.dat files after a loss of capture occurs.
The visualization feature does not remove these values permanently from
the output file.</p>
<h2 id="tasks">Tasks</h2>
<p>To run the experiments of this tutorial change directories as
follows:</p>
<div class="sourceCode" id="cb7"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/01_EP_single_cell/02B_APD_restitution</span></code></pre></div>
<ol type="1">
<li>Compare the restitution curves for two or more different ionic
models. Examples are shown below.</li>
</ol>
<div class="sourceCode" id="cb8"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--imp</span> tenTusscherPanfilov <span class="at">--visualize</span></span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb8-3"><a href="#cb8-3" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--imp</span> Courtemanche <span class="at">--visualize</span></span>
<span id="cb8-4"><a href="#cb8-4" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb8-5"><a href="#cb8-5" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--imp</span> Shannon <span class="at">--visualize</span></span></code></pre></div>
<div id="fig-APD-restitution-tenTuscher">
<figure>
<img src="02b_apd_restitution/01_02_APD_res_curves.png"
class="align-center" style="width:100.0%"
alt="Differences in restitution curves tenTuscher, Courtemanche and Shannon model" />
<figcaption aria-hidden="true">Differences in restitution curves
tenTuscher, Courtemanche and Shannon model</figcaption>
</figure>
</div>
<ol start="2" type="1">
<li>Using Table 2 from <a href="#Tusscher2006"
class="citation">[Tusscher2006]</a>, generate a APD restitution curve
for the parameter settings -params
'G_Kr=0.172,G_Ks=0.441,G_pCa=0.8666,G_pK=0.00219,G_tf=2' and compute the
maximum slope of the restitution curve. It should be well above 1.0 and
look similar to Figure 5 in the Tusscher2006 paper.</li>
</ol>
<div class="sourceCode" id="cb9"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--imp</span> tenTusscherPanfilov <span class="at">--params</span> GKr=0.172,GKs=0.441 <span class="at">--visualize</span></span></code></pre></div>
<ol start="3" type="1">
<li>Do the same as above in 2, but for the parameter settings -params
'GKr=0.134,GKs=0.270'. The maximum slope of the restitution curve should
be well below 1.0, which promotes alternans and arrhythmogenesis.</li>
</ol>
<div class="sourceCode" id="cb10"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--imp</span> tenTusscherPanfilov <span class="at">--params</span> GKr=0.134,GKs=0.270 <span class="at">--visualize</span></span></code></pre></div>
<p>Notes:</p>
<ol type="1">
<li>It could be useful in the future for Gernot to modify the
--restitute option to determine CI0 internally rather than having the
user determine it.</li>
<li>An adaptive decrement for S2 would be useful to better define the
slope of the APD restitution curve. For example, it is small when APD
does not change much to the next decrement, and is increased when APD
does change above a certain threshold.</li>
<li>It would also maybe be useful to print out the Vm for each S1S2
coupling interval to better visualize the restitution behavior. The
run.py currently uses the option --res-trace to output restout.txt, but
the Vm in the file format is not well described in the carpmanual or
bench documentation.</li>
<li>Lastly, you may need to adjust CI0 and CI1 to the bounds at which
the chosen single-cell model was constrained to. The best thing to do is
find an APD restitution protocol in the published manuscript for the
model, then try to reproduce it.</li>
</ol>
<p><strong>References</strong></p>
<div id="citations">
<dl>
<dt><span id="Tusscher2006"
class="citation-label">Tusscher2006</span></dt>
<dd>
<p>ten Tusscher KHWJ, Panfilov AV. <strong>Alternans and spiral breakup
in a human ventricular tissue model.</strong> <em>Am J Physiol Heart
Circ Physiol</em>, 291(3):H1088-H1100, 2006. <a
href="https://www.ncbi.nlm.nih.gov/pubmed/16565318">[Pubmed]</a></p>
</dd>
</dl>
</div>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>