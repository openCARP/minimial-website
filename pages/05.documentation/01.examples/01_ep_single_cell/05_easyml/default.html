<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>EasyML</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/01_EP_single_cell/05_EasyML/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Edward Vigmond <edward.vigmond@u-bordeaux.fr></i>
<h2 id="EasyML">EasyML</h2>
<h2 id="basics">Basics</h2>
<p>EasyML was designed to be compatible with C, and C++, and elementary
Matlab. You should be able to copy paste equations from these languages
and use them more or less as-is in the modeling language. This language
is simply a way to describe and markup equations. This language is NOT
Turing complete. There is no way to make arrays (but see <a
href="#arrays">Arrays</a>), no for loops, no while loops, no functions,
and no flow of control. We maybe could add functions later, but they
aren't implemented currently. There are no #ifdefs</p>
<p>C syntax conventions are followed when appropriate. Operator
precedence follows C. Every statement ends with ';'. C and C++ comments
are allowed.</p>
<h3 id="getting-started">Getting started</h3>
<p>To run the translator on a model, simply run :</p>
<pre><code>limpet_fe modelname.model
This will produce modelname.c and modelname.h.</code></pre>
<p>From there, you can create a dynamic library by running :</p>
<pre><code>make_dynamic_model modelname.c</code></pre>
<p>Upon running successfully, the translator also prints out a list of
equations that use functions. We find that the time it takes to solve an
ionic model is roughly proportional to the number of function
invocations that are called during the main compute routine. limpet_fe
counts these invocations so you don't have to read the generated code to
figure out what expressions will be slowing down the computation. Try
inserting lookup tables to minimize the number of function
invocations.</p>
<h3 id="defining-the-math">Defining the math</h3>
<p>At its core, the language is made up of 3 types of statements:
assignments, declarations, and markup.</p>
<h4 id="assignments">Assignments</h4>
<p>Assignments look like this:</p>
<pre><code>variable = expression;</code></pre>
<p>These types of statements make up the bulk of the statements within
the EasyML language. EasyML differs from C and Matlab in that the order
of its statements don't matter. Variables do not need to be declared
before use, and therefore :</p>
<pre><code>a = 3;
b = a*a;</code></pre>
<p>CellML is a markup language for describing, amongst other things,
ionic models. Many models are published and availmable from the CellML
Model Repository. However, this format can only be processed by a
machine. CARPentry uses its own MarkUp Language called EasyML.</p>
<p>is the same as:</p>
<pre><code>b = a*a;
a = 3;</code></pre>
<p>A dependency tree is built up from the assignments and as a result,
<strong>variables can only be assigned to once</strong>. All variables
and numeric constants are floating point numbers. Right now the parser
only recognizes a subset of the C math functions. If you need to add
another function email us and let us know. The absolute time is
available with the variable <code>t</code>. The variable <code>dt</code>
is defined and given in ms, but its use is discouraged. Use
<code>diff_XXX</code> instead of writing your own update functions if
possible. This makes it easy for you to change the integration method
later.</p>
<h4 id="if-statements">If statements</h4>
<p>Conditional statements can be done in two ways: C ternary operators
or if statements. C ternary operators look like this:</p>
<pre><code>v_abs = (v &gt; 0) ? v : -v;</code></pre>
<p>However, for nested conditionals, entering equations in this format
gets tedious and ugly. Instead, we advise that you use this format:</p>
<pre><code>if (v &gt; 0) {
    v_abs = v;
} else {
    v_abs = -v;
}</code></pre>
<p>This is simply syntaxic sugar for the above ternary form. In general
the full syntax for if statements is if () {...} elif () {...} else
{...} instead of if () {...} else if () {...} else {...}. C floating
point boolean operators are used in as the conditional expressions. The
translator recognizes "and, &amp;&amp;, or, ||".</p>
<p>Note that because the order of operations doesn't matter, there are
some gotchas when using if statements. In particular, these two sections
of code are equivalent:</p>
<pre><code>v_abs = -v;
if (v &gt; 0) {
    v_abs = v;
}</code></pre>
<p>versus :</p>
<pre><code>if (v &gt; 0) {
    v_abs = v;
} else {
    v_abs = -v;
}</code></pre>
<p>To understand what value a variable takes in the presence of multiple
branches, you can read the code top to bottom. This should let you copy
and paste code from your existing models.</p>
<p>However, because of this quirk, you can't do things like this that
you could do in C</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a>A <span class="op">=</span> <span class="dv">50</span><span class="op">*</span>x<span class="op">*</span>x<span class="op">+</span><span class="dv">40</span><span class="op">*</span>x<span class="op">+</span><span class="dv">100</span><span class="op">;</span></span>
<span id="cb10-2"><a href="#cb10-2" aria-hidden="true" tabindex="-1"></a><span class="cf">if</span> <span class="op">(</span>A <span class="op">&gt;</span> <span class="dv">2000</span><span class="op">)</span> <span class="op">{</span></span>
<span id="cb10-3"><a href="#cb10-3" aria-hidden="true" tabindex="-1"></a>    A <span class="op">=</span> <span class="dv">2000</span><span class="op">;</span></span>
<span id="cb10-4"><a href="#cb10-4" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span></code></pre></div>
<p>This does not clamp the variable above 2000 in the way you expect,
because this is equivalent to :</p>
<pre><code>A = (A &gt; 2000) ?  2000 : 50*x*x+40*x+100;</code></pre>
<p>which means A depends on itself, which in turn means that A will have
to be a state variable. This is not what you want. Rewrite this equation
to</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true" tabindex="-1"></a>A_factor <span class="op">=</span> <span class="dv">50</span><span class="op">*</span>x<span class="op">*</span>x<span class="op">+</span><span class="dv">40</span><span class="op">*</span>x<span class="op">+</span><span class="dv">100</span><span class="op">;</span></span>
<span id="cb12-2"><a href="#cb12-2" aria-hidden="true" tabindex="-1"></a>A <span class="op">=</span> A_factor<span class="op">;</span></span>
<span id="cb12-3"><a href="#cb12-3" aria-hidden="true" tabindex="-1"></a><span class="cf">if</span> <span class="op">(</span>A_factor <span class="op">&gt;</span> <span class="dv">2000</span><span class="op">)</span> <span class="op">{</span></span>
<span id="cb12-4"><a href="#cb12-4" aria-hidden="true" tabindex="-1"></a>    A <span class="op">=</span> <span class="dv">2000</span><span class="op">;</span></span>
<span id="cb12-5"><a href="#cb12-5" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span></code></pre></div>
<h4 id="variable-updates">Variable updates</h4>
<p>We also have syntaxic sugar for accumulating variables. The following
two codes are equivalent: CellML is a markup language for describing,
amongst other things, ionic models. Many models are published and
availmable from the CellML Model Repository. However, this format can
only be processed by a machine. CARPentry uses its own MarkUp Language
called EasyML.</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode C"><code class="sourceCode c"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a>Iion <span class="op">=</span> <span class="dv">0</span><span class="op">;</span></span>
<span id="cb13-2"><a href="#cb13-2" aria-hidden="true" tabindex="-1"></a>Iion <span class="op">+=</span> INa<span class="op">;</span></span>
<span id="cb13-3"><a href="#cb13-3" aria-hidden="true" tabindex="-1"></a>Iion <span class="op">+=</span> Ito<span class="op">;</span></span></code></pre></div>
<p>versus</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode C"><code class="sourceCode c"><span id="cb14-1"><a href="#cb14-1" aria-hidden="true" tabindex="-1"></a>Iion <span class="op">=</span> <span class="dv">0</span><span class="op">+</span>INa<span class="op">+</span>Ito<span class="op">;</span></span></code></pre></div>
<h4 id="markup">Markup</h4>
<p>Markup statements change the way the code handles some variables
compared to others. A markup statement takes the form:</p>
<pre><code>.markup_function(optional_arguments);</code></pre>
<p>The markup function is applied to the variable definition or
declaration immediately preceding the markup command. Therefore, :</p>
<pre><code>X; .external();
Y = 3;
Z = 3; .external();</code></pre>
<p>applies the .external markup to X and Z. If a markup follows an if
statement, the markup applies to every variable definition or
declaration enclosed in that if statement.</p>
<p>In order to avoid tedious repetition of markup statements, the group
directive can be used:</p>
<pre><code>group {
    X;
    Z = 3;
} .external();
Y = 3;</code></pre>
<p>group statements can be nested, and like an if statement, a group
statement collects every declared/defined variable it encloses for
markup.</p>
<h3 id="recognized-markup">Recognized Markup</h3>
<p>Right now, here are the markups that the limpet_fe translator
understands / respects :</p>
<table>
<thead>
<tr class="header">
<th>markup</th>
<th>meaning</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>.external([name])</p></td>
<td><p>the variable can be used externally to the model in the LIMPET
code. An optional argument lets you specify the external name if
different from the variable's name.</p></td>
</tr>
<tr class="even">
<td><p>.flag(A,B,...)</p></td>
<td><p>The variable can only take on the values specified in the
argument, and is set with the <code>flags</code> parameter.</p></td>
</tr>
<tr class="odd">
<td>.lb()</td>
<td>clamped lower bound for variable</td>
</tr>
<tr class="even">
<td><p>.lookup(min,max,step)</p></td>
<td><p>Define a lookup table on this variable with lower bound min,
upper bound max, and with step as the discretization of the
table.</p></td>
</tr>
<tr class="odd">
<td><p>.method(integration)</p></td>
<td><p>integration method for equation. Available values are:</p>
<ul>
<li>fe = forward euler. Explicit, fast, but only conditionally stable.
The default.</li>
<li>rk2 = Runge-Kutta method, 2 steps. Explicit, conditionally stable.
Twice as slow as fe.</li>
<li>rk4 = Runge-Kutta method, 4 steps. Explicit, conditionally stable. 4
times as slow as fe.</li>
<li>rush_larsen = Rush Larsen method. Explicit, but unconditionally
stable for diagonal jacobians. This is the preferred method for
gates.</li>
<li>sundnes = The Sundnes method is a second order Rush-Larsen
scheme.</li>
<li>markov_be = A backward euler inspired method (Implicit RK, order 1)
for Markov mdoels</li>
<li>rosenbrock = Rosenbrock (implicit RK, order 2). More general than
markov_be, but requires a slow dense linear solve.</li>
<li>cvode = use the CVODE adaptive integrators</li>
</ul></td>
</tr>
<tr class="even">
<td><p>.nodal()</p></td>
<td><p>parameter varies on a node by node basis. If used with .external,
the variable needs to be read/written to using the GlobalData_t arrays.
If not used with .external, the parameter becomes a state variable even
if the compute function never changes its value. If you want to
initialize a state variable with external data, do so by an adjustment
on the state variable itself rather than on the _init value.</p></td>
</tr>
<tr class="odd">
<td><p>.param()</p></td>
<td><p>Marks a variable as being a parameter that the user should be
able to adjust from the command line.</p></td>
</tr>
<tr class="even">
<td><p>.regional()</p></td>
<td><p>variable is constant on a per-region basis, and if used with
.external, means the variable can be found in the cgeom
structure.</p></td>
</tr>
<tr class="odd">
<td><p>.store()</p></td>
<td><p>Manually written update function which ignores the differential
update.</p>
<p>Take this example from RDII_F and how it implements the LRd 2000 Zeng
dynamics for Calcium:</p>
<pre><code>dCa_i = -((i_Cai*acap)/(vmyo*zCa*F) +i_up *vnsr/vmyo -irelcicr *vjsr/vmyo-
         ireljsrol*vjsr/vmyo)*dt;//differential equation is ignored
Catotal = trpn+cmdn+dCa_i+Ca_i;
bmyo    = cmdnbar+trpnbar+kmtrpn+kmcmdn-catotal;
cmyo    = kmcmdn*kmtrpn-catotal*(kmtrpn+kmcmdn)+trpnbar*kmcmdn+cmdnbar*kmtrpn;
dmyo    = -kmtrpn*kmcmdn*catotal;
fmyo = sqrt(bmyo*bmyo-3*cmyo);

Ca_i = ((2*fmyo/3)*cos(acos((9*bmyo*cmyo-2*bmyo*bmyo*bmyo-27*dmyo)/
       (2*pow((bmyo*bmyo-3*cmyo),1.5)))/3)-(bmyo/3));.store();//analytical solution</code></pre>
<p>This formulation for Calcium is preferred for LRDII_F because it
avoids extremely stiff differential equations for the Troponin calcium
buffers.</p></td>
</tr>
<tr class="even">
<td><p>.trace()</p></td>
<td><p>Says that a column for this variable should be written when
defining a trace_XXX() function.</p></td>
</tr>
<tr class="odd">
<td>.ub()</td>
<td>clamped upper bound for variable</td>
</tr>
<tr class="even">
<td><p>.units(unit_expression)</p></td>
<td><p>units of variable. Units can be written as arbitrary unit
expressions, and can use all SI prefixes (<em>pnumckMG</em>). Recognized
units are:</p>
<ul>
<li>m - meters</li>
<li>mol - moles</li>
<li>M - molar</li>
<li>L - liters</li>
<li>s - seconds</li>
<li>F - Farads</li>
<li>V - volts</li>
<li>C - coulombs</li>
<li>A - amperes</li>
<li>J - joules</li>
<li>N - newtons</li>
<li>K - kelvins</li>
<li>unitless - use as a placeholder for 1, eg, unitless/cm</li>
</ul></td>
</tr>
</tbody>
</table>
<h3 id="declarations">Declarations</h3>
<p>Declarations look like this:</p>
<pre><code>variable;</code></pre>
<p>These do nothing, but are useful for applying markup.</p>
<h3 id="special-variable-name-rules">Special variable name rules</h3>
<p>Some variables have special meanings. Here are the special meanings
that the translator currently recognizes:</p>
<table>
<thead>
<tr class="header">
<th>variable</th>
<th>meaning</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>diff_XXX</p></td>
<td><p>the differential equation for variable <strong>XXX</strong>. All
derivatives should be with respect to the same time unit. If no units
are specified, then milliseconds is assumed.</p></td>
</tr>
<tr class="even">
<td>d_XXX_dt</td>
<td>Same as <em>diff_XXX</em></td>
</tr>
<tr class="odd">
<td>XXX_init</td>
<td>initial value for <strong>XXX</strong></td>
</tr>
</tbody>
</table>
<h3 id="gate_vars">Gates</h3>
<p>The equations for a Hodgkin-Huxley type gate are best described
by:</p>
<table>
<thead>
<tr class="header">
<th>variable</th>
<th>meaning</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><em>alpha_XXX</em> or <em>a_XXX</em></td>
<td>the opening <span class="math inline">\(\alpha\)</span> rate</td>
</tr>
<tr class="even">
<td><em>b_XXX</em> or <em>beta_XXX</em></td>
<td>the closing <span class="math inline">\(\beta\)</span> rate</td>
</tr>
<tr class="odd">
<td>----------------------</td>
<td>---------------------------</td>
</tr>
<tr class="even">
<td><em>tau_XXX</em></td>
<td>the time constant of change</td>
</tr>
<tr class="odd">
<td><em>XXX_inf</em></td>
<td>the steady state value</td>
</tr>
</tbody>
</table>
<p>In order for a variable to be marked as a gate, then
<strong>XXX</strong> must be used as a variable in another equation
<em>AND</em> one of the pairs (<em>alpha</em>, <em>beta</em>) or
(<em>tau</em>, <em>inf</em>) must be found. When a variable is marked as
a gate, a <strong>diff_XXX</strong> equation will be added for it
automatically. The variable will be solved using Rush Larsen, and an
<strong>XXX_init</strong> variable will be defined if none exists.</p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>Defining 3 or more of these variables is <em>redundant</em> and can
cause problems. Do <strong>NOT</strong> define <em>diff_XXX</em> or
<em>d_XXX</em> as they will automatically be generated</p>
</div>
<h3 id="states">States</h3>
<p>States are like gates but their change is not described by a
differential equation. They may, for instance, discretely change value
based on voltage and time. To define a state variable, make sure to
initialize it as above, and when the value is updated, use the
<code>.store();</code> markup. For example, to use a variable called
<em>activated</em>:</p>
<pre><code>activated_init = 0;
if( activated==0 and V&gt;-20 ){
    new_act = 1;
}elif( activated==1 and V&lt;-20 ) {
    new_act = 2;
} else {
    new_act = 0;
}
activated = new_act;.store();</code></pre>
<h2 id="arrays">Arrays</h2>
<p>Arrayed values may be particularly useful when defining
compartmentalized models with repeated sections. Variable arrays are not
supported in EasyML but preprocessing with <em>arrayifier.py</em>
unrolls array variable expressions to mimic their fuctionality:</p>
<pre><code>arrayifier.py -h
usage: arrayifier.py [-h] [--def-size DEF_SIZE] [--index INDEX] [--process]
                 [--limpet-fe LIMPET_FE]
                 arrayed [unrolled]

fake arrays in limpet_fe

positional arguments:
  arrayed               original model file with array syntax
  unrolled              output model (stdout)

optional arguments:
  -h, --help            show this help message and exit
  --def-size DEF_SIZE   default array size (10)
  --index INDEX         loop index in RHS expr (__INDEX__)
  --process             run limpet_fe on unrolled file
  --limpet-fe LIMPET_FE
                      limpet_fe program (limpet_fe.py)</code></pre>
<p>A new markup function <code>.array()</code> has been added as well as
an array syntax. Let V be a variable. To make it an array of N values
:</p>
<pre><code>V;.array(N);
group {V1;V2;}.array(N);
V;.array();</code></pre>
<p>With no size specified, a default is used which can be changed as an
option (--def-size) to arrayifier.py.</p>
<p>To assign to variable slices, use python slice syntax
<em>V[a:b:c]</em> which is the set of values [a,a+c,a+2*c,...,b-1] A
missing "a" means a=0, and omitting "b" implies N, and negative numbers
are measured from the end Note, an extension is added: a leading
<em>^</em> means take the complement of the slice specified. Eg, given V
has 6 entries V[^1:3] =&gt; V[0],V[3],V[4],V[5] To assign to a
particular index :</p>
<pre><code>V[2] = ...</code></pre>
<p>The RHS may also contain array variables which must match in size the
LHS. All slices must be the same size. <strong>Be careful not to assign
to an entry which has been previously assigned in a slice.</strong> The
following is a 1D diffusion example :</p>
<pre><code>diff_V[1:-1] = k*(V[2:]+V[:-2]-2*V[1:-1])</code></pre>
<div class="warning">
<div class="title">
<p>Warning</p>
</div>
<p>Slice specifications cannot be expressions. eg [a+1:] and [1-1:5] are
<strong>INVALID</strong>.</p>
</div>
<p><code>__INDEX__</code> is recognized as the enumeration of the LHS
set, eg :</p>
<pre><code>V[1:] = 2+__INDEX__;</code></pre>
<p>would lead to :</p>
<pre><code>V[1] = 2;
V[2] = 3;
V[3] = 4;</code></pre>
<p>Another word can be chosen with a commandline option</p>
<p>If the initial predicate of the <em>if</em> statement contains an
array, the whole expression is unrolled including <em>elif</em>'s and
<em>else</em>'s, eg :</p>
<pre><code>if( a[:] &gt; 3 ) { b[:] = 2; } elif( a[:] &lt; 1 ) {b[:] = 2; } else { b[:] = __INDEX__ }</code></pre>
<p>becomes :</p>
<pre><code>if( a[0] &gt; 3 ) { b[0] = 2; } elif( a[0] &lt; 1 ) {b[0] = 2; } else { b[0] = 0 }
if( a[1] &gt; 3 ) { b[1] = 2; } elif( a[1] &lt; 1 ) {b[1] = 2; } else { b[1] = 1 }
etc</code></pre>
<p>If the predicate does not contain an array, arrays are expended as
needed, eg :</p>
<pre><code>if( foo == 1 ) {bar[:] = 3; }</code></pre>
<p>becomes :</p>
<pre><code>if( foo==1 ){ bar[0] = 3; bar[1] = 3; bar[2] = 3; ... }</code></pre>
<p><em>elif</em> predicates can contain arrays if and only if the
initial predicate does. For nested <em>if</em>'s, the outermost arrayed
initial predicate determines the array index of any contained array
expressions.</p>
<p>Derived variables are automatically arrayed if the original variable
is:</p>
<pre><code>diff_V[:] = f(V[:],Ca[:])
alpha_X[:] = ...
b_X[:] = ...
tau_X[:] = ...
X[:]_inf = ...</code></pre>
<p>To initialize variables:</p>
<pre><code>V[:]_init = ...
V[2]_init = ...</code></pre>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
