---
description: Here you learn how to import CellML data into openCARP and what problems
  might occur during this process
image: ''
title: Import CellML
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Converting CellML</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/01_EP_single_cell/10_fromCellML/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Edward Vigmond <edward.vigmond@u-bordeaux.fr></i>
<h2 id="converting-from-cellml">Converting from CellML</h2>
<h2 id="intro">Intro</h2>
<p>CellML is a markup language for describing, amongst other things,
ionic models. Many models are published and available from the <a
href="https://models.cellml.org/electrophysiology">CellML Model
Repository</a>. However, this format can only be processed by a machine.
openCARP uses its own markup language called <a
href="/documentation/examples/01_ep_single_cell/05_easyml">EasyML</a>.</p>
<h2 id="the-script">The Script</h2>
<p>Conversion of a CellML file is performed using the
<code>cellml_converter.py</code> script:</p>
<pre><code>usage: cellml_converter.py [-h] [--Vm VM] [--Istim ISTIM] [--out OUT]
                           [--trace-all] [--plugin] [--params]
                           [--keep-redundant]
                           CMLfile

Convert CellML 1.0 to model format

positional arguments:
  CMLfile           CellML file

optional arguments:
  -h, --help        show this help message and exit
  --Vm VM           transmembrane voltage variable [V]
  --Istim ISTIM     stimulus current variable [i_Stim]
  --out OUT         output file
  --trace-all       trace all variables
  --plugin          is a plug-in, not a whole cell model
  --params          parameterize all constants
  --keep-redundant  keep redundant differential expressions, eg alpha, beta
                    and diff)</code></pre>
<p>The flags mean the following</p>
<table>
<thead>
<tr class="header">
<th>Flag</th>
<th>Meaning</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Vm</td>
<td>the variable name in the CellML file referring to transmembrane
voltage.</td>
</tr>
<tr class="even">
<td>Istim</td>
<td>the CellML variable referring to the stimulus current. This will be
removed.</td>
</tr>
<tr class="odd">
<td>out</td>
<td>output file which will be processed by limpet_fe.py</td>
</tr>
<tr class="even">
<td>trace-all</td>
<td>add all variables to the trace so they can be monitored</td>
</tr>
<tr class="odd">
<td>plugin</td>
<td>not a stand alone ionic model, but a plug-in</td>
</tr>
<tr class="even">
<td>params</td>
<td>make all constants modifiable as parameters</td>
</tr>
<tr class="odd">
<td>keep-redundant</td>
<td>keep all differential equations, even if some are not needed and
redundant</td>
</tr>
</tbody>
</table>
<p>The process of creating an <a
href="/documentation/examples/01_ep_single_cell/05_easyml">EasyML</a>
file is not entirely automated. Manual intervention is always necessary.
Generally, the process to create a model file is as follows:</p>
<ol>
<li><em>cellml_converter.py</em> is run on the CellML file</li>
<li>references to stimuli and extraneous variables are removed</li>
<li>redundant gating variable equations are removed</li>
<li>units are adjusted</li>
<li>integration methods are adjusted</li>
<li>lookup tables are implemented</li>
</ol>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>Variable names may be changed since EasyML demands global uniqueness
while CellML only demands section uniqueness. Thus, in CellML, there may
be two <strong>a</strong> variables, but in two different channels. They
will be renamed to avoid the conflict in EasyML, and variables are
grouped together by section after conversion.</p>
</div>
<h2 id="example-1">Example 1</h2>
<p>We will begin by converting a relatively simple model, the LuoRudy II
model, which is shipped with this example in the file
<code>LuoRudy94.cellml</code>. The python scripts in the following
commands need to be called with their full path if they are not in your
<code>$PATH</code> environment variable. They are shipped with the
openCARP source code and located in the subfolder
<code>physics/limpet/src/python</code>. To compile new ionic models, we
highly recommend to build openCARP from source as described in the
installation instructions on the webpage. You'll need to enable dynamic
model support during the build process to follow this example. In the
following, we'll assume that the openCARP source code is located in
<code>/home/openCARP</code> but this can be easily adjusted according to
your local installation.</p>
<ol>
<li><p>run the converter to produce the EasyML model file:</p>
<pre><code>/home/openCARP/physics/limpet/src/python/cellml_converter.py LuoRudy94.cellml &gt; LuoRudy94.model</code></pre></li>
<li><p>run the model to C translator:</p>
<pre><code>/home/openCARP/physics/limpet/src/python/limpet_fe.py LuoRudy94.model /home/openCARP/physics/limpet/models/imp_list.txt /home/openCARP/physics/limpet/src/imps_src
/home/openCARP/physics/limpet/src/make_dynamic_model.sh LuoRudy94</code></pre>
<p>You will see the following output which has errors:</p>
<pre><code>Syntax error at line 79, token &#39;{&#39;
Warning, you should only use alpha/beta OR tau/infinity while formulating gates!

Using the tau infinity values.

Error with d

Warning, you should only use alpha/beta OR tau/infinity while formulating gates!

Using the tau infinity values.

Error with f

Traceback (most recent call last):
File &quot;/home/openCARP/physics/limpet/src/python/limpet_fe.py&quot;, line 1581, in &lt;module&gt;
    obj.printSource()
 File &quot;/home/openCARP/physics/limpet/src/python/limpet_fe.py&quot;, line 786, in printSource
    declare_type = &quot;double &quot;,
File &quot;/home/openCARP/physics/limpet/src/python/limpet_fe.py&quot;, line 201, in printEquations
    for var in eqnmap.getList(target, valid):
File &quot;/home/openCARP/physics/limpet/src/python/im.py&quot;, line 220, in getList
 for d in self[sym].depend:
File &quot;/home/openCARP/physics/limpet/src/python/basic.py&quot;, line 87, in __getitem__
    return self._map[key]
KeyError: Var(&quot;I_st&quot;)</code></pre></li>
<li><p>Open up <em>LuoRudy94.model</em> in the text editor of your
choice. Based on the error output, look at line <strong>79</strong>:</p>
<pre><code>dV_dt = ((I_st - (i_Na+i_Ca_L+i_K+i_K1+i_Kp+i_NaCa+i_p_Ca+i_Na_b+i_Ca_b+i_NaK+i_ns_Ca))/Cm); .units(mV/ms);
I_st = ((({http://www.w3.org/1998/Math/MathML}rem:time,stimPeriod)&lt;stimDuration) ? stimCurrent : 0.0); .units(uA/mm^2);</code></pre>
<p>It appears that <em>I_st</em> is the stimulus current which we would
like to remove. We can manually remove it by deleting line 79 and
eliminating it from line 78, or we can run
<code>/home/openCARP/physics/limpet/src/python/cellml_converter.py --Istim=I_st LuoRudy94.cellml &gt; LuoRudy94.model</code></p>
<p>Now we are left with errors with gating variables <em>d</em> and
<em>f</em>. Locate them around line 106 in the
<code>LuoRudy94.model</code> file. Note how alpha, beta, tau and _inf
are defined for all. By inspecting, we see that <em>alpha_d</em> and
<em>alpha_f</em> are defined using the infinity and tau values so the
alphas and betas can be removed. Save the file, and rerun
<code>/home/openCARP/physics/limpet/src/python/limpet_fe.py LuoRudy94.model</code>
which should now have no errors.</p></li>
</ol>
<p># make the linkable library and run it to be sure:</p>
<pre><code>/home/openCARP/physics/limpet/src/make_dynamic_model.sh LuoRudy94
bench --load ./LuoRudy94.so</code></pre>
<h2 id="example-2">Example 2</h2>
<p>Now, we will compile the Grandi (a.k.a. Grandi-Pasqualini-Bers) model
(<code>Grandi.cellml</code>), which is quite complicated.</p>
<ol>
<li><p>Run the EasyML generator:</p>
<pre><code>/home/openCARP/physics/limpet/src/python/cellml_converter.py Grandi.cellml &gt; Grandi.model
WARNING: stimulus current not found
WARNING: transmembrane voltage not found</code></pre>
<p>So, not ev ery author uses the same names for the same quantities.
Inspecting the model file, we can see that transmembrane voltage is
called <em>V_m</em> and the stimulating current is <em>I_app</em>. Rerun
the converter:</p>
<pre><code>/home/openCARP/physics/limpet/src/python/cellml_converter.py --Vm=V_m --Istim=I_app  Grandi.cellml &gt; Grandi.model</code></pre>
<p>Generate the C code:</p>
<pre><code>/home/openCARP/physics/limpet/src/python/limpet_fe.py Grandi.model</code></pre>
<p>which does not complain but when we try to run the model :</p>
<pre><code>bench --load ./Grandi.so</code></pre>
<p>we get NaNs which is not good. So, at this point we need to rely on
intuition and a good debugger. First we reduce the time step and hope it
is just an integration issue with stiff equations:</p>
<pre><code>bench --load ./Grandi.so --dt=0.005</code></pre>
<p>which luckily works. We do not want to be restricted to a 5
microsecond timestep so we need to investigate further. The most common
issues are</p>
<ol>
<li>calcium is released explosively during the upstroke so we often need
to proper integrate calcium related variables as a group with a more
complicated method.</li>
<li>gating variables are not defined with alpha/beta or tau/_inf so they
can go out of bounds, especially <em>m</em> of the sodium channel</li>
<li>Markov submodels may also be stiff systems so need the
<em>markov_be</em> integration method.</li>
</ol>
<p>In this particular case, the sodium channel, <em>I_Na</em> on line
170, is definitely a culprit. After inspection, we see <em>diff_</em>
defined as well as redundant alpha/beta/tau/_inf. This is what I finally
changed it to:</p>
<pre><code>#I_Na
I_Na = (I_Na_junc+I_Na_sl); .units(uA/uF);
I_Na_junc = (Fjunc*GNa*(m*m*m)*h*j*(V_m - ena_junc)); .units(uA/uF);
I_Na_sl = (Fsl*GNa*(m*m*m)*h*j*(V_m - ena_sl)); .units(uA/uF);
ah = ((V_m&gt;=-40.) ? 0. : (0.057*exp((-(V_m+80.)/6.8)))); .units(unitless);
tau_h = (1./(ah+bh)); .units(ms);
bh = ((V_m&gt;=-40.) ? (0.77/(0.13*(1.+exp((-(V_m+10.66)/11.1))))) : ((2.7*exp((0.079*V_m)))+(3.1e5*exp((0.3485*V_m))))); .units(unitless);
h_inf = (1./((1.+exp(((V_m+71.55)/7.43)))*(1.+exp(((V_m+71.55)/7.43))))); .units(unitless);
tau_m = ((0.1292*exp(-(((V_m+45.79)/15.54)*((V_m+45.79)/15.54))))+(0.06487*exp(-(((V_m - 4.823)/51.12)*((V_m - 4.823)/51.12))))); .units(ms);
m_inf = (1./((1.+exp((-(56.86+V_m)/9.03)))*(1.+exp((-(56.86+V_m)/9.03))))); .units(unitless);
bj = ((V_m&gt;=-40.) ? ((0.6*exp((0.057*V_m)))/(1.+exp((-0.1*(V_m+32.))))) : ((0.02424*exp((-0.01052*V_m)))/(1.+exp((-0.1378*(V_m+40.14)))))); .units(unitless);
aj = ((V_m&gt;=-40.) ? 0. : ((((-2.5428e4*exp((0.2444*V_m))) - (6.948e-6*exp((-0.04391*V_m))))*(V_m+37.78))/(1.+exp((0.311*(V_m+79.23)))))); .units(unitless);
tau_j = (1./(aj+bj)); .units(ms);
j_inf = (1./((1.+exp(((V_m+71.55)/7.43)))*(1.+exp(((V_m+71.55)/7.43))))); .units(unitless);</code></pre>
<p>We try to run it again and we still have an issue. As mentioned
above, Markov chains and calcium handling can be numercially stiff. We
will try more expensive integration methods at the cost of some speed. I
added the following code to <em>Grandi.model</em> :</p>
<pre><code>group {
    Ca_i;
    Ca_j;
    Ca_sl;
    Ca_sr;
    f_Ca_Bj;
    f_Ca_Bsl;
}.method(cvode);
group {
    Ry_Ri;
    Ry_Ro;
    Ry_Rr;
}.method(markov_be);</code></pre>
<p>This time, we can compile and run it. However, we are not done. Let's
try to improve performance by using a lookup table for the voltage
functions. We change line 2 of the model file to be :</p>
<pre><code>V_m; .external(Vm); .nodal();.lookup(-100,100,0.05);</code></pre>
<p>We generate the C file, make the linkable library and when we try to
run it:</p>
<pre><code>bench --load ./Grandi.so --stim-start=100 --dt=0.005 -Oa

L2 : LUT WARNING: Grandi V_m=-5 produces -nan in entry number 1!

L2 : LUT WARNING: Grandi V_m=-5 produces -nan in entry number 2!

Outputting the following quantities at each time:
      Time              Vm            Iion


[CVODE ERROR]  CVode
  At t = 283.5 and h = 1.90735e-08, the corrector convergence test failed repeatedly or with |h| = hmin.

bench: /home/openCARP/physics/limpet/src/imps_src/Grandi.cc:868: Grandi: Assertion `CVODE_flag == 0&#39; failed.</code></pre>
<p>Hmmm. Maybe the NaN in the lookup table triggers the error. Sure
enough, we see that entry for the d-gate time constant of I_Ca,
<em>tau_d</em>, has a singularity at -5 mV and that is where the
simulation failed. We need to use L'Hopital's rule for that voltage:</p>
<pre><code>tau_d = (V_m==-5.)?(-d_inf/6./0.035):((1.*d_inf*(1. - exp((-(V_m+5.)/6.))))/(0.035*(V_m+5.))); .units(ms);
d_inf = (1./(1.+exp((-(V_m+5.)/6.)))); .units(unitless);</code></pre>
<p>This <code class="interpreted-text"
role="download">model file &lt;/downloads/Grandi.good.model&gt;</code>
now works!!!! For further practice, determine which subset of Ca
variables can be removed from the group and/or if another integration
method can be used.</p>
<p>Also don't forget to add metadata to your newly generated .model
file. See the <a
href="https://git.opencarp.org/openCARP/openCARP/-/tree/master/physics/limpet/models">shipped
.model files</a>. for examples.</p></li>
</ol>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>