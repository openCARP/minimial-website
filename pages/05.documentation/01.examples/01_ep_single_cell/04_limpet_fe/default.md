---
description: This tutorial explains the basics of using the code generation tool limpet_fe.py
  for generating ODE solver code for models of cellular dynamics.
image: ''
title: EasyML to C code
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Basic usage of code generation tool limpet_fe.py</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/01_EP_single_cell/04_limpet_fe/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Edward Vigmond <edward.vigmond@ubordeaux-1.fr></i>
<p>This tutorial explains the basics of using the code generation tool
<code>limpet_fe.py</code> for generating ODE solver code for models of
cellular dynamics. The code needs to be compiled, thus you to have a
working developer environment in place (PETSc and other required
libraries). The best way to make sure this is the case and to avoid
compatibility issues is to compile openCARP from source as <a
href="https://opencarp.org/download/installation#building-from-source">outlined
here</a>.</p>
<p>Make sure to enable dynamic library support when building openCARP.
If you use CMake, this is done by passing <code>-DDLOPEN=ON</code> when
calling CMake. If you use the makefile, set <code>DLOPEN=1</code> in
your <code>my_switches.def</code> file.</p>
<h2 id="limpet-fe-tutorial">Usage of limpet_fe.py</h2>
<p><code>limpet_fe.py</code> is the <a
href="/documentation/examples/01_ep_single_cell/05_easyml">EasyML</a> to
C translator for writing ionic models. The easiest way to see how to
write a model is to examine a simpler example. To work on this tutorial
do</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/01_EP_single_cell/04_limpet_fe</span></code></pre></div>
<h2 id="input-.model-file">Input .model file</h2>
<p>Let's look at the Beeler-Reuter model (in your current folder:
my_MBRDR.model) as modified by Drouhard and Roberge:</p>
<pre><code>Iion; .nodal(); .external();
V; .nodal(); .external(Vm); 

V; .lookup(-800, 800, 0.05);
Ca_i; .lookup(0.001, 30, 0.001); .units(uM);

V_init = -86.926861;

# sodium current
GNa = 15;
ENa  = 40.0;
I_Na = GNa*m*m*m*h*(V-ENa);
I_Na  *= sv-&gt;j;

a_m = ((V &lt; 100)
       ? 0.9*(V+42.65)/(1.-exp(-0.22*(V+42.65)))
       : 890.94379*exp(.0486479163*(V-100.))/
            (1.+5.93962526*exp(.0486479163*(V-100.)))
       );
b_m = ((V &lt; -85)
       ? 1.437*exp(-.085*(V+39.75))
       : 100./(1.+.48640816*exp(.2597503577*(V+85.)))
       );
a_h = ((V&gt;-90.)
       ? 0.1*exp(-.193*(V+79.65))
       : .737097507-.1422598189*(V+90.)
       );
b_h = 1.7/(1.+exp(-.095*(V+20.5)));

a_d = APDshorten*(0.095*exp(-0.01*(V-5.)))/
  (exp(-0.072*(V-5.))+1.);
b_d = APDshorten*(0.07*exp(-0.017*(V+44.)))/
  (exp(0.05*(V+44.))+1.) ;

a_f = APDshorten*(0.012*exp(-0.008*(V+28.)))/
  (exp(0.15*(V+28.))+1.);
b_f = APDshorten*(0.0065*exp(-0.02*(V+30.)))/
  (exp(-0.2*(V+30.))+1.);

a_X = ((V&lt;400.)
       ? (0.0005*exp(0.083*(V+50.)))/
       (exp(0.057*(V+50.))+1.) 
       : 151.7994692*exp(.06546786198*(V-400.))/
       (1.+1.517994692*exp(.06546786198*(V-400.)))
       );

b_X   = (0.0013*exp(-0.06*(V+20.)))/(exp(-0.04*(V+20.))+1.);

xti   = 0.8*(exp(0.04*(V+77.))-1.)/exp(0.04*(V+35.));

I_K = (( V != -23. )
       ? 0.35*(4.*(exp(0.04*(V+85.))-1.)/(exp(0.08*(V+53.))+
                                              exp(0.04*(V+53.)))-
               0.2*(V+23.)/expm1(-0.04*(V+23.)))
       : 
       0.35*(4.*(exp(0.04*(V+85.))-1.)/(exp(0.08*(V+53.))+
                                            exp(0.04*(V+53.))) + 0.2/0.04 )
       );

# slow inward
Gsi = 0.09;
Esi = -82.3-13.0287*log(Ca_i/1.e6);
I_si = Gsi*d*f*(V-Esi);
I_X  = X*xti;

Iion= I_Na+I_si+I_X+I_K;

Ca_i_init = 3.e-1;
diff_Ca_i = ((V&lt;200.)
             ? (-1.e-1*I_si+0.07*1.e6*(1.e-7-Ca_i/1.e6))
             : 0
             );

group {
  GNa;
  Gsi;
  APDshorten = 1;
} .param();

group {
  I_Na;
  I_si;
  I_X;
  I_K;
} .trace();</code></pre>
<p>Generally, it looks like C code with a few extra commands. Let's
break it down a bit:</p>
<pre><code>Iion; .nodal(); .external();
V; .nodal(); .external(Vm); </code></pre>
<p><strong>These lines are very important.</strong>
<code>.external()</code> tells us that these are global variables and
through these variables we will interact with the simulation. The
argument to <em>external()</em> is the actual name of the global
variable if we want to call it something else locally. Here, the local
variable <code>V</code> is actually the global variable <code>Vm</code>
while <code>Iion</code> is the global name which we use locally. To
alter the transmembrane voltage, <code>Iion</code> must be assigned a
value. openCARP then uses this value of <em>Iion</em> to adjust the
transmembrane voltage. <strong>You do not specify the change of
*Vm*.</strong></p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>Ionic models are fully functioning models. Plugins are additional
components which must be added to an ionic model. They modify or expand
behaviour, eg., a new channel or a force genration description. For an
ionic model, the ionic current is set, e.g., Iion = 2.3, while plug-ins
add an additional current to an existing current, i.e., Iion = Iion +
2.3</p>
</div>
<p>The next two lines :</p>
<pre><code>V; .lookup(-800, 800, 0.05);
Ca_i; .lookup(0.001, 30, 0.001); .units(uM);</code></pre>
<p>tell the translator to use lookup tables for variables which are
functions of these variables in order to avoid expensive function calls.
The table range for <em>V</em> is -100 to 100 in steps of 0.05. This
should be done after the model is verified to be working correctly.</p>
<p>There are several Hodgkin-Huxley type gating variables. Here is the
<em>f</em> gate :</p>
<pre><code>a_f = APDshorten*(0.012*exp(-0.008*(V+28.)))/
  (exp(0.15*(V+28.))+1.);
b_f = APDshorten*(0.0065*exp(-0.02*(V+30.)))/
  (exp(-0.2*(V+30.))+1.);</code></pre>
<p>Variables of the form <em>a_XXX</em> and <em>b_XXX</em> are
automatically recognized as the <span
class="math inline">\(\alpha\)</span> and <span
class="math inline">\(\beta\)</span> coefficients. The differential
equation for the variable <em>XXX</em> will be automatically generated
as well as an intial condition. Alternatively, one can assign
<em>tau_XXX</em> and <em>XXX_inf</em>, which correspond to <span
class="math inline">\(\tau_{XXX}\)</span> and <span
class="math inline">\(XXX_{\inf}\)</span>.</p>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>if a variable is a differential variable but is <strong>NOT</strong>
a recognized <em>gating</em> variable, its initial condition init_XXX
must be explicitly set. That is why you will find <em>Ca_i_init</em> in
the .model file.</p>
</div>
<p>The block:</p>
<pre><code>group {
    GNa;
    Gsi;
    APDshorten = 1;
  } .param();</code></pre>
<p>declares variables with default values. However, their values can be
changed at the start of a simulation with the parameter changing
syntax.</p>
<p>The section:</p>
<pre><code>group {
  I_Na;
  I_si;
  I_X;
  I_K;
} .trace();</code></pre>
<p>declares a group of trace variables. If a node is selected as being
traced, these quantities will be output as functions of time at the
node.</p>
<h2 id="units-easyml">Units</h2>
<p>Units are a never ending source of grief. All sorts are used in
various papers. openCARP expects certain standards so that interactions
are possible between components. The table below gives the recommended
units</p>
<table>
<thead>
<tr class="header">
<th>quantity</th>
<th>units</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>membrane current</td>
<td><span class="math inline">\(\mu A/cm^2 = pA/pF\)</span></td>
</tr>
<tr class="even">
<td>dX/dt</td>
<td><span class="math inline">\(ms^{-1}\)</span></td>
</tr>
<tr class="odd">
<td><span class="math inline">\([Ca]_i\)</span></td>
<td><span class="math inline">\(\mu M\)</span></td>
</tr>
<tr class="even">
<td><span class="math inline">\([X]\)</span></td>
<td><span class="math inline">\(mM\)</span></td>
</tr>
<tr class="odd">
<td>voltage/potential</td>
<td><span class="math inline">\(mV\)</span></td>
</tr>
<tr class="even">
<td>membrane conductance</td>
<td><span class="math inline">\(mS/cm^2\)</span></td>
</tr>
<tr class="odd">
<td><span class="math inline">\(\tau_X\)</span> :</td>
<td>math:<span class="title-ref">ms</span></td>
</tr>
<tr class="even">
<td><span class="math inline">\(\alpha,\beta\)</span></td>
<td><span class="math inline">\(ms^{-1}\)</span></td>
</tr>
</tbody>
</table>
<p>Internally, you can use what you like but you need to convert any
variables passed between openCARP and your model, i.e., the total
membrane current, concentrations, derivatives, and variables associated
with gates.</p>
<h2 id="generating-the-c-source">Generating the C source</h2>
<p>Now, we need to make a runtime shared object library:</p>
<pre><code>make_dynamic_model.sh my_MBRDR</code></pre>
<p><code>make_dynamic_model.sh</code> (located in
<code>physics/limpet/src</code>) calls <code>limpet_fe.py</code>
(located in <code>physics/limpet/src/python</code>) to convert the model
file to C++ code for compilation. Provided you have no errors, this will
produce the files, <em>my_MBRDR.cc</em> and <em>my_MBRDR.h</em>.
<code>make_dynamic_model.sh</code> will then call
<code>make_dynamic_model.py</code>, which will produce
<em>my_MBRDR.so</em>. This shared library can then be loaded by openCARP
at runtime and provide an ionic model with the name
<code>my_MBRDR</code>.</p>
<p><code>NOTE:</code> The best way to make ensure a working development
environment including thirdparty dependencies like PETSc and to avoid
compatibility issues between your openCARP binary and the newly compile
model library is to compile openCARP from source as <a
href="https://opencarp.org/download/installation#building-from-source">outlined
here</a>. To create a dynamic model, you need to have compiled openCARP
from its source. The script needs the path of the source code as well as
compiled libraries from openCARP.</p>
<h2 id="using-the-imp">Using the IMP</h2>
<h3 id="single-cell">Single cell</h3>
<p>The program <span class="title-ref">bench</span> is used to run
single cell experiments. See <a
href="/documentation/examples/01_ep_single_cell/01_basic_bench">bench</a>
for more details running this program. In this tutorial, <em>run.py</em>
is just a wrapper for <em>bench</em>:</p>
<pre><code>run.py --load-module ./my_MBRDR.so </code></pre>
<div class="note">
<div class="title">
<p>Note</p>
</div>
<p>for the <em>--load</em> option, an absolute path must be specified.
That is why we used the <em>"./"</em> in <em>"./my_MBRDR.so"</em>. If
the <em>--imp</em> option is not specified, it will attempt to load the
ionic model with the same name as the loaded library.</p>
</div>
<p>Remembering that we set <code>APDshorten</code> as a parameter, we
can see its effect :</p>
<pre><code>run.py --load-module ./my_MBRDR.so --imp-par=&#39;APDshorten=3&#39;</code></pre>
<dl>
<dt>As an exercise, try adding <code>Ca_i</code> as a trace variable, or
add a <em>j</em> gate to the sodium channel described by the follwing
equations</dt>
<dd>
<p><span class="math inline">\(\alpha_j =
0.055\frac{exp(-0.25*(V+78))}{exp(-(V+78)/5)+1}\)</span> and <span
class="math inline">\(\beta_j  =
\frac{0.3}{(exp(-0.1*(V+32))+1}\)</span></p>
</dd>
</dl>
<h3 id="parameter-files">Parameter files</h3>
<p>To use the ionic model in a tissue simulation, it is specified in the
parameter file as :</p>
<pre><code>num_external_imp = 1
external_imp = ./my_MBRDR.so
imp_region[0].im = my_MBRDR</code></pre>
<p>Obviously, multiple IMPs can be added by increasing
<code>num_external_imp</code>. Make sure that the shared library was
built in the same environment (operating system, compiler etc.) that
openCARP was built in.</p>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>