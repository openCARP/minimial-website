---
description: This example introduces the basic steps of running EP simulations in
  an isolated myocytes
image: 01_01_MyocyteEP_Setup.png
title: Basic single cell EP
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Basic usage of single cell tool bench - Limit cycle experiments</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/01_EP_single_cell/01_basic_bench/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Gernot Plank <gernot.plank@medunigraz.at></i>
<div id="single-cell-EP-tutorial">
<p>To run the experiments of this tutorial change directories as
follows:</p>
</div>
<div class="sourceCode" id="cb1"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/01_EP_single_cell/01_basic_bench </span></code></pre></div>
<h2 id="modeling-ep-at-the-cellular-level">Modeling EP at the cellular
level</h2>
<p>This tutorial introduces the basic steps of setting up EP simulations
in an isolated myocytes. A more detailed account on <a
href="/documentation/examples/01_ep_single_cell/01_basic_bench">modeling
EP at the cellular scale</a> is given in the manual. For all experiments
shown in this tutorial we use the single cell EP tool <a
href="/documentation/examples/01_EP_single_cell/01_basic_bench">bench</a>.
Before starting we recommend therefore to look up the basic command line
options in the <a
href="/documentation/examples/01_EP_single_cell/01_basic_bench">bench
section</a> of the manual. In this introductory tutorial we focus on
simple pacing protocols for finding a stable limit cycle. We model an
isolated myocyte which we stimulate using an electrode for intracellular
current injection. The setup is shown in <code class="interpreted-text"
role="numref">fig-single-cell-EP</code>.</p>
<div id="fig-single-cell-EP">
<figure>
<img src="01_basic_bench/01_01_MyocyteEP_Setup.png" class="align-center"
style="width:50.0%"
alt="Setup for single cell EP experiments. The myocyted is activated with a suprathreshold stimulus current to initiate action potentials at a pacing cycle lenght of 500 ms." />
<figcaption aria-hidden="true">Setup for single cell EP experiments. The
myocyted is activated with a suprathreshold stimulus current to initiate
action potentials at a pacing cycle lenght of 500 ms.</figcaption>
</figure>
</div>
<h2 id="limit-cycle-tutorial-exposed-params">Experimental
Parameters</h2>
<p>The following parameters are exposed to steer the experiment:</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">--EP</span> <span class="dt">{tenTusscherPanfilov</span><span class="op">,</span><span class="dt">Grandi}</span>        pick human EP model <span class="er">(</span><span class="ex">default</span> is tenTusscherPanfilov<span class="kw">)</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="ex">--EP-par</span> EP_PAR       provide a parameter modification string <span class="er">(</span><span class="ex">default</span> is <span class="st">&#39;&#39;</span><span class="kw">)</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a><span class="ex">--init</span> INIT           pick state variable initialization file <span class="er">(</span><span class="ex">default</span> is none<span class="kw">)</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a><span class="ex">--duration</span> DURATION   pick duration of experiment <span class="er">(</span><span class="ex">default</span> is 500 ms<span class="kw">)</span></span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true" tabindex="-1"></a><span class="ex">--bcl</span> BCL             pick basic cycle length <span class="er">(</span><span class="ex">default</span> is 500 ms<span class="kw">)</span></span>
<span id="cb2-10"><a href="#cb2-10" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-11"><a href="#cb2-11" aria-hidden="true" tabindex="-1"></a><span class="ex">--visualize</span>           plots the myocyte action potential</span>
<span id="cb2-12"><a href="#cb2-12" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-13"><a href="#cb2-13" aria-hidden="true" tabindex="-1"></a><span class="ex">--overlay</span>             overlays the action potential or specified variables of several</span>
<span id="cb2-14"><a href="#cb2-14" aria-hidden="true" tabindex="-1"></a>                      <span class="ex">experiments</span> in the same plot</span>
<span id="cb2-15"><a href="#cb2-15" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-16"><a href="#cb2-16" aria-hidden="true" tabindex="-1"></a><span class="ex">--vis_var</span> VIS_VAR     specified the variables that you want to visualize <span class="er">(</span><span class="ex">default</span> is V<span class="kw">)</span></span></code></pre></div>
<p>Each experiment stores the state of myocyte and myofilament in the
current directory in a file to be used as initial state vector in
subsequent experiments.</p>
<h2 id="limit-cycle-tutorial">Limit cycle experiments</h2>
<p>Background on the computaiton of limit cycles is found in the limit
cycle section of the manual. Briefly, models of cellular dynamics are
virtually <em>never</em> used with the values given for the initial
state vector. Rather, cells are based to a given limit cycle to
approximate the experimental conditions one is interested in. The state
of the cell can be frozen at any given instant in time and this state
can be reused then in subsequent simulations as initial state vector. In
the following experiments the procedures for finding an initial state
vector for a given model and a given basic cycle length are elucidated.
To run the experiments of this tutorial change directories as
follows:</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> <span class="va">${TUTORIALS}</span>/01_EP_single_cell/01_basic_bench </span></code></pre></div>
<p>To see all the exposed parameters itemized above in experimental
parameters section.</p>
<p>run</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--help</span></span></code></pre></div>
<h3 id="exp-ep-sc-01"><strong>Experiment exp01 (short pacing
protocol)</strong></h3>
<p>We start with pacing the tenTusscherPanfilov human myocycte model at
a pacing cycle length of 500 ms for a duration of 5 seconds (5000
msecs).</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--EP</span> tenTusscherPanfilov <span class="at">--duration</span> 5000 <span class="at">--bcl</span> 500 <span class="at">--ID</span> exp01 <span class="at">--visualize</span> <span class="at">--vis_var</span> ICaL INa IK1</span></code></pre></div>
<p>You will get an overview of current traces of the model.</p>
<h3 id="exp-ep-sc-02"><strong>Experiment exp02 (limit cycle pacing
protocol)</strong></h3>
<p>In experiment exp01 we observed that the state variables of the model
did not stabilize at a stable limit cycle. We increase therefore the
duration of our pacing protocol to 20 seconds (20000 msecs).</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--EP</span> tenTusscherPanfilov <span class="at">--duration</span> 20000 <span class="at">--bcl</span> 500 <span class="at">--ID</span> exp02 <span class="at">--visualize</span></span></code></pre></div>
<p>At the end of the pacing protocol the initial state vector is saved
in the file
<code>exp02_tenTusscherPanfilov_bcl_500_ms_dur_20000_ms.sv</code>.</p>
<div class="sourceCode" id="cb7"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">-85.0922</span>            <span class="co"># Vm</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span>                   <span class="co"># Lambda</span></span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span>                   <span class="co"># delLambda</span></span>
<span id="cb7-4"><a href="#cb7-4" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span>                   <span class="co"># Tension</span></span>
<span id="cb7-5"><a href="#cb7-5" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span>                   <span class="co"># Ke</span></span>
<span id="cb7-6"><a href="#cb7-6" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span>                   <span class="co"># Nae</span></span>
<span id="cb7-7"><a href="#cb7-7" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span>                   <span class="co"># Cae</span></span>
<span id="cb7-8"><a href="#cb7-8" aria-hidden="true" tabindex="-1"></a><span class="ex">0.00312354</span>          <span class="co"># Iion</span></span>
<span id="cb7-9"><a href="#cb7-9" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span>                   <span class="co"># tension_component</span></span>
<span id="cb7-10"><a href="#cb7-10" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span>                   <span class="co"># illum</span></span>
<span id="cb7-11"><a href="#cb7-11" aria-hidden="true" tabindex="-1"></a><span class="ex">tenTusscherPanfilov</span></span>
<span id="cb7-12"><a href="#cb7-12" aria-hidden="true" tabindex="-1"></a><span class="ex">3.9033</span>              <span class="co"># CaSR</span></span>
<span id="cb7-13"><a href="#cb7-13" aria-hidden="true" tabindex="-1"></a><span class="ex">0.000391138</span>         <span class="co"># CaSS</span></span>
<span id="cb7-14"><a href="#cb7-14" aria-hidden="true" tabindex="-1"></a><span class="ex">0.135498</span>            <span class="co"># Cai</span></span>
<span id="cb7-15"><a href="#cb7-15" aria-hidden="true" tabindex="-1"></a><span class="ex">3.43519e-05</span>         <span class="co"># D</span></span>
<span id="cb7-16"><a href="#cb7-16" aria-hidden="true" tabindex="-1"></a><span class="ex">0.726548</span>            <span class="co"># F</span></span>
<span id="cb7-17"><a href="#cb7-17" aria-hidden="true" tabindex="-1"></a><span class="ex">0.958436</span>            <span class="co"># F2</span></span>
<span id="cb7-18"><a href="#cb7-18" aria-hidden="true" tabindex="-1"></a><span class="ex">0.995449</span>            <span class="co"># FCaSS</span></span>
<span id="cb7-19"><a href="#cb7-19" aria-hidden="true" tabindex="-1"></a><span class="ex">3.98e-05</span>            <span class="co"># GCaL</span></span>
<span id="cb7-20"><a href="#cb7-20" aria-hidden="true" tabindex="-1"></a><span class="ex">0.153</span>               <span class="co"># GKr</span></span>
<span id="cb7-21"><a href="#cb7-21" aria-hidden="true" tabindex="-1"></a><span class="ex">0.392</span>               <span class="co"># GKs</span></span>
<span id="cb7-22"><a href="#cb7-22" aria-hidden="true" tabindex="-1"></a><span class="ex">0.294</span>               <span class="co"># Gto</span></span>
<span id="cb7-23"><a href="#cb7-23" aria-hidden="true" tabindex="-1"></a><span class="ex">0.740339</span>            <span class="co"># H</span></span>
<span id="cb7-24"><a href="#cb7-24" aria-hidden="true" tabindex="-1"></a><span class="ex">0.669782</span>            <span class="co"># J</span></span>
<span id="cb7-25"><a href="#cb7-25" aria-hidden="true" tabindex="-1"></a><span class="ex">137.333</span>             <span class="co"># Ki</span></span>
<span id="cb7-26"><a href="#cb7-26" aria-hidden="true" tabindex="-1"></a><span class="ex">0.0017662</span>           <span class="co"># M</span></span>
<span id="cb7-27"><a href="#cb7-27" aria-hidden="true" tabindex="-1"></a><span class="ex">7.96972</span>             <span class="co"># Nai</span></span>
<span id="cb7-28"><a href="#cb7-28" aria-hidden="true" tabindex="-1"></a><span class="ex">2.47788e-08</span>         <span class="co"># R</span></span>
<span id="cb7-29"><a href="#cb7-29" aria-hidden="true" tabindex="-1"></a><span class="ex">0.904552</span>            <span class="co"># R_</span></span>
<span id="cb7-30"><a href="#cb7-30" aria-hidden="true" tabindex="-1"></a><span class="ex">0.999972</span>            <span class="co"># S</span></span>
<span id="cb7-31"><a href="#cb7-31" aria-hidden="true" tabindex="-1"></a><span class="ex">0.0171519</span>           <span class="co"># Xr1</span></span>
<span id="cb7-32"><a href="#cb7-32" aria-hidden="true" tabindex="-1"></a><span class="ex">0.46972</span>             <span class="co"># Xr2</span></span>
<span id="cb7-33"><a href="#cb7-33" aria-hidden="true" tabindex="-1"></a><span class="ex">0.01471</span>             <span class="co"># Xs</span></span></code></pre></div>
<h2 id="imp-param-modification-tutorial">Ionic model parameter
modification experiments</h2>
<p>Model behavior can be modified by providing parameter modification
string through <code>--imp-par</code>. Modifications strings are of the
following form:</p>
<div class="sourceCode" id="cb8"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="va">param</span><span class="op">[</span>+|-|/|<span class="pp">*</span>|=<span class="op">][</span>+|-<span class="op">]</span><span class="co">###[.[</span><span class="al">###</span><span class="co">][e|E[-|+]</span><span class="al">###</span><span class="co">][\%]</span></span></code></pre></div>
<p>where <code>param</code> is the name of the paramter one wants to
modify such as, for instance, the peak conductivity of the sodium
channel, <code>GNa</code>. Not all parameters of ionic models are
exposed for modification, but a list of those which are can be retrieved
for each model using imp-info. As an example, let's assume we are
interested in specializing a generic human ventricular myocyte based on
the ten Tusscher-Panfilov model to match the cellular dynamics of
myocytes as they are found in the halo around an infract, i.e.in the
peri-infarct zone (PZ). In the PZ various current are downregulated.
Patch-clamp studies of cells harvested from the peri-infract zone have
reported a 62% reduction in peak sodium current, 69% reduction in L-type
calcium current and a reduction of 70% and 80% in potassium currents
<span class="math inline">\(I_{\mathrm {Kr}}\)</span> and <span
class="math inline">\(I_{\mathrm K}\)</span>, respectively. In the model
these modifcations are implemented by altering the peak conductivities
of the respective currents as follows:</p>
<div class="sourceCode" id="cb9"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="co"># both modification strings yield the same results</span></span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true" tabindex="-1"></a><span class="ex">--imp-par=</span><span class="st">&quot;GNa-62%,GCaL-69%,Gkr-70%,GK1-80%&quot;</span></span>
<span id="cb9-3"><a href="#cb9-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb9-4"><a href="#cb9-4" aria-hidden="true" tabindex="-1"></a><span class="co"># or</span></span>
<span id="cb9-5"><a href="#cb9-5" aria-hidden="true" tabindex="-1"></a><span class="ex">--imp-par=</span><span class="st">&quot;GNa*0.38,GCaL*0.31,GKr*0.3,GK1*0.2&quot;</span></span></code></pre></div>
<p>In this experiment EP model parameters strings can be passed in using
the <code>--EP-par</code> which expects parameter strings as input. The
results of such modifications are illustrated in the following
experiments.</p>
<h3 id="exp-ep-sc-03"><strong>Experiment exp03 (short pacing with
modified myocyte)</strong></h3>
<p>In this experiment we modify the behavior of a tenTusscherPanfilov
myocyte to match experimental observations of myocytes harvested from
peri-infarct zones in humans. Initially, we simulated one cycle only to
observe the effect of parameter modifications relative to the baseline
tenTusscherPanfilov model. The referece traces are passed in and you can
compared the experiment output.</p>
<div class="sourceCode" id="cb10"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--EP</span> tenTusscherPanfilov <span class="at">--duration</span> 5000 <span class="at">--bcl</span> 500 <span class="at">--ID</span> exp03 <span class="at">--EP-par</span> <span class="st">&quot;GNa-62%,GCaL-69%,GKr-70%,GK1-80%&quot;</span>             <span class="at">--visualize</span> <span class="at">--overlay</span></span></code></pre></div>
<p>Make sure that the parameter modification string is correctly
interpreted by <a
href="/documentation/examples/01_EP_single_cell/01_basic_bench">bench</a>
by inspecting the output log:</p>
<div class="sourceCode" id="cb11"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--EP</span> tenTusscherPanfilov <span class="at">--duration</span> 500 <span class="at">--bcl</span> 5000 <span class="at">--ID</span> test <span class="at">--EP-par</span> <span class="st">&quot;GNa-62%,GCaL-69%,GKr-70%,GK1-80%&quot;</span></span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb11-3"><a href="#cb11-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb11-4"><a href="#cb11-4" aria-hidden="true" tabindex="-1"></a><span class="ex">...</span> </span>
<span id="cb11-5"><a href="#cb11-5" aria-hidden="true" tabindex="-1"></a><span class="ex">Ionic</span> model: tenTusscherPanfilov</span>
<span id="cb11-6"><a href="#cb11-6" aria-hidden="true" tabindex="-1"></a>     <span class="ex">GNa</span>                  modifier: <span class="at">-62%</span>            value: 5.63844</span>
<span id="cb11-7"><a href="#cb11-7" aria-hidden="true" tabindex="-1"></a>     <span class="ex">GCaL</span>                 modifier: <span class="at">-69%</span>            value: 1.2338e-05</span>
<span id="cb11-8"><a href="#cb11-8" aria-hidden="true" tabindex="-1"></a>     <span class="ex">GKr</span>                  modifier: <span class="at">-70%</span>            value: 0.0459</span>
<span id="cb11-9"><a href="#cb11-9" aria-hidden="true" tabindex="-1"></a>     <span class="ex">GK1</span>                  modifier: <span class="at">-80%</span>            value: 1.081</span>
<span id="cb11-10"><a href="#cb11-10" aria-hidden="true" tabindex="-1"></a><span class="ex">...</span></span></code></pre></div>
<h3 id="exp-ep-sc-04"><strong>Experiment exp04 (limit cycle pacing with
modified myocyte)</strong></h3>
<p>We repeat exp03 as a <em>limit cycle</em> experiment now by pacing
for 20 seconds (20000 msecs). State variable and current traces stored
in the baseline experiment exp02 are used as a reference.</p>
<div class="sourceCode" id="cb12"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./run.py</span> <span class="at">--EP</span> tenTusscherPanfilov <span class="at">--duration</span> 20000 <span class="at">--bcl</span> 500 <span class="at">--ID</span> exp04 <span class="at">--EP-par</span> <span class="st">&quot;GNa-62%,GCaL-69%,GKr-70%,GK1-80%&quot;</span>             <span class="at">--visualize</span> <span class="at">--vis_var</span> Cai</span></code></pre></div>
<p>Visual comparison of the traces reveals that Calcium-cycling in the
peri-infarct myocyte shows <strong>alternans</strong>. The difference in
initial state vectors between baseline and peri-infarct myocyte stored
at the end of the pacing protocol can be inspected with the
<em>meld</em> or any other diff program of your choice.</p>
<div class="sourceCode" id="cb13"><pre
class="sourceCode bash"><code class="sourceCode bash"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a><span class="ex">meld</span> exp02_tenTusscherPanfilov_bcl_500_ms_dur_20000_ms.sv exp04_tenTusscherPanfilov_bcl_500_ms_dur_20000_ms.sv</span></code></pre></div>
<figure>
<img src="01_basic_bench/01_01_meld_comparison_state_vectors.png"
class="align-center"
alt="Comparing initial state vectors of baseline and peri-infarct tenTusscherPanfilov human ventricular myocyte model after 20 seconds of pacing at a basic cycle length of 500 ms." />
<figcaption aria-hidden="true">Comparing initial state vectors of
baseline and peri-infarct tenTusscherPanfilov human ventricular myocyte
model after 20 seconds of pacing at a basic cycle length of 500
ms.</figcaption>
</figure>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>