---
description: Learn how to convert your .model file (EasyML) to  CellML
image: 01_11_CellML.png
title: Convert EasyML (.model) to CellML
---

<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>
<h1>Converting EasyML to CellML</h1>
<i>See <a href="https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/01_EP_single_cell/11_toCellML/run.py" target="_blank">code</a> in GitLab.</i><br/>
<i>Author: Axel Loewe <axel.loewe@kit.edu>, Jorge Sánchez <jorge.arciniegas@kit.edu></i>
<table>
<thead>
<tr class="header">
<th>Converting EasyML to CellML</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td>Intro</td>
</tr>
</tbody>
</table>
<p>CellML is a markup language for describing, amongst other things,
ionic models. Many models are published and available in the <a
href="https://models.cellml.org/electrophysiology">CellML Model
Repository</a>. However, this format can only be processed by a machine.
openCARP uses its own markup language called <a
href="/documentation/examples/01_ep_single_cell/05_easyml">EasyML</a>.
This tutorial describes how to convert your EasyML .model file to CellML
for sharing it with others or publishing it in the CelML Model
Repository. In this process, we employ <a
href="http://myokit.org">Myokit</a>'s .mmt format as an intermediate
step.</p>
<h2 id="prerequisites">Prerequisites</h2>
<p>This tutorial assumes that you installed openCARP from source as you
likely had to compile your new .model file anyway. It should also work
with other forms of installation but you may need to adapt some
paths.</p>
<p>You need to have myokit installed and in your
<code>PYTHONPATH</code>. Often this can be done by simply running
<code>pip install myokit</code>. See the <a
href="http://myokit.org">Myokit webpage</a> for more detailed
information.</p>
<h2 id="converting-.model-to-.mmt">Converting .model to .mmt</h2>
<p>In a first step, the EasyML .model file is converted to Myokit's .mmt
format. For this, a python script is shipped with openCARP. Conversion
of a CellML file is performed using the <code>cellml_converter.py</code>
script:</p>
<pre><code>usage: EasyML2mmt.py [-h] [--verbose] [--init_component INIT_COMPONENT]
                 [--istim ISTIM] [--pace_name PACE_NAME] [--tend TEND]
                 [--bcl BCL] [--plugin] [--author AUTHOR] [--desc DESC]
                 eml

convert a EasyML model file to myokit mmt format

In the EasyML file, comments with meta data provide the component name,
which is not part of the EasyML specification. A component called cname is
identified by one of the 2 forms:

# COMPONENT cname
// COMPONENT cname

All variables and equations belong to the last component declared.
By default, an initial component cell is in effect, but this particular name
can be changed.

N.B.: groups within groups are not supported
N.B.: hyperbolic trig functions are not supported
N.B.: try to avoid using variables named just d or inf

positional arguments:
  eml                   EasyML file

optional arguments:
  -h, --help            show this help message and exit
  --verbose             verbose output
  --init_component INIT_COMPONENT
                        initial component [cell]
  --istim ISTIM         name of stimulus current [I_stim]
  --pace_name PACE_NAME
                        name of pacing variable [pace]
  --tend TEND           duration [1100.0]
  --bcl BCL             pacing cycle length [500.0]
  --plugin              IMP is a plugin</code></pre>
<p>Set the <code>--plugin</code> flag when your EasyML file describes a
plugin (i.e. it adds at value to <code>Iion</code>) rather than a model
(i.e. it defines <code>Iion</code>). When conversion to CellML is your
goal, you can stay with the default values for the other optional
parameters as they mostly control the simulation protocol in Myokit. The
output of this step will be an .mmt file. Watch out for warnings as some
EasyML expressions are not supported in Myokit.</p>
<dl>
<dt>Copy the .model file to the local directory, for example:</dt>
<dd>
<p><code>cp /path/to/openCARP/physics/limpet/models/DrouhardRoberge.model .</code></p>
</dd>
<dt>Example call:</dt>
<dd>
<p><code>python /path/to/openCARP/physics/limpet/src/python/EasyML2mmt.py DrouhardRoberge.model</code></p>
</dd>
</dl>
<h2 id="converting-.mmt-to-.cellml">Converting .mmt to .cellml</h2>
<p>We use Myokit to generate the final CellML file. script:</p>
<pre><code>usage: convert.py [-h] model

Convert a .mmt model to CellML.

positional arguments:
  model       model.mmt is converted to model.cellml

optional arguments:
  -h, --help  show this help message and exit</code></pre>
<p>The output of this step is the .cellml file that can then be uploaded
to the <a href="https://models.cellml.org/electrophysiology">CellML
Model Repository</a> for example.</p>
<dl>
<dt>Example call:</dt>
<dd>
<p><code>python convert.py DrouhardRoberge</code></p>
</dd>
</dl>
<div class="notices green">
    <p>
        <a href="/documentation/examples">Back to the examples overview</a>
    </p>
</div>