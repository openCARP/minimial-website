---
title: Examples
visible: true
pipeline: experiments
icon: heartbeat
description: 'Examples of cardiac in-silico experiments as introduction to openCARP and Carputils'
multicards:
    -
        key: 01_ep_single_cell_cards
        title: 'Electrophysiology in single cell'
        description: 'The following examples illustrate how single cell modeling is performed using the tool `bench`. Additionally you learn how to integrate a single cell model from CellML into our library `limpet` using the math language EasyML'
    -
        key: 02_ep_tissue_cards
        title: 'Electrophysiology tissue'
        description: 'These examples should inform you about the most basic steps in developing simple tissue simulations using openCARP'
    -
        key: visualization_cards
        title: Visualization
        description: 'Here you will learn how to use the visualization tools LimpetGUI for single cell results and Meshalizer for tissue results'
01_ep_single_cell_cards:
    items:
        '@page.children': /documentation/examples/01_ep_single_cell
02_ep_tissue_cards:
    items:
        '@page.children': /documentation/examples/02_ep_tissue
visualization_cards:
    items:
        '@page.children': /documentation/examples/visualization
default_card_icon: lightbulb
---

# openCARP examples

These examples are intended to transfer basic user know-how regarding most openCARP features in an efficient way. The scripts are designed as mini-experiments, which can also serve as basic building blocks for more complex experiments. [This example](02_ep_tissue/22_simple) can be a good starting point to base your own experiment on.

There is a number of examples dedicated to teaching openCARP fundamental know-how for those who are interested in building more complex experiments from scratch themselves or in extending pre-existing experiments. All executable examples are coded up in carputils to facilitate an easy execution of all experiments without significant additional effort and complex command line interactions.

## Intended use

Most examples can be run by simply copying the command from the corresponding example web page. It is recommended to inspect the generated command lines to understand what the simulation looks like in the plain command line by adding the option `--dry` to the run script command line. You can download the examples from our [repository](https://git.opencarp.org/openCARP/experiments).
[//]: # "Detailed background information on carputils command line options is given in the carputils usage section."

